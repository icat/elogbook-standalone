import { GUI_CONFIG } from "../src/config/gui.config";
import React from "react";
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import EventPager from '../src/components/Event/EventPager';
jest.mock("../src/config/gui.config");

/* code executed before each test */
beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });
})

describe('EventPager tests', () => {
    function getWrapper(eventCount, isFloatingRight) {
        return (Enzyme.shallow(<EventPager
            eventCount={eventCount}
            isFloatingRight={isFloatingRight}
        />))
    }
    describe("rendering", () => {
        it("renders nothing when no event are found", () => {
            let actualEventCount = 0;
            GUI_CONFIG.mockReturnValue({ EVENTS_PER_PAGE: 10 });
            expect(getWrapper(actualEventCount).get(0)).toBeNull();
        })

        it("renders pages floating on the right when isFloatingRight is true", () => {
            let actualEventCount = 10;
            GUI_CONFIG.mockReturnValue({ EVENTS_PER_PAGE: 10 });
            let expectedStyle = {
                float: 'right',
                marginRight: '40px'
            }

            expect(getWrapper(actualEventCount, true).at(0).prop("style")).toEqual(expectedStyle);
        })

        it("renders pages not floating on the right when isFloatingRight is false or not set", () => {
            let actualEventCount = 10;
            GUI_CONFIG.mockReturnValue({ EVENTS_PER_PAGE: 10 });
            let expectedStyle = {
                marginBottom: "55px",
                marginRight: '40px',
                textAlign: 'right'
            }

            expect(getWrapper(actualEventCount, false).at(0).prop("style")).toEqual(expectedStyle);
            expect(getWrapper(actualEventCount, undefined).at(0).prop("style")).toEqual(expectedStyle);
        })

    })
    describe("getTotalPageNumber", () => {
        it('returns the number of page requested to display the data', () => {
            let actualEventCount = 100;
            GUI_CONFIG.mockReturnValue({ EVENTS_PER_PAGE: 10 });
            let expectedValue = 10;
            let myWrapper = getWrapper(actualEventCount);

            expect(myWrapper.instance().getTotalPageNumber()).toBe(expectedValue);
        })

        it('returns 1 when event count is lower than the config file value', () => {
            let actualEventCount = 20;
            GUI_CONFIG.mockReturnValue({ EVENTS_PER_PAGE: 30 });
            let expectedValue = 1;
            let myWrapper = getWrapper(actualEventCount);

            expect(myWrapper.instance().getTotalPageNumber()).toBe(expectedValue);
        })

        it('returns 1 when EVENT_PER_PAGE config value is not set', () => {
            let actualEventCount = 20;
            GUI_CONFIG.mockReturnValue({});
            let expectedValue = 1;
            let myWrapper = getWrapper(actualEventCount);

            expect(myWrapper.instance().getTotalPageNumber()).toBe(expectedValue);
        })
    })
})
