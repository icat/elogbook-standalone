module.exports = {
    rendering: {
        editingIcon: [
            {
                aboutThisTest: "editing icon is a + because the comment has no previous version",
                event: {
                    _id: 'testId1',
                    category: "comment",
                    content: [
                        { format: 'plainText', text: 'this is a test' },
                        { format: 'html', text: '<p> this is a test </p>' }
                    ],
                    createdAt: '2018-01-01T00:01:00.000Z',
                    creationDate: '2018-01-01T00:00:00.000Z',
                    datasetId: null,
                    file: [],
                    fileSize: null,
                    filename: null,
                    investigationId: 'testInvestigationId',
                    machine: null,
                    previousVersionEvent: null,
                    software: null,
                    tag: [],
                    title: 'test title',
                    type: "annotation",
                    updatedAt: "2018-01-01T00:00:00.000Z",
                    username: "mchaille"
                },
                expected: 'plus'
            },
            {
                aboutThisTest: "editing icon is an edit because the comment has one previous version",
                event: {
                    _id: 'testId2',
                    category: "comment",
                    content: [
                        { format: 'plainText', text: 'this is an updated  comment' },
                        { format: 'html', text: '<p> this is an updated comment </p>' }
                    ],
                    createdAt: '2018-01-02T00:01:00.000Z',
                    creationDate: '2018-01-02T00:00:00.000Z',
                    datasetId: null,
                    file: [],
                    fileSize: null,
                    filename: null,
                    investigationId: 'testInvestigationId',
                    machine: null,
                    previousVersionEvent: {
                        _id: 'testId1',
                        category: "comment",
                        content: [
                            { format: 'plainText', text: 'this is a comment' },
                            { format: 'html', text: '<p> this is a comment </p>' }
                        ],
                        createdAt: '2018-01-01T00:01:00.000Z',
                        creationDate: '2018-01-01T00:00:00.000Z',
                        datasetId: null,
                        file: [],
                        fileSize: null,
                        filename: null,
                        investigationId: 'testInvestigationId',
                        machine: null,
                        previousVersionEvent: null,
                        software: null,
                        tag: [],
                        title: 'test title',
                        type: "annotation",
                        updatedAt: "2018-01-01T00:01:00.000Z",
                        username: "mchaille"
                    },
                    software: null,
                    tag: [],
                    title: 'test title',
                    type: "annotation",
                    updatedAt: "2018-01-02T00:01:00.000Z",
                    username: "mchaille"
                },
                expected: 'edit'
            }
        ]
    }
}