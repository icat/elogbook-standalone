import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PeriodicRefresher from '../../src/containers/Logbook/PeriodicRefresher';

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() })
})

describe("periodicRefresher unit tests", () => {
    function getMountWrapper(initialValue, intervalInMilliSeconds, isEnabled, periodicCallback, onCallbackSuccess = () => null) {
        return Enzyme.mount(<PeriodicRefresher
            initialValue={initialValue}
            intervalInMilliSeconds={intervalInMilliSeconds}
            isEnabled={isEnabled}
            onCallbackSuccess={onCallbackSuccess}
            periodicCallback={periodicCallback}>
            <div id='child'> empty</div>
        </PeriodicRefresher>
        );
    }

    it('calls the callback periodically when enabled', (done) => {
        let mockedPeriodicCallback = jest.fn();
        // instanciate the component. This starts the callback counts
        let wrapper = getMountWrapper([], 1000, true, mockedPeriodicCallback);
        // wait 4s
        setTimeout(() => {
            expect(mockedPeriodicCallback.mock.calls.length).toBeGreaterThan(2);
            done();
        }, 4000);
    })

    it('calls onSuccess() when the callback request succeeds', (done) => {
        let mockedOnSuccess = jest.spyOn(PeriodicRefresher.prototype, 'onSuccess');

        let wrapper = getMountWrapper([], 100, true, (onSuccess, onFailure) => { onSuccess('data') });
        setTimeout(() => {
            expect(mockedOnSuccess).toHaveBeenCalled();
            done();
        }, 1000);
    })

    it('calls onCallbackSuccess() when the callback request succeeds', (done) => {
        let mockedOnCallbackSuccess = jest.fn();
        let wrapper = getMountWrapper([], 100, true, (onSuccess, onFailure) => { onSuccess('data') }, mockedOnCallbackSuccess);
        setTimeout(() => {
            expect(mockedOnCallbackSuccess).toHaveBeenCalled();
            done();
        }, 1000);
    })


    it('does not call the callback when not enabled', (done) => {
        let mockedPeriodicCallback = jest.fn();
        // instanciate the componet. THis start the callback counts
        let wrapper = getMountWrapper([], 1000, false, mockedPeriodicCallback);
        // wait 4s
        setTimeout(() => {
            expect(mockedPeriodicCallback.mock.calls.length).toBe(0);
            done();
        }, 4000);
    })

    it('transmits initialValue to its child in periodicdata property at initialization', () => {
        let wrapper = getMountWrapper('initialValue', 1000, false, () => null);
        expect(wrapper.find({ id: 'child' }).prop('periodicdata')).toEqual('initialValue');
    })

    it('transmits data obtained by the callback to its child in periodicdata ', () => {
        let wrapper = getMountWrapper('initialValue', 100, true, (onSuccess, onFailure) => { onSuccess('data') });
        // we do not wait before doing the assertion so only the inital value is transmitted at that moment
        expect(wrapper.find({ id: 'child' }).prop('periodicdata')).toEqual('initialValue');

        setTimeout(() => {
            //then after 100ms, the periodic callback took place, so the data is passed to the child
            expect(wrapper.find({ id: 'child' }).prop('periodicdata')).toEqual('data');
        }, 1000);
    })
});
