import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TagLabel from '../../src/components/Logbook/Tag/TagLabel';
let resources = require('./resources/TagLabel.resource');

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() })
})

describe("TagLabel unit tests", () => {
    function getShallowWrapper(tag, showDeleteButton, onDeleteTagClicked) {
        return Enzyme.shallow(<TagLabel
            onDeleteTagClicked={onDeleteTagClicked}
            showDeleteButton={showDeleteButton}
            tag={tag} />
        );
    }

    describe('rendering', () => {
        let resource = resources.rendering;
        it('renders a label with no deletebutton by default', () => {
            expect(getShallowWrapper(resource.tag, null, null).find('Label').length).toBe(1);
            expect(getShallowWrapper(resource.tag, null, null).find('Label').exists('Glyphicon')).toEqual(false);
        })

        it('renders a label with no deletebutton when showDeleteButton prop is false', () => {
            expect(getShallowWrapper(resource.tag, false, null).find('Label').length).toBe(1);
            expect(getShallowWrapper(resource.tag, false, null).find('Label').exists('Glyphicon')).toEqual(false);
        })

        it('renders a label with no deletebutton when showDeleteButton prop is true', () => {
            expect(getShallowWrapper(resource.tag, true, null).find('Label').length).toBe(2);
            expect(getShallowWrapper(resource.tag, true, null).find('Label').exists('Glyphicon')).toEqual(true);
        })
    });

    describe('callback', () => {
        it('onDeleteButton is called when user clicks on the delete button', () => {
            let resource = resources.rendering;
            let mockedOnDeleteTagClicked = jest.fn();
            let wrapper = getShallowWrapper(resource.tag, true, mockedOnDeleteTagClicked);
            expect(mockedOnDeleteTagClicked).toHaveBeenCalledTimes(0);
            wrapper.find('Glyphicon').simulate('click');
            expect(mockedOnDeleteTagClicked).toHaveBeenCalledTimes(1);
        });
    })
});
