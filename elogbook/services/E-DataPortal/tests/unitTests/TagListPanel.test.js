import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import TagListPanel from '../../src/components/Logbook/Tag/TagListPanel';
const resources = require('./resources/TagListPanel.resource.js')

require('it-each')({ testPerIteration: true });

describe("TagListPanel unit tests", () => {

    beforeEach(() => {
        Enzyme.configure({ adapter: new Adapter() });

    })
    function getShallowedWrapper(tags, onEditButtonClicked) {
        return Enzyme.shallow(<TagListPanel
            availableTags={tags}
            onEditButtonClicked={onEditButtonClicked} />);
    }

    function getMountedWrapper(tags, onEditButtonClicked) {
        return Enzyme.mount(<TagListPanel
            availableTags={tags}
            onEditButtonClicked={onEditButtonClicked} />);
    }

    describe('rendering', () => {

        it.each(resources.rendering, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.tags, () => null).find('TagListPanelLine').length).toBe(2);
                expect(getShallowedWrapper(element.tags, () => null).find('TagListPanelLine').at(0).prop("tag")).toEqual(element.tags[0]);
                expect(getShallowedWrapper(element.tags, () => null).find('TagListPanelLine').at(1).prop("tag")).toEqual(element.tags[1]);
                next();
            })


        it("informs that there is no tags when the investigation has no tags", () => {
            let expectedText = "There is no tag in this proposal yet."
            expect(getShallowedWrapper([], () => null).find('TagListPanelLine').length).toBe(0);
            expect(getShallowedWrapper([], () => null).find('UserMessage').prop('message')).toEqual(expectedText);
        })
    })

    describe('callback', () => {
        it('calls onEditButtonClicked when the user clicks the edit button', () => {
            resources.callback
            let mockedOnEditButtonClicked = jest.fn();
            let wrapper = getMountedWrapper(resources.callback.tags, mockedOnEditButtonClicked);
            expect(mockedOnEditButtonClicked).toHaveBeenCalledTimes(0);
            wrapper.find('Button').simulate('click');
            expect(mockedOnEditButtonClicked).toHaveBeenCalledTimes(1);
        })
    })
})
