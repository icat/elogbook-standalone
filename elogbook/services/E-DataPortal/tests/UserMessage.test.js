import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import UserMessage from '../src/components/UserMessage';
import { INFO_MESSAGE_TYPE, SUCCESS_MESSAGE_TYPE, ERROR_MESSAGE_TYPE } from '../src/constants/UserMessages';

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });

})

describe("Tests on UserMessage", () => {
    function getWrapper(message, type, isTextCentered) {
        return Enzyme.shallow(<UserMessage
            isTextCentered={isTextCentered}
            message={message}
            type={type}
        />)
    }

    it("renders nothing when no message is provided", () => {
        let actualMessage = "";
        let actualType = INFO_MESSAGE_TYPE;
        expect(getWrapper(actualMessage, actualType).get(0)).toBeNull();
    })

    describe("Tests on info messages", () => {
        it("renders an info alert when the type is INFO_MESSAGE_TYPE", () => {
            let actualMessage = "This is an message";
            let actualType = INFO_MESSAGE_TYPE;
            expect(getWrapper(actualMessage, actualType).find("Alert").prop("bsStyle")).toEqual("info");
        })

        it("text is left aligned when isCenteredText is not provided or false", () => {
            let actualMessage = "This is an message";
            let actualType = INFO_MESSAGE_TYPE;
            expect(getWrapper(actualMessage, actualType).find("Alert").prop("style").textAlign).toEqual("left");

            let actualIsTextCentered = false;
            expect(getWrapper(actualMessage, actualType, actualIsTextCentered).find("Alert").prop("style").textAlign).toEqual("left");
        })

        it("text is centered when isCenteredText is set to true", () => {
            let actualMessage = "This is an message";
            let actualType = INFO_MESSAGE_TYPE;
            let actualIsTextCentered = true;
            expect(getWrapper(actualMessage, actualType, actualIsTextCentered).find("Alert").prop("style").textAlign).toEqual("center");
        })
    })

    describe("Tests on success messages", () => {
        it("renders an success alert when the type is SUCCESS_MESSAGE_TYPE", () => {
            let actualMessage = "This is an message";
            let actualType = SUCCESS_MESSAGE_TYPE;
            expect(getWrapper(actualMessage, actualType).find("Alert").prop("bsStyle")).toEqual("success");
        })

        it("text is left aligned when isCenteredText is not provided or false", () => {
            let actualMessage = "This is an message";
            let actualType = SUCCESS_MESSAGE_TYPE;
            expect(getWrapper(actualMessage, actualType).find("Alert").prop("style").textAlign).toEqual("left");

            let actualIsTextCentered = false;
            expect(getWrapper(actualMessage, actualType, actualIsTextCentered).find("Alert").prop("style").textAlign).toEqual("left");
        })

        it("text is centered when isCenteredText is set to true", () => {
            let actualMessage = "This is an message";
            let actualType = SUCCESS_MESSAGE_TYPE;
            let actualIsTextCentered = true;
            expect(getWrapper(actualMessage, actualType, actualIsTextCentered).find("Alert").prop("style").textAlign).toEqual("center");
        })
    })

    describe("Tests on error messages", () => {
        it("renders an danger alert when the type is ERROR_MESSAGE_TYPE", () => {
            let actualMessage = "This is an message";
            let actualType = ERROR_MESSAGE_TYPE
            expect(getWrapper(actualMessage, actualType).find("Alert").prop("bsStyle")).toEqual("danger");
        })

        it("text is left aligned when isCenteredText is not provided or false", () => {
            let actualMessage = "This is an message";
            let actualType = ERROR_MESSAGE_TYPE;
            expect(getWrapper(actualMessage, actualType).find("Alert").prop("style").textAlign).toEqual("left");

            let actualIsTextCentered = false;
            expect(getWrapper(actualMessage, actualType, actualIsTextCentered).find("Alert").prop("style").textAlign).toEqual("left");
        })

        it("text is centered when isCenteredText is set to true", () => {
            let actualMessage = "This is an message";
            let actualType = ERROR_MESSAGE_TYPE;
            let actualIsTextCentered = true;
            expect(getWrapper(actualMessage, actualType, actualIsTextCentered).find("Alert").prop("style").textAlign).toEqual("center");
        })

    })

})