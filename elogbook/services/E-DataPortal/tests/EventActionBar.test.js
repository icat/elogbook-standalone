import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import EventActionBar from '../src/components/Event/EventActionBar';

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });

})

describe("EventActionBar tests", () => {

    function getWrapper(numberOfMatchingEventsFound, isNewEventVisible) {
        return Enzyme.shallow(<EventActionBar
            investigationId="test investigationId"
            isNewEventVisible={isNewEventVisible}
            numberOfMatchingEventsFound={numberOfMatchingEventsFound}
            reverseEventsSortingByCreationDate={() => null}
            searchEvents={() => null}
            sessionId="sessionIdForTests"
            setNewEventVisibility={() => null}
            setView={() => null}
            sortingFilter={{}}
        />)
    }
    describe("rendering", () => {
        describe("There is no events in the logbook", () => {
            it("renders disabled PDF button whether a new event is beaing created or not", () => {
                let actualNumberOfMatchingEventsFound = 0;
                let isNewEventVisible = false;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("PDFButton").dive().find("Button").prop("disabled")).toBe(true);
                isNewEventVisible = true;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("PDFButton").dive().find("Button").prop("disabled")).toBe(true);
            })

            it("renders enabled New button when no new event is being created", () => {
                let actualNumberOfMatchingEventsFound = 0;
                let isNewEventVisible = false;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("NewButton").dive().find("Button").prop('disabled')).toBe(false);
            })

            it("renders diabled New button when a new event is being created", () => {
                let actualNumberOfMatchingEventsFound = 0;
                let isNewEventVisible = true;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("NewButton").dive().find("Button").prop("disabled")).toBe(true);
            })
        })

        describe("There are events in the logbook", () => {
            it("renders disabled PDF button when a new event is beaing created", () => {
                let actualNumberOfMatchingEventsFound = 1;
                let isNewEventVisible = true;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("PDFButton").dive().find("Button").prop("disabled")).toBe(true);
            })

            it("renders disabled PDF button when isNewEventVisible is not set", () => {
                let actualNumberOfMatchingEventsFound = 1;
                let isNewEventVisible = undefined;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("PDFButton").dive().find("Button").prop("disabled")).toBe(true);
            })


            it("renders enabled New button when no new event is being created", () => {
                let actualNumberOfMatchingEventsFound = 1;
                let isNewEventVisible = false;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("NewButton").dive().find("Button").prop("disabled")).toBe(false);
            })

            it("renders diabled New button when a new event is being created", () => {
                let actualNumberOfMatchingEventsFound = 1;
                let isNewEventVisible = true;
                expect(getWrapper(actualNumberOfMatchingEventsFound, isNewEventVisible).find("NewButton").dive().find("Button").prop("disabled")).toBe(true);
            })
        })

    })
})