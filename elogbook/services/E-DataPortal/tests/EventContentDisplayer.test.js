import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { LOGGED_IN } from '../src/constants/ActionTypes.js'
import EventContentDisplayer from '../src/components/Event/EventContentDisplayer.js';
import { badError1, badComment1 } from './BadEventLibrary.js';
import { goodComment1, goodError1 } from './GoodEventLibrary.js';

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });

})

describe('EventContentDisplayer tests', () => {
    /* a general wrapper used several times among the tests */
    function getSimpleWrapper(event, useRichTextEditor, isEditionMode) {
        return (
            Enzyme.shallow(
                <EventContentDisplayer
                    content={event.content}
                    useRichTextEditor={useRichTextEditor}
                    isEditionMode={isEditionMode}
                    investigationId='123'
                    canEnableSaveButton={() => null}
                    user={{
                        type: LOGGED_IN,
                        username: 'username',
                        sessionId: 'sessionId'
                    }} />)
        )
    }

    it('returns null for an event with empty content', () => {
        let actualEvent = badComment1;
        expect(getSimpleWrapper(actualEvent, false, false).get(0)).toBe(null)
    })

    describe('with editor', () => {
        it('displays content for viewing/editing using html format when html and plain text is available', () => {
            let actualEvent = goodComment1;
            let expectedText = "<p> html text content 1 </p>"
            expect(getSimpleWrapper(actualEvent, true, false).find('HTMLEditor').prop('text')).toEqual(expectedText)
            expect(getSimpleWrapper(actualEvent, true, true).find('HTMLEditor').prop('text')).toEqual(expectedText)
        })

        it('displays content for viewing/editing using text format when html format is not available', () => {
            let actualEvent = goodError1;
            let expectedText = "<p>" + "beamline lost" + "</p>"

            expect(getSimpleWrapper(actualEvent, true, false).find('HTMLEditor').prop('text')).toEqual(expectedText)
            expect(getSimpleWrapper(actualEvent, true, true).find('HTMLEditor').prop('text')).toEqual(expectedText)
        })

        it('displays content for viewing/editing using html format when html and plain text is available', () => {
            let actualEvent = goodComment1;
            let expectedText = "<p> html text content 1 </p>"
            expect(getSimpleWrapper(actualEvent, true, false).find('HTMLEditor').prop('text')).toEqual(expectedText)
            expect(getSimpleWrapper(actualEvent, true, true).find('HTMLEditor').prop('text')).toEqual(expectedText)
        })
    })

    describe('without editor', () => {
        it('displays content for viewing/editing using html format when html and plain text is available', () => {
            let actualEvent = goodComment1;
            let expectedText = { __html: '<p> html text content 1 </p>' }

            expect(getSimpleWrapper(actualEvent, false, false).find({ dangerouslySetInnerHTML: expectedText }).exists()).toBe(true)
            expect(getSimpleWrapper(actualEvent, false, true).find({ dangerouslySetInnerHTML: expectedText }).exists()).toBe(true)
        })

        it('displays content for viewing/editing using text format when html format is not available', () => {
            let actualEvent = goodError1;
            let expectedText = { __html: "<p>" + "beamline lost" + "</p>"}

            expect(getSimpleWrapper(actualEvent, false, false).find({ dangerouslySetInnerHTML: expectedText }).exists()).toBe(true)
            expect(getSimpleWrapper(actualEvent, false, true).find({ dangerouslySetInnerHTML: expectedText }).exists()).toBe(true)
        })

        it('displays content for viewing/editing using html format when html and plain text is available', () => {
            let actualEvent = goodComment1;
            let expectedText = { __html: "<p> html text content 1 </p>" }

            expect(getSimpleWrapper(actualEvent, false, false).find({ dangerouslySetInnerHTML: expectedText }).exists()).toBe(true)
            expect(getSimpleWrapper(actualEvent, false, true).find({ dangerouslySetInnerHTML: expectedText }).exists()).toBe(true)
        })
    })

})
