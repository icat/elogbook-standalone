import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import EventList from '../src/components/Event/List/EventList';
const resources = require('./resources/eventList.resource.js')

require('it-each')({ testPerIteration: true });

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });
})

describe("Tests on EventList component", () => {
    describe("rendering", () => {
        function getShallowedWrapper(events) {
            return Enzyme.shallow(<EventList
                events={events}
                onEventClicked={() => null}
            />
            )
        }

        it.each(resources.rendering.editingIcon, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper([element.event]).find('Event').dive().find('Glyphicon').prop('glyph')).toEqual(element.expected);
                next();
            })

        // function getWrapper(events, view) {
        //     return Enzyme.shallow(<EventList
        //         events={events}
        //         investigationId="testInvestigationId"
        //         onEventUpdated={() => null}
        //         reloadEvents={() => null}
        //         reverseEventsSortingByCreationDate={() => null}
        //         user={{
        //             type: LOGGED_IN,
        //             username: 'username',
        //             sessionId: 'sessionId'
        //         }}
        //         view={view}
        //     />)
        // }

        // describe("there is no event", () => {
        //     it("render a user message when there is no event in the logbook", () => {
        //         let actualEvents = [];
        //         let actualView = LIST_VIEW;

        //         expect(getWrapper(actualEvents, actualView).find("UserMessage").prop("message")).toEqual("The logbook is currently empty.");
        //         expect(getWrapper(actualEvents, actualView).find("UserMessage").prop("type")).toEqual(INFO_MESSAGE_TYPE);
        //     })
        // })

        // describe("there are events", () => {
        //     /**
        //     * creates a new event object
        //     * @param {*} id  the event id
        //     * @param {*} title the event title
        //     * @param {*} creationDate  the creation date
        //     */
        //     function createEvent(id, title, creationDate) {
        //         return ({
        //             _id: id,
        //             category: "comment",
        //             content: [{
        //                 format: 'plainText',
        //                 text: 'this is a test'
        //             }, {
        //                 format: 'html',
        //                 text: '<p> this is a test </p>'
        //             }],
        //             createdAt: creationDate, // was "2018-01-01T00:00:00.000Z"
        //             creationDate: creationDate,
        //             datasetId: null,
        //             file: [],
        //             fileSize: null,
        //             filename: null,
        //             investigationId: 'testInvestigationId',
        //             machine: null,
        //             previousVersionEvent: null,
        //             software: null,
        //             tag: [],
        //             title: title,
        //             type: "annotation",
        //             updatedAt: "2018-01-01T00:00:00.000Z",
        //             username: "mchaille"
        //         })
        //     }

        //     function createEvents(amount) {
        //         let events = []
        //         for (let index = 1; index <= amount; index++) {
        //             events.push(createEvent(index, "title" + index, new Date('0' + index + ' Jan 2018 00:00:00 GMT')));
        //         }
        //         return events;
        //     }

        //     it("renders events", () => {
        //         let actualEvents = createEvents(2); // create 2 events
        //         let actualView = LIST_VIEW;

        //         expect(getWrapper(actualEvents, actualView).find("Event").length).toBe(2);
        //     })

        //     it("does not render the user message when there is no event", () => {
        //         let actualEvents = createEvents(2); // create 2 events
        //         let actualView = LIST_VIEW;

        //         expect(getWrapper(actualEvents, actualView).find("UserMessage").length).toBe(0);
        //     })
        // })
    })
})