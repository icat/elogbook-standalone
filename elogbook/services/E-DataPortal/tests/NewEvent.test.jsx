import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import NewEvent from '../src/components/Event/NewEvent'
import { Collapse, Panel, FormControl } from 'react-bootstrap'
import EventHeader from '../src/components/Event/EventHeader'
import EventFooter from '../src/components/Event/EventFooter'
import HTMLEditor from '../src/components/Event/HTMLEditor'
import { NEW_EVENT_CONTEXT } from '../src/constants/EventTypes';


/* global variables */
let myUser;
let myNewEventShallowedWrapper;

/* code executed before each test */
beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() })

    myUser = {
        sessionId: "testSessionId",
        username: 'testUsername'
    }

    myNewEventShallowedWrapper =
        Enzyme.shallow(
            <NewEvent
                investigationId="0"
                isVisible={false}
                reloadEvents={() => null}
                setNewEventVisibility={() => null}
                user={myUser}
            />)
})

test("'in' property of the collapse component equals expanded prop", () => {
    let myNewEvent2 = (isvisible) => {
        return Enzyme.shallow(
            <NewEvent
                investigationId="0"
                isVisible={isvisible}
                reloadEvents={() => null}
                setNewEventVisibility={() => null}
                user={myUser}
            />)
    }
    expect(myNewEvent2(false).find(Collapse).prop("in")).toBe(false);
    expect(myNewEvent2(true).find(Collapse).prop("in")).toBe(true);
})

test("Contains a Panel with bsStyle=primary", () => {
    expect(myNewEventShallowedWrapper.find(Panel).first().prop("bsStyle")).toBe("primary");
})

test("Panel contains eventHeader component with prop context=NEW_EVNT_CONTEXT", () => {
    expect(myNewEventShallowedWrapper.find(Panel).childAt(0).is(EventHeader)).toBe(true);
    expect(myNewEventShallowedWrapper.find(Panel).childAt(0).prop("context")).toBe(NEW_EVENT_CONTEXT);
})

test('Panel.body contains a formControl with prop type=text used to optionnally change the title', () => {
    /* the expected node below is not the real node which is rendered. The real node contains and array function 
which can not be tested with the contains method. */
    let expectedNode = <FormControl type="text" placeholder="Write a title here (optional)" />
    expect(myNewEventShallowedWrapper.find(Panel.Body).containsMatchingElement(expectedNode)).toBe(true);
})

test('Panel.Body contains a component HTMLEditor with prop isEditionMode=true', () => {
    let expectedNode = <HTMLEditor user={myUser} investigationId={"0"} isEditionMode={true} />
    expect(myNewEventShallowedWrapper.find(Panel.Body).containsMatchingElement(expectedNode)).toBe(true);
})

test('createEvent function is called when a new event is created', () => {
    let expectedFunction = myNewEventShallowedWrapper.instance().createEvent
    expect(myNewEventShallowedWrapper.find(EventFooter).prop("onSaveButtonClicked")).toEqual(expectedFunction);

})

describe("A new event is created", () => {

    let myNewEventMounted;

    beforeEach(() => {
        myNewEventMounted = Enzyme.mount(
            <NewEvent
                user={myUser}
                expanded={false}
                investigationId="0"
                onCancelNewEventClicked={() => null}
                onSaveNewEventClicked={() => null}
            />)
    })

    // test('investigationId, creationDate, type, category, username are properly filled', () => {
    //     // create a mock function for sendNewEventToServer
    //     myNewEventMounted.instance().sendNewEventToServer = jest.fn()
    //     myNewEventMounted.update();

    //     // check that sendNewEventToServer() has been called
    //     myNewEventMounted.instance().createEvent();
    //     expect(myNewEventMounted.instance().sendNewEventToServer).toHaveBeenCalledTimes(1)

    //     // check that the expected new event is created.
    //     let expectedNewEvent = {
    //         investigationId: "0",
    //         content: [
    //             {
    //                 format: "plain",
    //                 text: null
    //             },
    //             {
    //                 format: "html",
    //                 text: null
    //             }
    //         ],
    //         creationDate: Date(),
    //         type: 'annotation',
    //         category: 'comment',
    //         title: "",
    //         username: 'testUsername'
    //     }
    //     expect(myNewEventMounted.instance().sendNewEventToServer).toHaveBeenCalledWith(expectedNewEvent)
    // })

    // test('The new title (not "") provided by the user is saved', () => {
    //     // create a mock function for sendNewEventToServer
    //     myNewEventMounted.instance().sendNewEventToServer = jest.fn()
    //     myNewEventMounted.update();

    //     // mimic the user typing some text in the input as a new title for the event.
    //     myNewEventMounted.find("input").instance().value = 'new title typed by the user'

    //     // call the method which creates the new event
    //     myNewEventMounted.instance().createEvent();


    //     // check that the expected new event is created.
    //     let expectedNewEvent = {
    //         investigationId: "0",
    //         content: [
    //             {
    //                 format: "plain",
    //                 text: null
    //             },
    //             {
    //                 format: "html",
    //                 text: null
    //             }
    //         ],
    //         creationDate: Date(),
    //         type: 'annotation',
    //         category: 'comment',
    //         title: 'new title typed by the user',
    //         username: 'testUsername'
    //     }
    //     expect(myNewEventMounted.instance().sendNewEventToServer).toHaveBeenCalledWith(expectedNewEvent)
    // })

})
