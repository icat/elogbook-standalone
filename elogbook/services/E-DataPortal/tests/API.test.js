import ICATPLUS from '../src/config/icat/icatPlus'
import { getTagsByInvestigationId, createTagsByInvestigationId, updateTagsByInvestigationId } from '../src/api/icat/icatPlus';

describe("API/icatPlus.js", () => {
    it("Tests getTagsByInvestigationId", () => {
        let actualInvestigationId = "123456789";
        let actualSessionId = "sessionIdTest";
        let expectedURL = ICATPLUS.server + "/logbook/sessionIdTest/investigation/id/123456789/tag";

        expect(getTagsByInvestigationId(actualSessionId, actualInvestigationId)).toEqual(expectedURL);
    })

    it("Tests createTagsByInvestigationId", () => {
        let actualInvestigationId = "123456789";
        let actualSessionId = "sessionIdTest";
        let expectedURL = ICATPLUS.server + "/logbook/sessionIdTest/investigation/id/123456789/tag/create";

        expect(createTagsByInvestigationId(actualSessionId, actualInvestigationId)).toEqual(expectedURL);
    })

    it("Tests updateTagsByInvestigationId", () => {
        let actualInvestigationId = "123456789";
        let actualSessionId = "sessionIdTest";
        let tagId = 'tagId';
        let expectedURL = ICATPLUS.server + "/logbook/sessionIdTest/investigation/id/123456789/tag/id/tagId/tag/update";

        expect(updateTagsByInvestigationId(actualSessionId, actualInvestigationId, tagId)).toEqual(expectedURL);
    })

})