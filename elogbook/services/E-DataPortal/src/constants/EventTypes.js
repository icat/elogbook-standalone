/* Constants mapping  the corresponding concepts as they are stored in the DB */
export const ANNOTATION = 'annotation';
export const NOTIFICATION = 'notification';

export const EVENT_CATEGORY_COMMENT = 'comment';
export const EVENT_CATEGORY_COMMANDLINE = 'commandLine';

export const LIST_VIEW = 'list';
export const DOC_VIEW = 'doc';

export const NEW_EVENT_VISIBLE = 'newEventExpanded';
export const NEW_EVENT_INVISIBLE = 'newEventCollapsed';


export const EDIT_EVENT_VISIBLE = 'editEventExpanded';
export const EDIT_EVENT_INVISIBLE = 'editEventCollapsed';


export const SORT_EVENTS_FROM_YOUNGEST = -1;
export const SORT_EVENTS_FROM_OLDEST = 1;

export const READ_MODE = 'event in basic read mode';
export const EDIT_MODE = 'event in edition mode';

export const BASIC_EVENT_CONTEXT = 'basicEventContext';
//export const DETAILED_EVENT_CONTEXT = 'detailedEventContext';
export const NEW_EVENT_CONTEXT = 'newEventContext';
export const EDIT_EVENT_CONTEXT = 'editEventContext';
export const EVENT_HISTORY_ORIGINAL_VERSION_CONTEXT = 'eventHistoryOriginalVersionContext';
export const EVENT_HISTORY_LATEST_VERSION_CONTEXT = 'eventHistoryLatestVersionContext';
export const EVENT_HISTORY_MIDDLE_VERSION_CONTEXT = 'eventHistoryNotOriginalAndNotLatestVersionContext';
export const TAG_MANAGER_CONTEXT = 'tagManagerContext';
export const EDIT_TAG_CONTEXT = 'editTagContext';
export const NEW_TAG_CONTEXT = 'newTagContext';
export const LOGBOOK_CONTAINER_CONTEXT = 'logbookContainerContext';
export const EVENTLIST_CONTEXT = 'eventListContext';

export const INFO_MESSAGE_TYPE = 'info';
export const ERROR_MESSAGE_TYPE = 'error';

/* Constants used to manage data reception status. To be Applied to all kind of data. */
export const FETCHED_STATUS = 'dataFetchedSuccessfully';
export const FETCHING_STATUS = 'dataBeingFetched';

export const LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_PLAINTEXT_FORMAT = 'localstorageKeyNewEventContentInPlaintextFormat';
export const LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_HTML_FORMAT = 'localstorageKeyNewEventContentInHtmlFormat';
export const LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_PLAINTEXT_FORMAT = 'localstorageKeyEditedEventContentInPlaintextFormat';
export const LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_HTML_FORMAT = 'localstorageKeyEditedEventContentInHtmlFormat';

export const PLAINTEXT_CONTENT_FORMAT = 'plainTextContentFormat';
export const HTML_CONTENT_FORMAT = 'htmlContentFormat';

