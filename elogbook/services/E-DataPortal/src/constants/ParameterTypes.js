export const VOLUME = "__volume";
export const DATASET_COUNT = "__datasetCount";
export const FILE_COUNT = "__fileCount";
export const SAMPLE_COUNT = "__sampleCount";
export const TITLE = "title";
export const INVESTIGATION_NAMES = "investigationNames";
export const INSTRUMENT_NAMES = "instrumentNames";
