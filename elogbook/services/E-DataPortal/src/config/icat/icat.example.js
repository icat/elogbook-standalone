/**
 * icat.example.js configuration file
 */
var ICAT =
{
    /** Connexion settings for none anonymous users */
    connection: {
        /** ICAT plugin used for authentication. */
        plugin: 'esrf'
    },
    /** Anonymous user's credentials*/
    anonymous: {
        /** ICAT plugin used for authentication. */
        plugin: '**',
        /** Username for the anonymous user */
        username: '***',
        /** Password for the anonymous user */
        password: '****'
    }
};

export default ICAT;