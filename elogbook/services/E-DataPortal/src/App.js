import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import "babel-polyfill";
import { Panel, Glyphicon } from 'react-bootstrap';
import LoginContainer from './containers/LoginContainer.js';
import ExpirationLoginContainer from './containers/ExpirationLoginContainer.js';
import InvestigationsContainer from './containers/InvestigationsContainer.js';
import DataCollectionsContainer from './containers/DataCollectionsContainer.js';
import CameraContainer from './containers/CameraContainer.js';
import MenuContainer from './containers/MenuContainer.js';
import InvestigationContainer from './containers/Investigation/InvestigationContainer.js';
import SearchContainer from './containers/SearchContainer.js';
import SelectionContainer from './containers/Selection/SelectionContainer.js';
import MintSelectionContainer from './containers/Selection/MintSelectionContainer.js';
import Footer from './components/Footer.js';
import { BrowserRouter as Router, Route } from "react-router-dom";
import TagContainer from './containers/Logbook/TagContainer';
import { TAG_MANAGER_CONTEXT } from './constants/EventTypes';
import _ from 'lodash';
import queryString from 'query-string';

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path="/" component={MyDataPage} />

                    <Route exact path="/home" component={MyDataPage} />

                    <Route exact path="/search" component={MySearchPage} />

                    <Route exact path="/investigations" render={props => (<MyDataPage location={props.location} {...this.props}  />)} />
                    

                    <Route exact path="/public" render={props => (<OpenDataPage {...this.props} />)} />
                    <Route exact path="/public/:prefix/:suffix" component={DOIPage} />

                    <Route exact path="/closed" render={props => (<ClosedDataPage {...this.props} />)} />

                    <Route exact path="/investigation/:investigationId/datasets" component={DatasetsPage} />
                    <Route exact path="/investigation/:investigationId/events/tagManager" component={EventTagPage} />
                    <Route exact path="/investigation/:investigationId/events" component={EventsPage} />
                    <Route exact path="/investigation/:investigationId/camera" component={CameraPage} />

                    <Route exact path="/selection" component={SelectionPage} />
                    <Route exact path="/selection/mint" component={MintSelectionPage} />

                    <Footer></Footer>
                </div>
            </Router>
        );
    }

}

class DOIPage extends React.Component {
    render() {
        return (
            <div>
                <MenuContainer />
                <LoginContainer />
                <ExpirationLoginContainer></ExpirationLoginContainer>
                <InvestigationContainer doi={this.props.match.params.prefix + "/" + this.props.match.params.suffix} isDOI={true} activeTab={1} {...this.props} />

            </div>
        );
    }
}


class SelectionPage extends React.Component {
    render() {
        return (
            <div>
                <MenuContainer />
                <LoginContainer />
                <ExpirationLoginContainer></ExpirationLoginContainer>
                <SelectionContainer {...this.props} />
            </div>
        );
    }
}

class MintSelectionPage extends React.Component {
    render() {
        return (
            <div>
                <MenuContainer />
                <LoginContainer />
                <ExpirationLoginContainer></ExpirationLoginContainer>
                <MintSelectionContainer investigationId={this.props.match.params.investigationId} {...this.props} />
            </div>
        );
    }
}


class MySearchPage extends React.Component {
    render() {
        return (
            <div>
                <MenuContainer />
                <LoginContainer />
                <ExpirationLoginContainer></ExpirationLoginContainer>

                <div style={{ marginTop: '30px', marginLeft: '30px', marginRight: '30px' }}>
                    <SearchContainer linkProposal={true} openData={true} investigations={this.props.myInvestigations} />
                </div>


            </div>
        );
    }
}

class MyDataPage extends React.Component {
    render() {
        var investigations = this.props.myInvestigations;        
        var params=queryString.parse(this.props.location.search);
        /** Filter by beamline Name */
        if (params.beamline){
            var _this = this;
            
            investigations = {
                data : _.filter(this.props.scientistInstrumentInvestigations.data, function(inv){ return inv.investigationInstruments[0].instrument.name == params.beamline;}),
                fetching : false,
                fetched : true
            }
        }

        return (
            <div >
                <MenuContainer />
                <LoginContainer />
                <ExpirationLoginContainer></ExpirationLoginContainer>
                <div style={{ marginTop: '30px', marginLeft: '30px', marginRight: '30px' }}>
                    <InvestigationsContainer showInvestigationStats={true} linkProposal={true} openData={true} investigations={investigations} />
                </div>
            </div>
        );
    }
}

class ClosedDataPage extends React.Component {
    render() {
        if ((!this.props.user) || (!this.props.user.sessionId)) {
            return <div>
                <MenuContainer />
                <LoginContainer />
            </div>;
        }
        return (
            <div>
                <MenuContainer />
                <LoginContainer />
                <ExpirationLoginContainer></ExpirationLoginContainer>

                <Panel bsStyle='danger' style={{ marginTop: '30px', marginLeft: '30px', marginRight: '30px' }}>
                    <Panel.Heading>
                        <Panel.Title componentClass="h2"> <Glyphicon glyph="lock" style={{ marginRight: "10px" }} />These investigations are under embargo</Panel.Title>
                    </Panel.Heading>
                    <Panel>
                        <Panel.Body>
                            <InvestigationsContainer showInvestigationStats={this.props.user.isAdministrator} linkProposal={false} investigations={this.props.investigations} openData={false} {...this.props} />
                        </Panel.Body>
                    </Panel>
                </Panel>
            </div>
        );
    }
}

class OpenDataPage extends React.Component {
    render() {
        if ((!this.props.user) || (!this.props.user.sessionId)) {
            return <div>
                <MenuContainer />
                <LoginContainer />
            </div>;
        }

        var filterNotPublications = function (data) {
            return _.filter(data, function (o) { return _.find(o.parameters, function (p) { return (p.name == "instrumentNames" && p.value == "PUBLISHER") }) });
        }
        var filterPublications = function (data) {
            return _.filter(data, function (o) { return _.find(o.parameters, function (p) { return (p.name == "instrumentNames" && p.value != "PUBLISHER") }) });
        }

        return (
            <div>
                <MenuContainer />
                <LoginContainer />
                <ExpirationLoginContainer></ExpirationLoginContainer>
                <Panel bsStyle="primary" style={{ marginTop: '30px', marginLeft: '30px', marginRight: '30px' }}>
                    <Panel.Heading >
                        <Panel.Title componentClass="h2"> <Glyphicon glyph="eye-open" /> Open Data Collections</Panel.Title>
                    </Panel.Heading>

                    <Panel.Body>
                        <DataCollectionsContainer filterFunction={filterPublications} datacollections={this.props.datacollections} openData={true} />
                    </Panel.Body>

                </Panel>


                <Panel bsStyle="success" style={{ marginTop: '30px', marginLeft: '30px', marginRight: '30px' }}>
                    <Panel.Heading >
                        <Panel.Title componentClass="h2"> <Glyphicon glyph="eye-open" /> Public Documents</Panel.Title>
                    </Panel.Heading>

                    <Panel.Body>
                        <DataCollectionsContainer filterFunction={filterNotPublications} datacollections={this.props.datacollections} openData={true} />
                    </Panel.Body>

                </Panel>

                <Panel bsStyle="primary" style={{ marginTop: '30px', marginLeft: '30px', marginRight: '30px' }}>
                    <Panel.Heading >
                        <Panel.Title componentClass="h2"> <Glyphicon glyph="eye-open" /> Open Investigations</Panel.Title>
                    </Panel.Heading>

                    <Panel.Body>
                        <InvestigationsContainer
                            linkProposal={false}
                            investigations={this.props.releasedInvestigations}
                            closedData={false}
                        />
                    </Panel.Body>

                </Panel>

            </div>
        );
    }
}


function DatasetsPage(props) {
    return (
        <div>
            <MenuContainer />
            <LoginContainer />
            <ExpirationLoginContainer></ExpirationLoginContainer>
            <InvestigationContainer activeTab={1} investigationId={props.match.params.investigationId} {...props} />
        </div>
    );
}

function EventsPage(props) {
    return (
        <div>
            <MenuContainer />
            <LoginContainer />
            <ExpirationLoginContainer></ExpirationLoginContainer>
            <InvestigationContainer activeTab={3} investigationId={props.match.params.investigationId} {...props} />
        </div>
    );
}

function EventTagPage(props) {
    return (
        <div>
            <MenuContainer />
            <LoginContainer />
            <ExpirationLoginContainer></ExpirationLoginContainer>
            <TagContainer investigationId={props.match.params.investigationId} context={TAG_MANAGER_CONTEXT} />
        </div>
    );
}

function CameraPage(props) {
    return (
        <div>
            <CameraContainer investigationId={props.match.params.investigationId} />
        </div>
    );
}

function mapStateToProps(state) {
    return {
        user: state.user,
        selection: state.selection,
        datasets: state.datasets,
        investigations: state.investigations,
        myInvestigations: state.myInvestigations,
        scientistInstrumentInvestigations: state.scientistInstrumentInvestigations,
        releasedInvestigations: state.releasedInvestigations
    };
}



export default (connect(
    mapStateToProps
)(App));

