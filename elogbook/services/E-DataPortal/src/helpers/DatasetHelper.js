import _ from 'lodash';
import { convertParameterNameToAlias } from './Parameters.js';


export function getDatasetParameterByName(dataset, parameterName) {
    var parameter = _.find(dataset.parameters, function (o) { return o.name === parameterName });
    if (parameter) {
      if (parameter.value) {
        return parameter.value;
      }
    }
    return;
};

export function getDatasetParameterByPrefixName(dataset, prefix) {
    var parameters =  _.filter(JSON.parse(JSON.stringify(dataset.parameters)), function (o) { return o.name.startsWith(prefix) });    
    return   _.map(parameters, function(parameter){      
        parameter.name = convertParameterNameToAlias(parameter.name);
        return parameter;
    });        
    
};

export function getDatasetParameterValueByName(dataset, name){
  var sampleNameParameter = dataset.parameters.filter(function (o) { return o.name === name });
  if (sampleNameParameter){
      if (sampleNameParameter[0]){
          return sampleNameParameter[0].value;
      }
  }
}

