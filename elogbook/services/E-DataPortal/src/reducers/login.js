import Moment from 'moment';
import {
  LOG_IN,
  LOGGED_IN,
  LOG_OUT,
  LOGIN_ERROR
} from '../constants/ActionTypes'


const initialState =  {username: null, fullName:null, name:null, isAdministrator:false, "sessionId": null, "isAuthenticating":false, "error": null, "expirationTime":null}

const user = (state = initialState, action) => {  
      
  switch (action.type) {
    case LOG_IN: {                      
        state = {...state,  username: action.username, isAuthenticating : true, error : null, expirationTime : null};                  
        break;
    }
    case LOGGED_IN:{                      
        state = {...state, 
            username: action.username, 
            isAdministrator:action.isAdministrator, 
            fullName:action.fullName, 
            name:action.name,
            sessionId: action.sessionId, 
            isAuthenticating : false, 
            error : null, 
            expirationTime : Moment().add(action.lifeTimeMinutes, 'minutes').format("MMMM Do YYYY, h:mm:ss a")}                 
        break;  
    }
    case LOG_OUT: {        
        state = {};
        break;  
    }
    case LOGIN_ERROR: {       
        state = {...state, username: null, fullName:null, name:null, isAdministrator:false, "sessionId": null, "isAuthenticating":false, "error": null, "expirationTime":null}               
        break;
    }
    default:
        break;
  }
  return state
}

export default user
