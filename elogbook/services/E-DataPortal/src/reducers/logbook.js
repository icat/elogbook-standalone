import { SET_AVAILABLE_TAGS, CLEAR_AVAILABLE_TAGS } from '../constants/ActionTypes';
import { FETCHING_STATUS, FETCHED_STATUS } from '../constants/EventTypes';

// initialize the logbook redux state
const initialState = {
    /* all available tags for that investigation */
    availableTags: [],
    availableTagsReceptionStatus: FETCHING_STATUS,
};

/**
 *  The logbook reducer. THe reducer takes the current state and the action and returns the new state 
 */
const logbookReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_AVAILABLE_TAGS: {
            state = {
                ...state,
                availableTags: action.payload,
                availableTagsReceptionStatus: FETCHED_STATUS
            };
            break;
        }

        case CLEAR_AVAILABLE_TAGS: {
            state = {
                ...state,
                availableTags: [],
                availableTagsReceptionStatus: FETCHING_STATUS
            };
        }
        default: break; // leave the state unchanged when the action should have no impact on current logbook store state
    };

    return state;
};

export default logbookReducer;
