import {
  LOG_OUT,  FECTH_DATASETS_BY_DOI_PENDING, FECTH_DATASETS_BY_DOI_FULFILLED, 
  FECTH_DATASETS_BY_INVESTIGATION_PENDING, FECTH_DATASETS_BY_INVESTIGATION_FULFILLED,
  FECTH_DATASETS_BY_INVESTIGATION_REJECTED
} from '../constants/ActionTypes'




const initialState =  {data : [], fetching: false, fetched: false}

const datasets = (state = initialState, action) => {    
  
  switch (action.type) {
   
    case FECTH_DATASETS_BY_INVESTIGATION_PENDING:{
        state = {...state, data: [], fetched: false, fetching: true};        
        break;
    }
    case FECTH_DATASETS_BY_INVESTIGATION_FULFILLED:{        
        state = {...state,  data : action.payload.data.reverse(), fetched : true, fetching : false};      
        break;
    }

    case FECTH_DATASETS_BY_INVESTIGATION_REJECTED:{
        state = {...state, data:[], fetched : false, fetching : false};      
        break;
    }
    

    case FECTH_DATASETS_BY_DOI_PENDING:{        
        state = {...state, data: [], fetched: false, fetching: true};   
        break;
    }
    case FECTH_DATASETS_BY_DOI_FULFILLED:{               
        state = {...state,  data : action.payload.data, fetched : true, fetching : false};              
        break;
    }


     case LOG_OUT: {        
        state = {...state, "data" : [], fetching : false, fetched : false};
        break;  
    }
    
    default:
        break;
  }
  return state;
}

export default datasets;
