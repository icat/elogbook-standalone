import {   
  LOG_OUT,
  FETCH_INSTRUMENT_SCIENTIST_INVESTIGATIONS_FULFILLED,
  FETCH_INSTRUMENT_SCIENTIST_INVESTIGATIONS_PENDING
} from '../constants/ActionTypes'


const initialState =  {"fetching": false, "fetched": false,"data":[], "error": null}

const scientistInstrumentInvestigations = (state = initialState, action) => {      
      
  switch (action.type) {
            

    case FETCH_INSTRUMENT_SCIENTIST_INVESTIGATIONS_PENDING: {           
        state = {...state,  fetched: false, fetching: true};
        break;
    }
    case FETCH_INSTRUMENT_SCIENTIST_INVESTIGATIONS_FULFILLED: {                                            
         state = {...state, data: action.payload.data.map((object,i ) => object.Investigation), fetched: true, fetching: false}                
        break;
    }   
    case LOG_OUT: {        
        state = {...state, "fetching": false, "fetched": false, data: []};   
        break;  
    }
    default:
        break;
  }
  return state;
}

export default scientistInstrumentInvestigations;


