import { SET_AVAILABLE_TAGS, CLEAR_AVAILABLE_TAGS } from '../constants/ActionTypes';

/**
 * Action used to set available tags
 * @param {array} availableTags available tags
 * @return {object} action
 */
export function setAvailableTagAction(availableTags) {
    return {
        type: SET_AVAILABLE_TAGS,
        payload: availableTags
    };
}

export function clearAvailableTagAction() {
    return {
        type: CLEAR_AVAILABLE_TAGS,
    };
}