import axios from "axios";
import { getInvestigationsByUser, getEmbargoedInvestigations, getReleasedInvestigations, getInvestigationsByInstrumentScientist } from '../api/icat/icatPlus.js';
import { FECTH_INVESTIGATIONS, FECTH_MY_INVESTIGATIONS, FECTH_RELEASED_INVESTIGATIONS, FETCH_INSTRUMENT_SCIENTIST_INVESTIGATIONS } from '../constants/ActionTypes';

export function fetchMyInvestigations(sessionId) {
    return {
        type: FECTH_MY_INVESTIGATIONS,
        payload: axios.get(getInvestigationsByUser(sessionId))
    };
}

export function fecthEmbargoedInvestigations(sessionId) {
    return {
        type: FECTH_INVESTIGATIONS,
        payload: axios.get(getEmbargoedInvestigations(sessionId))
    };
}

export function fecthReleasedInvestigations(sessionId) {
    return {
        type: FECTH_RELEASED_INVESTIGATIONS,
        payload: axios.get(getReleasedInvestigations(sessionId))
    };
}

export function fecthInvestigationsAsInstrumentScientist(sessionId) {
    return {
        type: FETCH_INSTRUMENT_SCIENTIST_INVESTIGATIONS,
        payload: axios.get(getInvestigationsByInstrumentScientist(sessionId))
    };
}

