import axios from "axios";
import { getDatasetByDOI, getDatasetsByInvestigationId } from '../api/icat/icatPlus.js';
import { FECTH_DATASETS_BY_DOI, FECTH_DATASETS_BY_INVESTIGATION } from '../constants/ActionTypes';

export function fetchDatasetsByDOI(sessionId, doi) {
    return function (dispatch) {
        dispatch({
            type: FECTH_DATASETS_BY_DOI,
            payload: axios.get(getDatasetByDOI(sessionId, doi))
        });
    }
}


export function fetchDatasetsByInvestigationId(sessionId, investigationId) {
    return function (dispatch) {
        dispatch({
            type: FECTH_DATASETS_BY_INVESTIGATION,
            payload: axios.get(getDatasetsByInvestigationId(sessionId, investigationId))
        });
    }
}



