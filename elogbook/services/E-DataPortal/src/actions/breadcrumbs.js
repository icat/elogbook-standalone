import {
  SET_BREADCRUMBS,
  APPEND_BREADCRUMB,
  CLEAN_BREADCRUMBS
} from '../constants/ActionTypes'


export function setBreadCrumbs(breadcrumbs) { 
  return {
    type: SET_BREADCRUMBS,
    breadcrumbs
  };
}

export function appendBreadCrumb(breadcrumb) {   
  return {
    type: APPEND_BREADCRUMB,
    breadcrumb
  };
}

export function CleanBreadcrumbs() {
  return {
    type: CLEAN_BREADCRUMBS
  };
}