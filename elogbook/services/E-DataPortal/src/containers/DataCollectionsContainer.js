import React, { Component } from 'react';
import { connect } from 'react-redux';
import DataCollectionTable from "../components/DataCollection/DataCollectionTable.js"
import Loader from 'react-loader-advanced';
import { setBreadCrumbs } from '../actions/breadcrumbs.js';


class DataCollectionsContainer extends Component {
    render() {
        if ((!this.props.user) || (!this.props.user.sessionId)) {
            return null;
        }
        var data = this.props.datacollections.data;
        if (this.props.filterFunction){
            data = this.props.filterFunction(this.props.datacollections.data);
        }
        return (
            <div>
                <Loader show={this.props.datacollections.fetching} >
                    <DataCollectionTable fetching={this.props.datacollections.fetching} datacollections={data}></DataCollectionTable>
                </Loader>
            </div>);
    }

    componentDidMount() {        
        this.props.setBreadCrumbs([
            { name: 'Open Data', link: '/public' },
            { name: 'Investigations', link: '/public' },
        ]);
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        datacollections: state.datacollections
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setBreadCrumbs: breadcrumbs => dispatch(setBreadCrumbs(breadcrumbs))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DataCollectionsContainer);







