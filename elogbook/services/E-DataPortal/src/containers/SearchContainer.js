import React, { Component } from 'react';
import ICATPLUS from '../config/icat/icatPlus.js';
import { connect } from 'react-redux';
import { setBreadCrumbs } from '../actions/breadcrumbs.js';
import { ReactiveBase, RangeInput, DateRange, MultiList,  CategorySearch, ResultCard, SingleDropdownList } from '@appbaseio/reactivesearch';
import moment from 'moment';
import { Label, Grid, Row, Col, Table } from 'react-bootstrap';
import { getFileByEventId } from '../api/icat/icatPlus.js';
import noimage from '../images/noimage.png';
import TECHNIQUES from '../config/techniques/techniques.js';
import _ from 'lodash';
import { stringifyBytesSize } from '../components/Helpers.js';
class SearchContainer extends Component {
  componentDidMount(){
      this.props.setBreadCrumbs([{ name: 'Search', link: '/search' }]);
  }
  getTechniqueColor(definition){
      if (definition){
        if (TECHNIQUES){            
            var technique = _.find(TECHNIQUES, function(technique){ return technique.shortname == definition});
            if (technique){
                return technique.color;
            }
        }
      }
      return "white";
  }
  getDatasetImageURL(data){
      /**Getting the image */
        var url = null;
        if (data.ResourcesGallery){
            var images = data.ResourcesGallery.split(" ");
            if (images){
                if (images.length > 0){
                    url = getFileByEventId(this.props.user.sessionId, data.investigationId, images[0] );                                        
                }
            }
        }
        return url;
  }
  getTitle(data){
    var styleTechnique = {
        backgroundColor : this.getTechniqueColor(data.definition),
        fontSize : "10px"
    }
    var redirect = "/investigation/" + data.investigationId +"/datasets";
    return <div>                              
                <Grid fluid>                     
                    <Row>
                        <Col sm={4}><Label style={styleTechnique}> {data.definition} </Label></Col> 
                        <Col sm={4}></Col> 
                        <Col sm={4}  style={{ fontSize : "12px"}}><Label bsStyle="primary"> DATASET </Label></Col>                        
                    </Row>                    
                    <Row style={{marginTop:'10px'}}>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Name</Col>                        
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}><a href={redirect}>{data.name}</a></Col>                        
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Sample</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.sampleName}</Col>                        
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Proposal</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.investigationName}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Date</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{moment(data.startDate).format('DD/MM/YYYY')}</Col>                        
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Beamline</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.investigationVisitId}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Files</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.fileCount}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Volume</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{stringifyBytesSize(data.volume)}</Col>                            
                    </Row>

                    
                </Grid>	
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>;
  }

  getFullTitle(data){
    var styleTechnique = {
        backgroundColor : this.getTechniqueColor(data.definition),
        fontSize : "10px"
    }
    
    var redirect = "/investigation/" + data.investigationId +"/datasets";
    return <div>                              
                <Grid fluid>                   
                    <Row>
                        <Col sm={4}><Label style={styleTechnique}> {data.definition} </Label></Col> 
                        <Col sm={4}></Col> 
                        <Col sm={4}  style={{ fontSize : "12px"}}><Label bsStyle="primary"> DATASET </Label></Col>                        
                    </Row>  
                                
                    <Row style={{marginTop:'10px'}}>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Name</Col>                        
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}><a href={redirect}>{data.name}</a></Col>                        
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Sample</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.sampleName}</Col>                        
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Proposal</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.investigationName}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Date</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{moment(data.startDate).format('DD/MM/YYYY')}</Col>                        
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Beamline</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.investigationVisitId}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Files</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.fileCount}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Volume</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{stringifyBytesSize(data.volume)}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Mode</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.InstrumentSource_mode}</Col>                            
                    </Row>
                 
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Type</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.InstrumentMonochromatorCrystal_type}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Reflection</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.InstrumentMonochromatorCrystal_reflection}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Usage</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.InstrumentMonochromatorCrystal_usage}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Energy</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.InstrumentMonochromator_energy}</Col>                            
                    </Row>
                    <Row>
                        <Col sm={3} style={{textAlign:'left', color:'gray', fontSize : "11px"}}>Wavelength</Col> 
                        <Col sm={9} style={{textAlign:'left', fontSize : "11px"}}>{data.InstrumentMonochromator_wavelength}</Col>                            
                    </Row>
                </Grid>	
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>;
  }


  getDescription(data){    
    var redirect = "/investigation/" + data.investigationId +"/datasets";   
    return   <div style={{marginTop:'15px'}} className="flex column justify-space-between text-center">
                <div>
                    <div>	                                                
                        <span className="authors-list"><a href={redirect}>{data.investigationSummary}</a></span>                          
                </div>	
                <br />	
                </div>					                    
            </div>;
  }

  renderDataset(data){        
    var image = this.getDatasetImageURL(data);
    if (image){
        return {
            title: this.getTitle(data),
            image : image,
            description: this.getDescription(data)                
        };
    }
    else{
        return {
            title: this.getFullTitle(data),               
            description: this.getDescription(data)                
        };
    }
	
  }
  render() {     
     if ((!this.props.user) || (!this.props.user.sessionId) ) {         
      return null;
    }      
    var url = ICATPLUS.server + "/elasticsearch/" + this.props.user.sessionId + "/";    
    return (<div>
    <ReactiveBase  app="datasets"      
     url = {url}
     themePreset="light">
        <Grid fluid >
            <Row>
                <Col sm={2}></Col>
                <Col xs={10}>
                        <CategorySearch
                            componentId="searchbox"
                            dataField={["investigationName", "investigationSummary","name", "definition", "sampleName", "scanType"]}
                            categoryField="investigationName.keyword"
                            queryFormat="or"
                            placeholder="Search by dataset name, sample name or investigation title" />
                </Col>
                
            </Row>
            <Row>
                <Col sm={2}>                  
                    <br />                   
                    <DateRange
                        componentId="DateSensor"
                        dataField="startDate"
                        title="Filter by date"                                              
                    />
                     <br />
                                      
                   <MultiList
                        componentId="BeamlineSensor"
                        dataField="investigationVisitId.keyword"
                        title="Beamlines"
                        react={{
                            and: ["DateSensor", "DefinitionSensor"]
                          }}
                    />
                     <br />
                    <MultiList
                        componentId="DefinitionSensor"
                        dataField="definition.keyword"
                        title="Technique"
                        react={{
                            and: ["DateSensor", "BeamlineSensor"]
                          }}
                    />
                
                </Col>
                <Col sm={10}>
                    <ResultCard
                        componentId="result"
                        title="Results"
                        dataField="investigationName"
                        from={0}
                        size={20}
                        paginationAt="top"
                        pagination={false}
                        stream={true}  
                        showResultStats={true}
                        react={{
                            //and: ["SingleDropdownListUsage", "RangeSliderSensorWavelength", "searchbox", "DateSensor", "BeamlineSensor", "DefinitionSensor", "RangeSliderSensor", "SingleDropdownListReflection"]
                            //and: ["searchbox", "DateSensor", "BeamlineSensor", "DefinitionSensor"]
                            //and: ["searchbox", "DateSensor", "BeamlineSensor", "DefinitionSensor", "SingleDropdownListUsage", "SingleDropdownListType", "SingleDropdownListReflection", "RangeSliderSensorEnergy", "RangeSliderSensorWavelength"]
                            and: ["searchbox", "DateSensor", "BeamlineSensor", "DefinitionSensor"]
                        }}
                        onData={(res) => {                            
                            return this.renderDataset(res);
                        }}
                    />
                    
                </Col>  
                  
            </Row>
        </Grid>                  				   
    </ReactiveBase>
</div>);
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {  
    setBreadCrumbs: breadcrumbs => dispatch(setBreadCrumbs(breadcrumbs))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchContainer);







