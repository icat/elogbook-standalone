import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoginForm from '../components/Login/LoginForm';
import { doSignIn, doLogOut } from '../actions/login.js';
import { Modal, Grid, Row, Col } from 'react-bootstrap';
//import Style from '../App.css';
import moment from 'moment';

/**
 * LoginContainer shows the welcome page and the login form on the right
 * It will be only displayed if username or sessionId are null
 */
class ExpirationLoginContainer extends Component {
  render() {          
    if (this.props.user.expirationTime) {
      var diff = moment(this.props.user.expirationTime, 'MMMM Do YYYY, h:mm:ss a').diff(moment());
      if (diff < 0) {
        return <Modal.Dialog autoFocus>
          <Modal.Header>
            <Modal.Title> 
            <Grid fluid >
              <Row>               
                <Col xs={12} md={12} style={{  textAlign: 'center' }}>
                  <h3> Your session has expired</h3>
                </Col>
              </Row>
            </Grid></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Grid fluid >
              <Row>            
                <Col xs={12} md={12} >
                  <LoginForm {...this.props} doSignIn={this.props.doSignIn}></LoginForm>
                </Col>
                
              </Row>
            </Grid> </Modal.Body>
        </Modal.Dialog>;
      }
    }
    return null;
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
     doSignIn: bindActionCreators(doSignIn, dispatch),
      doLogOut: bindActionCreators(doLogOut, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpirationLoginContainer);