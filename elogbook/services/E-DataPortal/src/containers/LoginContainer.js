import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { doSignIn, doLogOut } from '../actions/login.js';
import LoginForm from '../components/Login/LoginForm';
import { Grid, Row, Col } from 'react-bootstrap';
import style from './LoginContainer.css'


/**
 * LoginContainer shows the welcome page and the login form on the right
 * It will be only displayed if username or sessionId are null
 */
class LoginContainer extends Component {
  render() {
    if (this.props.user.username == null || this.props.user.sessionId == null) {
      return <Grid fluid className="App-top-margin">
        <Row>
          <Col xs={12} md={12}>
            
            
            <Grid fluid >

              <Row>
              
    <Col xs={12} md={6} className="mainText" style={{ textAlign: 'center' }}>                 
                    <span className="highlighted">Explore, search, and download</span><br />data and metadata from your experiments <br /> and from public<span className="highlighted"> Open Data</span>
                  </Col>
                  <Col xs={12} md={3} style={{ textAlign: 'center', borderLeft:'1px solid black' }}>
                    <LoginForm {...this.props} doSignIn={this.props.doSignIn}></LoginForm>
                  </Col>
              </Row>            
            
            </Grid>
          </Col>
          <Col xs={1} md={1}>
          </Col>
          <Col xs={12} md={3} style={{ borderLeft: '1px solid #e6e6e6' }}>

            
          </Col>
          <Col xs={1} md={1} >
          </Col>
        </Row>
      </Grid>;
    }
    return null;
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    doSignIn: bindActionCreators(doSignIn, dispatch),
    doLogOut: bindActionCreators(doLogOut, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);