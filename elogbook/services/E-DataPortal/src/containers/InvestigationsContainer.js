import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setBreadCrumbs } from '../actions/breadcrumbs.js';
import InvestigationTable from "../components/Investigation/InvestigationTable.js"
import Loader from 'react-loader-advanced';


class InvestigationsContainer extends Component {
  componentDidMount(){
    if (this.props.closedData){
      this.props.setBreadCrumbs([{ name: 'Closed Data', link: '/investigations' },{ name: 'Investigations', link: '/investigations' }]);
    }
    else{
       this.props.setBreadCrumbs([{ name: 'My Data', link: '/investigations' },{ name: 'Investigations', link: '/investigations' }]);
    }
  }

  render() {               
    if ((!this.props.user) || (!this.props.user.sessionId) || (!this.props.investigations)) {         
      return null;
    }

    return (
      <div style={{marginBottom:100}}>
        <Loader show={this.props.investigations.fetching} >
          <InvestigationTable user={this.props.user} 
                            linkProposal={this.props.linkProposal} 
                            closedData={this.props.closedData} 
                            fetching={this.props.investigations.fetching} 
                            investigations={this.props.investigations.data}
                            showInvestigationStats={this.props.showInvestigationStats}>
          </InvestigationTable>
        </Loader>
      </div>);
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  };
}

function mapDispatchToProps(dispatch) {
  return {  
    setBreadCrumbs: bindActionCreators(setBreadCrumbs, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InvestigationsContainer);







