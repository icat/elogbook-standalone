import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { doLogOut } from '../actions/login.js';
import { Menu } from '../components/Menu/Menu.js'

class MenuContainer extends Component {
    render() {
        return <Menu
            breadcrumbsList={this.props.breadcrumbsList}
            selection={this.props.selection}
            doLogOut={this.props.doLogOut}
            user={this.props.user}
            myInvestigations={this.props.myInvestigations}
            investigations={this.props.investigations}
            datacollections={this.props.datacollections}
            scientistInstrumentInvestigations={this.props.scientistInstrumentInvestigations}
        >
        </Menu>;
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        myInvestigations: state.myInvestigations,
        investigations: state.investigations,
        selection: state.selection,
        breadcrumbsList: state.breadcrumbsList,
        datacollections: state.datacollections,
        scientistInstrumentInvestigations : state.scientistInstrumentInvestigations
    };
}

function mapDispatchToProps(dispatch) {
    return {
        doLogOut: bindActionCreators(doLogOut, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MenuContainer);