import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Button, Label } from 'react-bootstrap';
import { INFO_MESSAGE_TYPE } from '../../../constants/EventTypes';

import _ from 'lodash';
import UserMessage from '../../UserMessage';

import './TagList.css';

/**
 * React component which renders a list of tags in a panel
 */
class TagListPanel extends React.Component {
    render() {
        let { availableTags, onEditButtonClicked } = this.props;

        if (!availableTags || availableTags.length === 0) {
            return (
                <Grid fluid={true} >
                    <h3> Tags currently available in this proposal</h3>
                    <Row style={{ marginLeft: '30px', marginRight: '30px', marginTop: '20px' }}>
                        <UserMessage
                            message='There is no tag in this proposal yet.'
                            isTextCentered={true}
                            type={INFO_MESSAGE_TYPE}
                        />
                    </Row>
                </Grid >
            );
        } else {
            let detailedTags = [];
            let sortedAvailableTags = _.sortBy(availableTags, ['name']);
            detailedTags = sortedAvailableTags.map((tag, index) => { return (<TagListPanelLine key={index} tag={tag} onEditButtonClicked={onEditButtonClicked} />); });
            return (
                <div style={{ paddingBottom: '70px' }}>
                    <Grid fluid={true} >
                        <h3> Tags currently available in this proposal</h3>
                        <Row style={{ marginTop: '20px' }}>
                            <Col xs={0} md={1}> </Col>
                            <Col xs={12} md={10}>
                                {detailedTags}
                            </Col>

                            <Col xs={0} md={1}> </Col>

                        </Row>
                    </Grid >
                </div>);
        };
    }
}

TagListPanel.propTypes = {
    /* List of available tags (can be null when there is no available tags yet)*/
    availableTags: PropTypes.array,
    /* Callback function trigered when the user click to edit the tag. Used only in the contexct of tag management */
    editTag: PropTypes.func,
};


class TagListPanelLine extends React.Component {
    render() {
        let { tag, onEditButtonClicked } = this.props;

        //set the tag scope
        let tagScope = null;
        if (tag.investigationId) {
            tagScope = <span style={{ padding: '5px', backgroundColor: '#f2f2f2' }}> proposal </span>
        } else if (tag.beamlineName) {
            tagScope = <span style={{ padding: '5px', backgroundColor: '#f2f2f2' }}> beamline </span>
        }

        return (
            <div>
                <Row>
                    <div style={{ alignItems: 'center' }}>
                        <Col xs={12} sm={2} style={{ textAlign: 'center' }}>
                            <Label
                                style={{
                                    backgroundColor: tag.color,
                                    borderRadius: '10px',
                                    display: 'inline-block',
                                    paddingBottom: '5px',
                                    fontSize: '14px'
                                }}>
                                {tag.name}
                            </Label>
                        </Col>

                        <Col xs={12} sm={6} style={{ alignItems: 'stretch', }}>
                            {tag.description ? <span> {tag.description} </span> : <span style={{ color: 'gray', fontStyle: 'italic' }}> No description yet</span>}
                        </Col>

                        <Col xs={12} sm={3}>
                            {tagScope}
                        </Col>

                        <Col xs={12} sm={1}>
                            <Button bsStyle="primary" bsSize="small" onClick={() => onEditButtonClicked(tag)}>Edit</Button>
                        </Col>
                    </div>
                </Row>
                <hr />
            </div>
        )
    }
}

TagListPanelLine.proptype = {
    /** tag object */
    tag: PropTypes.object,
    /** callback function when the user click the edit button */
    onEditButtonClicked: PropTypes.func
}

export default TagListPanel;
