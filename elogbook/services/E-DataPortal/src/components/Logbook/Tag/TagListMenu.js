import React from 'react';
import PropTypes from 'prop-types';
import { ButtonToolbar, Grid, Row, Col } from 'react-bootstrap';
import EventListMenuButton from '../Menu/EventListMenuButton';

/**
 * The menu displayed above the tag list
 */
class TagListMenu extends React.Component {
    render() {
        return (
            <Grid fluid={true} style={{ marginTop: '10px' }}>
                <Row>
                    <Col xs={2}>
                        <ButtonToolbar>
                            <div onClick={this.props.onNewTagButtonClicked}>
                                <EventListMenuButton
                                    text='New'
                                    tooltipText='Create a new tag'
                                    glyph='plus'
                                    isEnabled={true} />
                            </div>
                        </ButtonToolbar>
                    </Col>
                </Row>
            </Grid >
        );
    }
}

TagListMenu.propTypes = {
    /** Callback function used to display new tag form */
    onNewTagClicked: PropTypes.func
};

export default TagListMenu;