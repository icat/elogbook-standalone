import React from 'react';
import Proptype from 'prop-types';
import TagLabel from './TagLabel';
import ScrollMenu from 'react-horizontal-scrolling-menu';

/** React component which renders tags in line */
class TagListInLine extends React.Component {

    render() {
        let { hasHorizontalScrolls, tags } = this.props;

        if (tags && tags instanceof Array && tags.length !== null) {
            let tagLabels = tags.map((tag) => {
                return (<TagLabel
                    key={tag._id}
                    onDeleteTagClicked={this.props.onDeleteTagClicked ? this.props.onDeleteTagClicked : null}
                    showDeleteButton={this.props.showDeleteButton === true ? true : false}
                    tag={tag} />);
            })

            if (hasHorizontalScrolls === true) {
                return (
                    <ScrollMenu
                        data={tagLabels}
                        arrowLeft={<div className='arrow-prev'> &#60; </div>}
                        arrowRight={<div className='arrow-next'> &#62; </div>}
                        wrapperClass='customWrapperClass'
                        innerWrapperClass='customInnerWrapperClass'
                    />)
            } else {
                return tagLabels;
            }
        }
        return null;
    }
}

TagListInLine.proptype = {
    /** whether an horizontal scroll is displayed */
    hasHorizontalScrolls: Proptype.bool,
    /** Callback function triggered when the user clicks on the delete button of a tag. */
    onDeleteTagClicked: Proptype.func,
    /** Whether or not the delete button is shown for the tags. */
    showDeleteButton: Proptype.bool,
    /** all tag objects to be rendered */
    tags: Proptype.array
}

TagListInLine.defaultProps = {
    hasHorizontalScrolls: false,
}
export default TagListInLine;