import { DOC_VIEW } from '../../../constants/EventTypes';
import React from 'react';
import PropTypes from 'prop-types'
import _ from 'lodash'
import Moment from 'moment'
import { getContent, convertImagesToThumbnails } from '../../../helpers/EventHelpers';
import { getOriginalEvent } from '../../../helpers/EventHelpers'
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import { getEventIcon } from '../../../helpers/EventHelpers';
import CategoryIcon from './CategoryIcon.js';
require("./eventList.css");

/**
 * The list of the all events
 */
class EventTimeLine extends React.Component {
    /** Returns the list of days and statistics */
    getItems() {
        var days = {};
        for (var i = 0; i < this.props.events.length; i++) {
            var date = Moment(getOriginalEvent(this.props.events[i]).creationDate).format("MMMM Do YYYY");

            if (days[date] == null) {
                days[date] = {
                    commandLine: 0,
                    info: 0,
                    error: 0,
                    comment: 0
                };
            }

            switch (this.props.events[i].category) {
                case "commandLine":
                    days[date].commandLine = days[date].commandLine + 1;
                    break;
                case "error":
                    days[date].error = days[date].error + 1;
                    break;
                case "info":
                    days[date].info = days[date].info + 1;
                    break;
                case "comment":
                    days[date].comment = days[date].comment + 1;
                    break;
            }



        }

        return days;
    }

    getNode(category, items, key) {
        if (items[key][category] != 0) {
            return <td style={{ width: '25px' }}>
                {items[key][category]}
            </td>;
        }
        return <td style={{ width: '25px' }}></td>;
    }

    getNodeIcon(category, items, key) {
        if (items[key][category] != 0) {
            return <td style={{ width: '25px' }}>
                <CategoryIcon category={category}> </CategoryIcon>
            </td>;
        }
        return <td style={{ width: '25px' }}></td>;
    }

    render() {
        if (!this.props.events || this.props.events.length === 0) {
            return null;
        }
        var items = this.getItems();

        return (
            <VerticalTimeline layout='1-column' className='vertical-time-line-custom-general' animate={false}>
                {Object.keys(items).map((key, index) => {
                    return <VerticalTimelineElement
                        className="vertical-timeline-element-custom"
                        key={key}
                        iconStyle={{ top: '15px', left: '15px', width: '10px', height: '10px', background: 'rgb(33, 150, 243)', color: '#ccc' }}>
                        <div>
                            <a href={'#' + key}> {key}</a>
                            <table style={{ fontSize: '14px' }}>
                                <tbody>
                                    <tr>
                                        {this.getNode("commandLine", items, key)}
                                        {this.getNodeIcon("commandLine", items, key)}
                                        {this.getNode("error", items, key)}
                                        {this.getNodeIcon("error", items, key)}
                                        {this.getNode("info", items, key)}
                                        {this.getNodeIcon("info", items, key)}
                                        {this.getNode("comment", items, key)}
                                        {this.getNodeIcon("comment", items, key)}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </VerticalTimelineElement>;

                })}
            </VerticalTimeline>
        )
    }

}

EventTimeLine.propTypes = {
    /** the array of unsorted events as provided by the ICAT+ server */
    events: PropTypes.array
}

export default EventTimeLine;
