import React from 'react';
import { getEventIcon } from '../../../helpers/EventHelpers';

class CategoryIcon extends React.Component {
    render() {                
        return (<div className='pull-left' style={{ paddingRight: '8px' }}>
                    {getEventIcon(this.props.category, "14")}
                </div>)                            
    }
}


export default CategoryIcon;