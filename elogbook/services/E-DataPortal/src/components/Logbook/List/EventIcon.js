import React from 'react';
import CategoryIcon from './CategoryIcon.js';


/**
 * A basic event of the logbook
 */
class EventIcon extends React.Component {
    render() {                
        return <CategoryIcon category={this.props.event.category}></CategoryIcon>
    }
}


export default EventIcon;