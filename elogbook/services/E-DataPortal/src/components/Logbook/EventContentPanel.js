import React from 'react';
import { EDIT_EVENT_CONTEXT, ANNOTATION, NOTIFICATION, NEW_EVENT_CONTEXT, PLAINTEXT_CONTENT_FORMAT, LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_PLAINTEXT_FORMAT, HTML_CONTENT_FORMAT, LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_HTML_FORMAT, LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_HTML_FORMAT, LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_PLAINTEXT_FORMAT } from '../../constants/EventTypes';
import EventContentDisplayer from './EventContentDisplayer';
import PropTypes from 'prop-types';
import { getPreviousVersionNumber } from '../../helpers/EventHelpers';
import HTMLEditor from './HTMLEditor';

/**
 * React component which render the content panel of an event
 */
class EventContentPanel extends React.Component {
    constructor(props) {
        super(props);
        if (props.event) { this.eventId = props.event._id; }; //required for component will unmount in EDIT_EVENT_CONTEXT

        this.storeToLocalStorage=this.storeToLocalStorage.bind(this);
    }
    render() {
        let { event, context, investigationId, user, onEventModified } = this.props;
        if (context === EDIT_EVENT_CONTEXT) {
            if (event) {
                if (event.type === ANNOTATION) {
                    return (<EventContentDisplayer
                        onEventModified={onEventModified}
                        content={event.content}
                        eventId={event._id}
                        investigationId={investigationId}
                        isEditionMode={true}
                        storeToLocalStorage={this.storeToLocalStorage}
                        useRichTextEditor={true}
                        user={user}
                    />);
                }

                if (event.type === NOTIFICATION) {
                    return (<EventContentDisplayer
                        content={getPreviousVersionNumber(event) === 0 ? [{ format: 'html', text: '<p> </p>' }] : event.content}
                        eventId={event._id}
                        investigationId={investigationId}
                        isEditionMode={true}
                        onEventModified={onEventModified}
                        storeToLocalStorage={this.storeToLocalStorage}
                        useRichTextEditor={true}
                        user={user} />);
                }
            }
        } else if (context === NEW_EVENT_CONTEXT) {
            return (<HTMLEditor
                onEventModified={onEventModified}
                investigationId={investigationId}
                isEditionMode={true}
                storeToLocalStorage={this.storeToLocalStorage}
                user={user}
            />
            );
        }
        return null;
    }

    componentWillUnmount() {
        if (this.props.context === NEW_EVENT_CONTEXT) {
            localStorage.removeItem(LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_PLAINTEXT_FORMAT);
            localStorage.removeItem(LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_HTML_FORMAT);
        }

        if (this.eventId && this.props.context === EDIT_EVENT_CONTEXT) {
            localStorage.removeItem(LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_PLAINTEXT_FORMAT + this.eventId);
            localStorage.removeItem(LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_HTML_FORMAT + this.eventId);
        }
    }

    /**
     * Callback function used to store data to the localstorage
     * @param {string} context
     * @param {string} content content data to store
     * @param {string} contentFormat data format
     */
    storeToLocalStorage(context, content, contentFormat) {
        if (context && contentFormat) {
            if (context === NEW_EVENT_CONTEXT) {
                if (contentFormat === PLAINTEXT_CONTENT_FORMAT) {
                    localStorage.setItem(LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_PLAINTEXT_FORMAT, content);
                } else if (contentFormat === HTML_CONTENT_FORMAT) {
                    localStorage.setItem(LOCALSTORAGE_KEY_NEW_EVENT_CONTENT_IN_HTML_FORMAT, content);
                }
            }
            if (context === EDIT_EVENT_CONTEXT) {
                if (contentFormat === PLAINTEXT_CONTENT_FORMAT) {
                    localStorage.setItem(LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_PLAINTEXT_FORMAT + this.props.event._id, content);
                } else if (contentFormat === HTML_CONTENT_FORMAT) {
                    localStorage.setItem(LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_HTML_FORMAT + this.props.event._id, content);
                }
            }
        }
    }
}

export default EventContentPanel;

EventContentPanel.proptype = {
    /** Callback function which returns the min or max editor height of the editor. */
    getEditorHeight: PropTypes.func,
    /* investigation identifier */
    investigationId: PropTypes.string,
    /* user who is using this component */
    user: PropTypes.object,
}