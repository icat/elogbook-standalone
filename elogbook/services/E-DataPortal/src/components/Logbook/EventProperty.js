import React from 'react'

/**
 * This represents event properties such as authorship, tags updating time for example
 */
class EventProperty extends React.Component {

    render() {
        const propertyName = this.props.propertyName;
        const propertyValue = this.props.propertyValue;

        function leftPart() {
            // if (propertyName === 'creationAuthorship') {
            //     return <Glyphicon glyph='user' />
            // }
        }

        function middlePart() {
            if (propertyName === 'creationAuthorship') {
                return <span className='padding-left-5' style={{ fontStyle: 'italic' }}> Created by {propertyValue} </span>
            }
            if (propertyName === 'updateAuthorship') {
                return <span className='padding-left-5' style={{ fontStyle: 'italic' }}>  Updated by {propertyValue} </span>
            }
            if (propertyName === 'historyAuthorship') {
                return <span className='padding-left-5' style={{ fontStyle: 'italic' }}>  Commented by {propertyValue} </span>
            }
        }


        function rightPart() {
            /* this is temporarily desactivated because there is yet no action associated to the ...*/

            // if (propertyName === 'creationAuthorship') {
            //     return (<span style={{ backgroundColor: '#FFFFFF' }}>...</span>)

            // }
            // if (propertyName === 'updateAuthorship') {
            //     return (<span style={{ backgroundColor: '#FFFFFF' }}>...</span>)

            // }
            return <span> &nbsp; </span>
        }

        return (
            <span style={{ border: 'solid 1px #FFFFFF' }} >
                {leftPart()}
                {middlePart()}
                {rightPart()}
            </ span>
        )
    }
}

export default EventProperty