import React from 'react';
import PropTypes from 'prop-types';
import { getOriginalEvent, getEventIcon, getLastCommentContent, getEventCreationDate, getEventHistoryCreationDate, getContent } from '../../helpers/EventHelpers';
import EventContentDisplayer from './EventContentDisplayer';
import { ANNOTATION, NOTIFICATION, EDIT_MODE, DOC_VIEW, BASIC_EVENT_CONTEXT } from '../../constants/EventTypes';

import { OverlayTrigger, Tooltip, Dropdown, MenuItem, Button } from 'react-bootstrap';
import TagContainer from '../../containers/TagContainer';

require('./event.css');


/**
 * A basic event of the logbook
 */
class BasicEvent extends React.Component {
    render() {

        function showContent() {
            if (event.content && (getContent(event.content, 'plainText') !== null || getContent(event.content, 'html') !== null)) {
                if (event.type === ANNOTATION) {
                    return (<AnnotationContent event={event} view={view} />);
                } else if (event.type === NOTIFICATION) {
                    return (<NotificationContent event={event} />);
                };
            } else {
                return (<div>
                    <div style={{ flexGrow: '1', marginLeft: '10px', paddingTop: '4px', paddingBottom: '4px' }} >
                        <Icon event={event} view={view} />
                    </div>

                    <p style={{ margin: '0px', color: '#777777', fontStyle: 'italic' }}>This event has no content</p>
                </div>
                );
            };
        };

        let { event, toggleMode, view, investigationId } = this.props;
        let paddingTopDate = (event.type === ANNOTATION) ? '6px' : '0px';

        return (
            <div
                id='contentDocList'
                style={{ display: 'flex' }} >
                <Button onClick={() => { this.props.onEventClicked(event); }} />
                <div
                    id={(this.props.isSelected === true) ? 'selectedEvent' : ''}
                    style={{ flexGrow: '0', flexShrink: '0' }}
                >
                    <OverlayTrigger
                        placement='right'
                        overlay={
                            <Tooltip id='tooltip'>
                                <p> Event created on {getEventHistoryCreationDate(getOriginalEvent(event))} </p>
                            </Tooltip>
                        }>
                        <div>
                            <div style={{ paddingTop: paddingTopDate, paddingLeft: '10px', color: '#666666', display: 'inline' }}>
                                {getEventCreationDate(getOriginalEvent(event))}
                            </div>
                            <Dropdown id='dropdown-custom-1' >
                                <Dropdown.Toggle bsStyle='link' style={{
                                    paddingRight: '2px',
                                    paddingLeft: '4px',
                                    paddingTop: '0px',
                                    paddingBottom: '0px',
                                    backgroundColor: 'transparent',
                                    marginRight: '10px',
                                    color: '#888888'
                                }}>

                                </Dropdown.Toggle >
                                <Dropdown.Menu className='super-colors'>
                                    <MenuItem eventKey='1' onClick={() => this.props.reverseEventsSortingByCreationDate(this.props.event._id)}    >
                                        Reverse sort and highlight this event.
                                    </MenuItem>

                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </OverlayTrigger>
                </div>
                <div style={{ flexGrow: '1', marginLeft: '30px' }}>
                    {showContent()}
                </div>
                {/* <div style={{ flexGrow: '1', flexShrink: '0', flexBasis: '100px' }}>
                    <div style={{ textAlign: 'right' }}>
                        <TagContainer context={BASIC_EVENT_CONTEXT} investigationId={investigationId} event={event} />
                    </div>
                </div> */}
            </div >
        );
    };
}

BasicEvent.propTypes = {
    /* the event object as received from the ICAT+ server */
    event: PropTypes.object.isRequired,
    /** whether this event is selected or not */
    isSelected: PropTypes.bool,
    /* callback functon which trigger a change to another mode */
    toggleMode: PropTypes.func.isRequired,
    /** the user who is currently logged in */
    user: PropTypes.object.isRequired,
    /* the current view */
    view: PropTypes.string.isRequired,
};


const AnnotationContent = (props) => {
    let { event, view } = props;

    return (
        <div style={{ flexGrow: '1', marginLeft: '10px', paddingTop: '4px', paddingBottom: '4px' }} >
            <Icon event={event} view={view} />

            <EventContentDisplayer
                content={event.content}
                useRichTextEditor={false}
                isEditionMode={false}
            />
        </div>
    );
};

const NotificationContent = (props) => {
    let { event } = props;

    let notificationMessage = getOriginalEvent(event);
    let renderedCommentContent;
    let lastCommentContent = getLastCommentContent(event);
    if (lastCommentContent) {
        renderedCommentContent = () => (
            <div style={{ clear: 'left', marginLeft: '30px', paddingTop: '8px', paddingBottom: '8px' }}>
                {/* <div className='pull-left' style={{ paddingRight: '8px' }}>
                    {getEventIcon('comment', '20')}
                </div> */}
                <EventContentDisplayer
                    content={lastCommentContent}
                    useRichTextEditor={false}
                    isEditionMode={false}
                />
            </div>);
    } else { renderedCommentContent = () => null; };

    return (
        <div style={{ flexGrow: '1', marginLeft: '10px' }} >
            <div className='pull-left' style={{ paddingRight: '8px' }}>
                {getEventIcon(event.category, '20')}
            </div>

            <div id='divContainingHTMLNotificationInDocView'>
                <EventContentDisplayer
                    content={notificationMessage.content}
                    useRichTextEditor={false}
                    isEditionMode={false}
                />
            </div>

            {renderedCommentContent()}
        </div>);
};

const Icon = (props) => {
    let { event, view } = props;
    if (view && view === DOC_VIEW) {
        return null;
    };

    return (<div className='pull-left' style={{ paddingRight: '8px' }}>
        {getEventIcon(event.category, '20')}
    </div>
    );
};

export default BasicEvent;