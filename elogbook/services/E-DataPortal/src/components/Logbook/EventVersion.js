import React from 'react'
import PropTypes from 'prop-types';
import { Row, Col, Panel } from 'react-bootstrap'

import EventProperty from './EventProperty'
import EventHeader from './EventHeader'
import EventContentDisplayer from './EventContentDisplayer';
import { EVENT_HISTORY_ORIGINAL_VERSION_CONTEXT, EVENT_HISTORY_LATEST_VERSION_CONTEXT, EVENT_HISTORY_MIDDLE_VERSION_CONTEXT } from '../../constants/EventTypes';


// import Moment from 'moment'

/*
* This class represents an event version.
*/
class EventVersion extends React.Component {
    render() {
        let { event, isOriginalVersion, isLatestVersion } = this.props;
        return (
            <div class='margin-bottom-10'>
                <Panel>
                    <EventHeader event={event} context={(isOriginalVersion) ? EVENT_HISTORY_ORIGINAL_VERSION_CONTEXT : (isLatestVersion) ? EVENT_HISTORY_LATEST_VERSION_CONTEXT : EVENT_HISTORY_MIDDLE_VERSION_CONTEXT} />
                    <Panel.Body>
                        <Row>
                            <Col xs={12}>
                                <EventContentDisplayer
                                    content={event.content}
                                    useRichTextEditor={false}
                                    isEditionMode={false}
                                // user={this.props.user}
                                />
                            </Col>
                        </Row>
                    </Panel.Body>
                    <Panel.Footer>
                        <EventProperty propertyName={(isOriginalVersion) ? "creationAuthorship" : "historyAuthorship"} propertyValue={event.username} />
                    </Panel.Footer>
                </Panel>
            </div>)
    }
}

EventVersion.PropTypes = {
    /** the user who is currently logged in */
    user: PropTypes.string.isRequired,
}

export default EventVersion