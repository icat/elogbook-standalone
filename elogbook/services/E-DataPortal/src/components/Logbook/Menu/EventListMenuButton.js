import React from 'react'
import { Button, Glyphicon, Tooltip, OverlayTrigger } from 'react-bootstrap'
import PropTypes from 'prop-types';

/* A React component which renders a button in the event list menu. It does not handle event associated to the button.*/
class EventListMenuButton extends React.Component {
    render() {
        let { isEnabled, text, glyph, tooltipText } = this.props;
        if (isEnabled === false) {
            //return (<Button bsSize="small" bsStyle="primary" disabled >  <Glyphicon glyph={glyph} />{text} </Button>);
            return (<div className='btn btn-sm btn-primary'> <Glyphicon glyph={glyph} />{text} </div>);
        }

        // default behavior
        return (<OverlayTrigger placement="bottom" overlay={<Tooltip id="tooltip"> {tooltipText} </Tooltip>}>
            {/* <Button bsSize="small" bsStyle="primary" disabled={false} > <Glyphicon glyph={glyph} /> {text} </Button> */}
            <div className='btn btn-sm btn-primary'> <Glyphicon glyph={glyph} /> {text} </div>
        </OverlayTrigger>
        )
    }
}

export default EventListMenuButton;

EventListMenuButton.propTypes = {
    /** the text displayed in the button */
    text: PropTypes.string.isRequired,
    /** glyphicon added on the left side of the displayed text */
    glyph: PropTypes.string.isRequired,
    /** text of the tooltip when the user hover the mouse over the button */
    tooltipText: PropTypes.string,
    /** Whether the button is enabled or disabled */
    isEnabled: PropTypes.bool.isRequired
}