import React from 'react';
import { READ_MODE, EDIT_MODE, ANNOTATION, NOTIFICATION } from '../../constants/EventTypes';
import BasicEvent from './BasicEvent';
import EditEvent from './EditEvent';
import PropTypes from 'prop-types'

/**
 * An event of the logbook
 */
class Event extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            mode: READ_MODE
        }

        this.toggleMode = this.toggleMode.bind(this);
    }
    render() {
        let { investigationId, user, view, event, onEventUpdated } = this.props;

        if (event.type && (event.type === ANNOTATION || event.type === NOTIFICATION)) {
            if (view) {
                if (this.state.mode === READ_MODE) {
                    return <BasicEvent
                        event={event}
                        investigationId={investigationId}
                        isSelected={this.props.isSelected}
                        onEventClicked={this.props.onEventClicked}
                        toggleMode={this.toggleMode}
                        user={user}
                        reverseEventsSortingByCreationDate={this.props.reverseEventsSortingByCreationDate}
                        view={view}
                    />
                } else if (this.state.mode === EDIT_MODE){}
                    // return <DetailedEvent
                    //     event={event}
                    //     investigationId={investigationId}
                    //     onEventUpdated={onEventUpdated}
                    //     toggleMode={this.toggleMode}
                    //     user={user}
                    //     updateEvent={this.props.updateEvent}
                    // />
            } else {
                console.log("[ERROR] the view is not set. This should not happen.");
                return null;
            }
        }
        console.log("[ERROR] event type is unknown or not defined for eventId " + event._id);
        return null
    }

    /**
    * Toggle the mode of the event: READ or EDIT
    * @param {string} mode the requested new mode of the event
    */
    toggleMode(mode) {
        if (mode && this.state.mode !== mode) {
            this.setState({ mode: mode })
        }
    }

}

export default Event;


Event.propTypes = {
    /** the event object as received from the ICAT+ server */
    event: PropTypes.object.isRequired,
    /** the investigationId */
    investigationId: PropTypes.string,
    /* whether this event is selected or not*/
    isSelected: PropTypes.bool,
    /* Callback function triggered when the user updated an event */
    onEventUpdated: PropTypes.func,
    /* callback function which reverse how events are sorted */
    reverseEventsSortingByCreationDate: PropTypes.func,
    /** the user who is currently logged in */
    user: PropTypes.object.isRequired,
    /* the view the event is in */
    view: PropTypes.string
}
