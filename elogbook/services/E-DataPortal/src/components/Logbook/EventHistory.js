import React from 'react';
import { Row, Col, OverlayTrigger, Popover, Button, Well, Label } from 'react-bootstrap';
import { getPreviousVersionNumber, } from '../../helpers/EventHelpers';
import Popup from 'reactjs-popup';
import Moment from 'moment';
import EventVersion from './EventVersion';
import LabeledElement from './LabeledElement';

/**
 * React component which renders event history label and the correwsponding modal.
 */
class EventHistory extends React.Component {
    render() {
        if (this.props.event) {
            return (
                <div >
                    <OverlayTrigger
                        trigger='click' placement='top' overlay={eventVersionsPopover(this.props.event)} rootClose={true} >
                        <div>
                            <LabeledElement data={null} hasNoData={true} type='text' labelText={<span> History <div className='arrow-down' /> </span>} />
                        </div>
                    </OverlayTrigger>
                </div>);
        }
        return null;
    }
}

/**
* Render the popup which displays all event versions of the event
* @param {*} event the event under investigation
        */
function eventVersionsPopover(event) {
    let e = event;

    /* build the different lines representing the previous versions */
    let lines = [];
    while (e.previousVersionEvent != null) {
        if (lines.length === 0) {
            lines.push(<EventVersionItem event={e} type='edition' mostRecent={true} />);
        } else {
            lines.push(<EventVersionItem event={e} type='edition' mostRecent={false} />);
        };
        e = e.previousVersionEvent;
    }
    lines.push(<EventVersionItem event={e} type='creation' />);


    /* build the popover */
    return (<Popover id='mypopover'>
        <div>
            <p style={{ marginBottom: '5px' }}> This log has {getPreviousVersionNumber(event) + 1} versions. </p>
            <hr style={{ marginTop: '5px' }} />
            {lines.map((item, index) => <Popup
                key={index}
                trigger={<div> {item} </div>}
                modal
                closeOnDocumentClick={false}
                contentStyle={{ width: '90%', height: '90%' }}
            >
                {close => (
                    <div className='fullPage'>
                        <a className='close' onClick={close}> &times; </a>
                        <div className='content'>
                            <EventVersion
                                event={item.props.event}
                                isLatestVersion={(index === 1) ? true : false}
                                isOriginalVersion={(index === lines.length - 1) ? true : false}
                            />
                        </div>
                    </div>
                )}
            </Popup>
            )}
            <hr style={{ marginBottom: '5px' }} />

            <Popup
                trigger={<div style={{ display: 'inline' }}> <Button bsStyle='link'> See all versions  </Button> </div>}
                modal
                closeOnDocumentClick={false}
                contentStyle={{ width: '90%', height: '90%' }}
            >
                {close => (
                    <div className='fullPage'>
                        <a className='close' onClick={close}> &times; </a>
                        <div className='content'>
                            <CompleteEventHistory event={event} />
                        </div>
                    </div>
                )}
            </Popup>

        </div>
    </Popover>);
}

/**
 * React component which represents on item of the event history
* @param {*} props  the props passed to this component
                */
class EventVersionItem extends React.Component {
    render() {
        function buildTheDisplay(event, type, mostRecent) {
            return (
                <Well bsSize='small' style={{ marginBottom: '5px', cursor: 'pointer' }}>
                    <b> {event.username} </b>
                    {type === 'creation' ? 'created on ' : ''}
                    {type === 'edition' ? 'edited on ' : ''}
                    {Moment(event.creationDate).format('MMMM DD HH:mm')}
                    {mostRecent === true ? ' (most recent)' : ''}
                    {event.machine ? <span style={{ color: '#777777', fontStyle: 'italic' }}> from {event.machine} </span> : ''}
                </Well>
            );
        };

        return (
            buildTheDisplay(this.props.event, this.props.type, this.props.mostRecent)
        );
    };
};

/**
 * React component which displays the complete event history
 */
const CompleteEventHistory = (props) => {
    let { event } = props;
    let eventVersions = [];

    /* first push the current event which is the latest version of the event*/
    //eventHistoryItems.push(<EventListViewExpanded event={event} isFullPage={true} />)
    eventVersions.push(<EventVersion event={event} isLatestVersion={true} />);
    /* then display the previous version of this event */
    while (event.previousVersionEvent != null) {
        event = event.previousVersionEvent;
        eventVersions.push(<div class='margin-bottom-10'>
            <Row>
                <Col xs={1}> </Col>
                <Col xs={11}>
                    <EventVersion event={event} isOriginalVersion={event.previousVersionEvent === null ? true : false} />
                </Col>
            </Row>
        </div>);
    };

    return eventVersions;
};

export default EventHistory;