import React from 'react'
import { Row, Panel } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { getEventIcon, getEventHistoryCreationDate } from '../../helpers/EventHelpers'
import { NEW_EVENT_CONTEXT, EVENT_HISTORY_ORIGINAL_VERSION_CONTEXT, EVENT_HISTORY_MIDDLE_VERSION_CONTEXT, EVENT_HISTORY_LATEST_VERSION_CONTEXT, EDIT_EVENT_CONTEXT } from '../../constants/EventTypes';

/**
 * A class which represents the event header. This component renders something different in the header based on the context prop. 
 * This class is used in event history or for the creation of a new event. 
 */
class EventHeader extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        let { event, context } = this.props;

        if (event) {
            //happens during the visualisation or edition an event
            if (context === EVENT_HISTORY_ORIGINAL_VERSION_CONTEXT) {
                return (<Panel.Heading >
                    <Row>
                        {/* <div className="pull-right"> <p> Tags not shown </p> </div> */}
                        <div style={{ paddingLeft: '10px' }}> {getEventIcon(event.category)}
                            &nbsp; On {getEventHistoryCreationDate(event)}, the original message was:
               </div>
                    </Row>
                </Panel.Heading>)
            } else if (context === EVENT_HISTORY_LATEST_VERSION_CONTEXT) {
                return (<Panel.Heading >
                    <Row>
                        {/* <div className="pull-right"> <p> Tags not shown </p> </div> */}
                        <div style={{ paddingLeft: '10px' }}> {getEventIcon("previousVersion")}
                            &nbsp; Written on {getEventHistoryCreationDate(event)} (latest version), the current comment is:
                   </div>
                    </Row>
                </Panel.Heading>)
            } else if (context === EVENT_HISTORY_MIDDLE_VERSION_CONTEXT) {
                return (<Panel.Heading >
                    <Row>
                        {/* <div className="pull-right"> <p> Tags not shown </p> </div> */}
                        <div style={{ paddingLeft: '10px' }}> {getEventIcon("previousVersion")}
                            &nbsp; On {getEventHistoryCreationDate(event)}, the comment was:
                   </div>
                    </Row>
                </Panel.Heading>)
            }

        } else if (context === NEW_EVENT_CONTEXT) {
            // happens when a new event is created
            return (<Panel.Heading >
                <b> New comment </b>
            </Panel.Heading>)
        } else if (context === EDIT_EVENT_CONTEXT) {
            return (<Panel.Heading> <b> Edit </b> </Panel.Heading>);
        }
    }
}

EventHeader.propTypes = {
    /** the event object as received from the ICAT+ server. (optional: not present when a new comment is being created.) */
    event: PropTypes.object,
    /* the context indicating what needs to be displayed.*/
    context: PropTypes.string.isRequired,
}
export default EventHeader;
