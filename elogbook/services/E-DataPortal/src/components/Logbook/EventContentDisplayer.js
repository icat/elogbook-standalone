import React from 'react'
import PropTypes from 'prop-types'
import HTMLEditor from './HTMLEditor';
import { getContent } from '../../helpers/EventHelpers';
import { LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_HTML_FORMAT, LOCALSTORAGE_KEY_EDITED_EVENT_CONTENT_IN_PLAINTEXT_FORMAT, EDIT_EVENT_CONTEXT, PLAINTEXT_CONTENT_FORMAT } from '../../constants/EventTypes';


/**
 * This component takes care about displaying the provided content
 * using a rich text editor or not
 * in edition mode or in view mode
 * 
 */
class EventContentDisplayer extends React.Component {
    render() {
        let { isEditionMode, user, content, useRichTextEditor, investigationId, onEventModified, storeToLocalStorage } = this.props;
        if (content && content instanceof Array && content.length !== 0) {
            if (useRichTextEditor !== null && useRichTextEditor !== undefined) {
                let HTMLText = this.getContent(content); // can be null
                if (useRichTextEditor === true) {
                    // save the event plainText format to localstorage. This is usefull in case the editor is not changed (when only title is updated)
                    storeToLocalStorage(EDIT_EVENT_CONTEXT, getContent(content, 'plainText'), PLAINTEXT_CONTENT_FORMAT);
                    return (<HTMLEditor
                        onEventModified={onEventModified}
                        isEditionMode={isEditionMode}
                        investigationId={investigationId}
                        storeToLocalStorage={storeToLocalStorage}
                        text={HTMLText}
                        user={user}
                    />)
                } else if (useRichTextEditor === false) {
                    return (<div dangerouslySetInnerHTML={{ __html: HTMLText }} />)
                }
            }
        }
        return null
    }

    /**
     * Gets the content of the event as an html string. 
     * @param {array} the event content, not null
     * @returns {string} the html formatted content if it exists in the content. The plaintext content surrounded by <p> </p> if it does not exists. Null if none of these exist. 
     */
    getContent(content) {
        let HTMLText = getContent(content, 'html');
        if (HTMLText) {            
            return HTMLText;
        } else {
            let plainText = getContent(content, 'plainText');
            if (plainText) {
                return "<p>" + plainText + "</p>"
            }
        }
        return null;
    }

}

EventContentDisplayer.propTypes = {
    /* the event content which will be displayed. An array containing different usable formats of the content */
    content: PropTypes.array.isRequired,
    /* whether a rich text editor should be used to view the content or not */
    useRichTextEditor: PropTypes.bool.isRequired,
    /* true when the display prupose is edition or creation of a new event. False when the display purpose is viewing an existing event content. */
    isEditionMode: PropTypes.bool.isRequired,
    /* the user currently logged in */
    user: PropTypes.object,
    /** the investigationId of the event being edited. */
    investigationId: PropTypes.string,
    /** callback function called when editor content changed : from no text to text or vice versa, or  when the current text is identical to the original text provided to the editor*/
    canEnableSaveButton: PropTypes.func,
}

export default EventContentDisplayer;