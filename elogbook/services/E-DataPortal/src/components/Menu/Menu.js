import React from 'react';
import { Link } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap'
import { Glyphicon, Nav, NavItem, Navbar, NavDropdown, MenuItem } from 'react-bootstrap';
import logo from '../../images/ebs.gif';
import './Menu.css';
import './glyhicon-spinner.css';
import BreadCrumbs from '../Breadcrumbs/BreadCrumbs.js';
import _ from 'lodash';

export class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.onLogOutClicked = this.onLogOutClicked.bind(this);        
    }

    onLogOutClicked(e) {
        this.props.doLogOut();
    }

    getLoggedOutRender() {
        return <Navbar fluid inverse fixedTop>
            <Navbar.Header>
                <Navbar.Brand>
                    <Link to="/investigations" ><img alt="" src={logo} className='logo' /></Link>
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
        </Navbar>;
    }

    getOpenDataMenuItem() {
        if (this.props.datacollections.fetching) {
            return <LinkContainer to='/public'>
                <NavItem eventKey={3} href='/public'>
                    Public Logbooks<span style={{ marginLeft: '10px' }} className="glyphicon glyphicon-repeat fast-right-spinner"></span>
                </NavItem>
            </LinkContainer>;
        }
        return <LinkContainer to='/public'>
            <NavItem eventKey={3} href='/public'>
                Public Logbooks<span className="badge" style={{ marginLeft: '10px' }}>{this.props.datacollections.data.length}</span>
            </NavItem>
        </LinkContainer>;
    }

    getClosedDataMenuItem() {
        if (this.props.investigations.fetching) {
            return <LinkContainer to='/closed'>
                <NavItem eventKey={4} href='/closed'>
                    Closed Data<span style={{ marginLeft: '10px' }} class="glyphicon glyphicon-repeat fast-right-spinner"></span>
                </NavItem>
            </LinkContainer>;
        }
        return <LinkContainer to='/closed'>
            <NavItem eventKey={4} href='/closed'>
                Closed Data<span className="badge" style={{ marginLeft: '10px' }}>{this.props.investigations.data.length}</span>
            </NavItem>
        </LinkContainer>;

    }

    getManagerMenuItem() {
        if (this.props.scientistInstrumentInvestigations.fetching || this.props.scientistInstrumentInvestigations.data.length === 0) {
            return null;
        }
        var instruments = _.uniq(_.map(this.props.scientistInstrumentInvestigations.data, function(inv){ return inv.investigationInstruments[0].instrument.name})); 


        if (instruments){
            var _this = this;
            function getBeamlineBadge(beamlineName){
                return <span className="badge" style={{ marginLeft: '10px' }}>
                             {_.filter(_this.props.scientistInstrumentInvestigations.data, function(inv){ return inv.investigationInstruments[0].instrument.name === beamlineName}).length}
                        </span>;
            }
            const navDropdownTitle = (<span>Manager <span className="badge" style={{ marginLeft: '10px' }}>{instruments.length}</span> <sub style={{ color: 'red', fontSize: '10px', marginLeft: '5px' }}>NEW</sub></span> );

            return <NavDropdown eventKey={3} title={navDropdownTitle} id="basic-nav-dropdown">
                     {instruments.map((value, index) => {
                        return <LinkContainer to={"/investigations?beamline="+ value}><MenuItem key={index}>{value} {getBeamlineBadge(value)}</MenuItem></LinkContainer>
                    })}
                   
                    </NavDropdown>;
        }   
        return null;            
       

    }

    

    render() {

        /** If there is not sessionId it means that we are not already been authenticated **/
        if ((!this.props.user) || (!this.props.user.sessionId)) {
            return this.getLoggedOutRender();
        }

        return (<div>
            <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/">E-Logbook</a>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                       
                        <LinkContainer to='/investigations'>
                            <NavItem eventKey={3} href='/investigations'>
                                My logbooks<span className="badge" style={{ marginLeft: '10px' }}>{this.props.myInvestigations.data.length}
                                </span>
                            </NavItem>
                        </LinkContainer>
                        {this.getOpenDataMenuItem()}
                       
                                                
                    </Nav>
                    <Nav pullRight>
                        <NavItem eventKey={2} onClick={this.onLogOutClicked} >
                            <Glyphicon glyph="glyphicon glyphicon-log-out" />
                            Log out <strong>{this.props.user.fullName}</strong>
                        </NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <BreadCrumbs breadcrumbsList={this.props.breadcrumbsList}></BreadCrumbs>
        </div>)
    }
}

