import React from 'react';
import { getDatasetParameterByName } from '../../helpers/DatasetHelper.js';
import ParameterTableWidget from './ParameterTableWidget.js';

class InstrumentSlitWidget extends React.Component {
    
 

  getParameter(parameterName){      
      return getDatasetParameterByName(this.props.dataset, parameterName);
  }

  getJSONKeyValuePair(name, value){
      return {
              name : name,
              value : value
          };
  }

  getSlitValues(prefix){      
      return [
          this.getJSONKeyValuePair('Blade Front',  getDatasetParameterByName(this.props.dataset, prefix + "_blade_front")),
          this.getJSONKeyValuePair('Blade Back',  getDatasetParameterByName(this.props.dataset, prefix + "_blade_back")),
          this.getJSONKeyValuePair('Blade Up',  getDatasetParameterByName(this.props.dataset, prefix + "_blade_up")),
          this.getJSONKeyValuePair('Blade Down',  getDatasetParameterByName(this.props.dataset, prefix + "_blade_down")),
          this.getJSONKeyValuePair('Horizontal Gap',  getDatasetParameterByName(this.props.dataset, prefix + "_horizontal_gap")),
          this.getJSONKeyValuePair('Horizontal Offset',  getDatasetParameterByName(this.props.dataset, prefix + "_horizontal_offset")),
          this.getJSONKeyValuePair('Vertical Gap',  getDatasetParameterByName(this.props.dataset, prefix + "_vertical_gap")),
          this.getJSONKeyValuePair('Vertical Offset',  getDatasetParameterByName(this.props.dataset, prefix + "_vertical_offset"))         
      ]
  }

  render(){
        
      return  <div class="container-fluid">
                 <br />           
                    <span style={{fontSize:'16px', fontWeight:'bold'}} >Slits</span>
                <div class="row">
                <div class="col-sm-2">
                        <ParameterTableWidget
                            header="Primary slit"
                            parameters={this.getSlitValues("InstrumentSlitPrimary")}
                        ></ParameterTableWidget>	      
                </div>
                <div class="col-sm-2">
                        <ParameterTableWidget
                            header="Secondary slit"
                            parameters={this.getSlitValues("InstrumentSlitSecondary")}
                        ></ParameterTableWidget>		      
                </div>
                <div class="col-sm-8">
                        <ParameterTableWidget
                            header="Slits"
                            parameters={this.getSlitValues("InstrumentSlits")}
                        ></ParameterTableWidget>	     
                </div>
                </div>
            </div>;

  }
}

export default InstrumentSlitWidget;