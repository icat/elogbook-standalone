import React from 'react';
import _ from 'lodash';
import InstrumentDetectorWidget from './InstrumentDetectorWidget.js';
import InstrumentSlitWidget from './InstrumentSlitWidget.js';
import ParameterTableWidget from './ParameterTableWidget.js';
import { getDatasetParameterByName, getDatasetParameterByPrefixName } from '../../helpers/DatasetHelper.js';

class InstrumentWidget extends React.Component {

  render() {  
    var instrumentParameters = getDatasetParameterByPrefixName(this.props.dataset, "Instrument");
    if (instrumentParameters.length == 0){
        return null;
    }    

    return <div style={{fontSize:'12px'}}>     

      <InstrumentWidgetSection
          header="Monochromator"
          parameters={[
            { name : "Energy",      value : getDatasetParameterByName(this.props.dataset, "InstrumentMonochromator_energy")},
            { name : "Wavelength",  value : getDatasetParameterByName(this.props.dataset, "InstrumentMonochromator_wavelength")},
            { name : "d_spacing",   value : getDatasetParameterByName(this.props.dataset, "InstrumentMonochromatorCrystal_d_spacing")},
            { name : "Reflection",  value : getDatasetParameterByName(this.props.dataset, "InstrumentMonochromatorCrystal_reflection")},
            { name : "Type",        value : getDatasetParameterByName(this.props.dataset, "InstrumentMonochromatorCrystal_type")},
            { name : "Usage",       value : getDatasetParameterByName(this.props.dataset, "InstrumentMonochromatorCrystal_usage")}

          ]}
      ></InstrumentWidgetSection>


      <InstrumentDetectorWidget dataset={this.props.dataset}></InstrumentDetectorWidget>


      <InstrumentSlitWidget dataset={this.props.dataset}></InstrumentSlitWidget>
       
      <InstrumentWidgetSection
          header="Optics"
          names={getDatasetParameterByPrefixName(this.props.dataset, "InstrumentOpticsPositioners_name")}
          values={getDatasetParameterByPrefixName(this.props.dataset, "InstrumentOpticsPositioners_value")}
      >
       </InstrumentWidgetSection>

        <InstrumentWidgetSection
          header="Positioners"
          names={getDatasetParameterByPrefixName(this.props.dataset, "InstrumentPositioners_name")}
          values={getDatasetParameterByPrefixName(this.props.dataset, "InstrumentPositioners_value")}
      >
       </InstrumentWidgetSection>
      
    </div>;        
  }
}

export default InstrumentWidget;



class InstrumentWidgetSection extends React.Component {
  render() { 
    var names = null; 
    var values = null;             
    if (this.props.values && this.props.names){
          /** This is a comma separeted pair of name-values 
          * names = [{"name":"InstrumentOpticsPositioners_name","value":" m2angle m0tz m0tyrot mono enmono r2theta r2gamma "}]
          * values = [{"name":"InstrumentOpticsPositioners_value","value":" 26 -22 6.7 19.244 5.99885 -0.0829958 -0.456489 -0.820325 5.5 0.85 -18.5746 -4.49 5.96497 5.86713 6.06281 5.86713 7.5 100 0.141575 -0.511 NaN NaN NaN NaN NaN NaN 0"}]
          */
          
          if (this.props.values.length > 0 && this.props.names.length > 0){
              names = this.props.names[0].value;
              values = this.props.values[0].value;
          }
    }
    
    if (!names && !values && (!this.props.parameters || this.props.parameters === 0 || !_.find(this.props.parameters, function(parameter){ console.log(parameter.name); return parameter.value }))){
        return null;
    }

    
    
    
    return <div className="container-fluid">     
                    <br />           
                    <span style={{fontSize:'16px', fontWeight:'bold'}} >{this.props.header}</span>
                        <div class="row">
                            <div class="col-sm-2">
                                <ParameterTableWidget names={names} values={values} parameters={this.props.parameters}></ParameterTableWidget>	        
                            </div>  
                        </div>                                        	           
                    </div>;         
  }
}