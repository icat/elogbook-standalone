import React from 'react';
import { Breadcrumb, Label } from 'react-bootstrap';

export class BreadCrumbs extends React.Component {

    render() {
        var items = [];
        if (this.props.breadcrumbsList) {
            if (this.props.breadcrumbsList.items) {
                items = this.props.breadcrumbsList.items;
            }
        }
        /** breadCrumbsSize is used to mark as active the latest item in the list */
        var breadCrumbsSize = items.length;
        
        return (<Breadcrumb style={{ backgroundColor: '#bfbfbf', marginTop: -20, borderBotton: '1px solid #f2f2f2', fontSize: '15px' }}>

            {items.map(function (breadcrumb, i) {
                
                if (i === 0) {
                    return <Breadcrumb.Item key={i} style={{ marginLeft: '40px' }} href={breadcrumb.link}> {breadcrumb.name} </Breadcrumb.Item>;
                }
                if (breadCrumbsSize === (i + 1)) {
                    return <Breadcrumb.Item key={i} active href={breadcrumb.link}>
                        <Label style={{ fontSize: '15px' }}>{breadcrumb.badge}</Label> <span style={{ color:'black'}}>{breadcrumb.name}</span> </Breadcrumb.Item>;
                }
                return <Breadcrumb.Item key={i} href={breadcrumb.link}> {breadcrumb.name} </Breadcrumb.Item>;

            })}
        </Breadcrumb>)
    }
}

export default BreadCrumbs;


