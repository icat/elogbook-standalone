import React from 'react';
import PropTypes from 'prop-types';
import { INFO_MESSAGE_TYPE, SUCCESS_MESSAGE_TYPE, ERROR_MESSAGE_TYPE } from '../constants/UserMessages';
import { Panel } from 'react-bootstrap';

/* React component which displays a message to the user */
class UserMessage extends React.Component {
    render() {
        const { type, message } = this.props;

        if (type && message && message !== '') {
            return (<Panel bsStyle={type === INFO_MESSAGE_TYPE ? 'info' : (type === SUCCESS_MESSAGE_TYPE) ? 'success' : (type === ERROR_MESSAGE_TYPE) ? 'danger' : 'primary'} style={{ marginBottom: '0px' }}>
                <Panel.Heading>
                    {type === INFO_MESSAGE_TYPE ? 'Information message' : (type === SUCCESS_MESSAGE_TYPE) ? 'Information message' : (type === ERROR_MESSAGE_TYPE) ? 'Error message' : 'Message'}
                </Panel.Heading>
                <Panel.Body>
                    {message}
                </Panel.Body>
            </Panel>)
        }
        return null;
    }
}

UserMessage.propTypes = {
    /** the error message itself */
    message: PropTypes.string.isRequired,
    /* the type of the message */
    type: PropTypes.string.isRequired,
};

export default UserMessage;