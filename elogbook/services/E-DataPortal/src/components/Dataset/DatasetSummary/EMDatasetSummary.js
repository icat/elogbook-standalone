import React from 'react';
import Moment from 'react-moment';
import { Grid, Row, Col, Image } from 'react-bootstrap';
import './DatasetSummary.css';
import GalleryDatasetThumbnail from '../GalleryDatasetThumbnail.js'
import { getDatasetParameterValueByName } from '../../../helpers/DatasetHelper.js';
import ParameterTableWidget from '../../Instrument/ParameterTableWidget.js';

class EMDatasetSummary extends React.Component { 
   
    getParameters(){
        return [
            {name : 'Dataset', value : this.props.dataset.name},
            {name : 'Sample', value : this.props.dataset.sampleName},
            {name : 'Protein', value : getDatasetParameterValueByName(this.props.dataset, "EM_protein_acronym")},
            {name : 'Definition', value : getDatasetParameterValueByName(this.props.dataset, "definition")},
            {name : 'Start', value : <Moment parse="YYYY-MM-DD HH:mm" format="LTS">{this.props.dataset.startDate}</Moment>},
            {name : 'Description', value : getDatasetParameterValueByName(this.props.dataset, "Sample_description")}
        ];
    }

    getTechniqueParameters(){
        return [
            
            {name : 'Amplitute', value : getDatasetParameterValueByName(this.props.dataset, "EM_amplitude_contrast") + " %" },
            {name : 'Initial Dose', value : getDatasetParameterValueByName(this.props.dataset, "EM_dose_initial")},
            {name : 'Dose/frame', value : getDatasetParameterValueByName(this.props.dataset, "EM_dose_per_frame")},
            {name : 'Images', value : getDatasetParameterValueByName(this.props.dataset, "EM_images_count")},
            {name : 'Magnification', value : getDatasetParameterValueByName(this.props.dataset, "EM_magnification")},
            {name : 'Sampling Rate', value : getDatasetParameterValueByName(this.props.dataset, "EM_sampling_rate") + " Å/pixel"},
            {name : 'Spherical Ab.', value : getDatasetParameterValueByName(this.props.dataset, "EM_spherical_aberration") + " mm"},
            {name : 'Voltage', value : getDatasetParameterValueByName(this.props.dataset, "EM_voltage")}
            
        ];
    }

    render() {
        
        return (
            <Grid fluid style={{margin:'20px'}}>
                <Row>
                    <Col xs={12} md={2}>                       
                        <ParameterTableWidget striped={false} parameters={this.getParameters()} ></ParameterTableWidget>   

                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <ParameterTableWidget striped={false} parameters={this.getTechniqueParameters()} ></ParameterTableWidget> 
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        
                    </Col>
                  
                    <Col xs={12} sm={12} md={2}>
                         <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={0} ></GalleryDatasetThumbnail>
                    </Col>
                </Row>
            </Grid>


        );
    }
}

export default EMDatasetSummary;