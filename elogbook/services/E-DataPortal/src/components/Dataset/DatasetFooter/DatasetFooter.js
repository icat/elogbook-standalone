import React from 'react';
import { Glyphicon} from 'react-bootstrap';
import './DatasetFooter.css';
import DatasetDownloadButton from '../DatasetDownloadButton.js';

class DatasetFooter extends React.Component {
     render() {
        return (
            <div className="container-fluid">
                <div className="pull-left">
                    <span> <Glyphicon glyph="folder-open" /> {this.props.location}</span>
                </div>
                <div className="pull-right">
                    <DatasetDownloadButton sessionId={this.props.sessionId} id={this.props.dataset.id} ></DatasetDownloadButton>
                </div>
            </div>
        );
    }
}

export default DatasetFooter;