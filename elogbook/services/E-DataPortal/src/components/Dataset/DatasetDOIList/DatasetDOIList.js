import React from 'react';
import axios from "axios";
import { getDataCollectionByDatasetId } from '../../../api/icat/icatPlus.js';
import { DOIBadge } from '../../../components/doi/DOIBadge.js';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Alert } from 'react-bootstrap';

class DatasetDOIList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataCollections: [],
            fetching: false,
            fetched: false
        }
        this.titleFormatter = this.titleFormatter.bind(this);
        this.investigationNamesFormatter = this.investigationNamesFormatter.bind(this);
        this.instrumentNamesFormatter = this.instrumentNamesFormatter.bind(this);
        this.mintedByFormatter = this.mintedByFormatter.bind(this);

    }

    doiFormatter(cell, row) {
        return <DOIBadge doi={cell}></DOIBadge>
    }

    getParameterValue(row, name) {
        var parameter = row.parameters.filter(function (o) { return o.type.name === name });
        if (parameter) {
            if (parameter[0]) {
                return <div>{parameter[0].stringValue}</div>;
            }
        }
    }
    titleFormatter(cell, row) {
        return this.getParameterValue(row, "title");
    }
    investigationNamesFormatter(cell, row) {
        return this.getParameterValue(row, "investigationNames");
    }
    instrumentNamesFormatter(cell, row) {
        return this.getParameterValue(row, "instrumentNames");
    }
    mintedByFormatter(cell, row) {
        return this.getParameterValue(row, "mintedByFullName");
    }


    componentDidMount() {
        axios.get(getDataCollectionByDatasetId(this.props.sessionId, this.props.dataset.id))
            .then(res => {

                let dataCollections = res.data.map(function (dc) { return dc.DataCollection });

                this.setState({
                    dataCollections: dataCollections,
                    fetching: false,
                    fecthed: true
                });
            })
            .catch((error) => {
                if (error.response) {
                    if (error.response.status === 403) {
                        this.props.doLogOut();
                    }
                }
            });

    }
    render() {
        const options = {
            paginationSize: 10,
            sizePerPage: 10,
            paginationShowsTotal: true,
            hidePageListOnlyOnePage: true
        };

        if (this.state.dataCollections.length === 0) {
            return <Alert bsStyle="default">
                This dataset has not got any <strong>DOI</strong>
            </Alert>;
        }
        return <div style={{ marginTop: '20px' }}>
            <BootstrapTable
                data={this.state.dataCollections}
                options={options}
                pagination
                striped
                search
                hover
                condensed>
                <TableHeaderColumn hidden isKey dataField='id'>id</TableHeaderColumn>
                <TableHeaderColumn width='25%' dataFormat={this.doiFormatter} dataField='doi' >DOI</TableHeaderColumn>
                <TableHeaderColumn width='45%' dataFormat={this.titleFormatter} dataField='title' >Title</TableHeaderColumn>
                <TableHeaderColumn width='10%' dataFormat={this.investigationNamesFormatter} dataField='title' >Proposals</TableHeaderColumn>
                <TableHeaderColumn width='10%' dataFormat={this.instrumentNamesFormatter} dataField='title' >Beamlines</TableHeaderColumn>
                <TableHeaderColumn width='10%' dataFormat={this.mintedByFormatter} dataField='title' >Minted by</TableHeaderColumn>

            </BootstrapTable>
        </div>;
    }
}



export default DatasetDOIList;
