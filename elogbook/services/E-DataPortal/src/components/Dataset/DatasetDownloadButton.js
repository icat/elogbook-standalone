import React from 'react';
import axios from 'axios';
import './DatasetTable.css';
import {getDownloadURLByDatasetId} from "../../api/icat/icatPlus"
import { Button } from 'react-bootstrap';

import { getDatasetStatus } from "../../api/icat/icatPlus.js"




import { getDatasetParameterByName } from '../../helpers/DatasetHelper.js';
import _ from 'lodash';


export default class DatasetDownloadButton extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            status: "FETCHING",
            fetching: false,
            fetched: false
        }
    }

    getStatus() {
        axios.get(getDatasetStatus(this.props.sessionId, this.props.id))
            .then(response => {
                this.setState({
                    status: response.data,
                    fetching: false,
                    fetched: true
                });

            })
            .catch((error) => {
                this.setState({
                    status: error.response.data.message + " " + this.props.id,
                    fetching: false,
                    fetched: true
                });
            });
    }

    restore(e) {

        axios.get(getDownloadURLByDatasetId(this.props.sessionId, this.props.id))
            .then(response => {
                this.getStatus();
            })
            .catch((error) => {
                this.getStatus();
            });

    }

    componentDidMount() {
        if (!this.state.fetching) {
            this.setState({
                fetching: true
            });
        }

        this.getStatus();

    }

    render() {
        var downloadURL = getDownloadURLByDatasetId(this.props.sessionId, this.props.id);
        if (this.state.status === 'ONLINE') {
            return <a style={{ color: 'black' }} href={downloadURL}>
                <Button bsStyle="primary" bsSize="xsmall">
                    <span className="glyphicon glyphicon-download-alt"></span>
                    <span style={{ marginLeft: '10px' }} ></span>
                    Download</Button>
            </a>;
        }

        if (this.state.status === 'RESTORING') {
            return <a style={{ color: 'black' }} href={downloadURL}>
                <Button variant="dark" bsSize="xsmall" >
                    <span bsSize="xsmall" className="glyphicon glyphicon-repeat fast-right-spinner"></span>
                    <span style={{ color: 'black', marginLeft: '10px' }} >Restoring</span>
                </Button>
            </a>;
        }

        if (this.state.status === 'ARCHIVED') {
            return <Button variant="light" bsSize="xsmall" onClick={(e) => { this.restore(e) }}>
                <span style={{ color: 'black' }} className="glyphicon glyphicon-tasks"></span>
                <span style={{ color: 'black', marginLeft: '10px' }} >Restore</span>
            </Button>;
        }

        if (this.state.status === 'FETCHING') {
            return <span><span style={{ marginLeft: '10px' }} className="glyphicon glyphicon-repeat fast-right-spinner"></span></span>;
        }

        return <span>{this.state.status}</span>;
    }

};