import React from 'react';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import { Alert, Glyphicon, Form, FormGroup, Col, ControlLabel, FormControl } from 'react-bootstrap/lib';
import Button from 'react-bootstrap-button-loader';
import ICAT from '../../config/icat/icat.js'

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.onSignInClicked = this.onSignInClicked.bind(this);
    this.onSignInAsAnonymous = this.onSignInAsAnonymous.bind(this);

    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.state = {
      isLoading: false,
      error: null
    }
  }

  handleKeyPress(target) {
    if (target.charCode === 13) {
      this.onSignInClicked();
    }
  }
  onSignInClicked(e) {
    this.setState({ isLoading: true });
    this.props.doSignIn(ICAT.connection.plugin, this.loginID.value, this.password.value);

  }

  onSignInAsAnonymous(e) {
    this.setState({ isLoading: true });
    this.props.doSignIn(ICAT.anonymous.plugin, ICAT.anonymous.username, ICAT.anonymous.password);

  }

  componentDidMount() {
    this.setState({ isLoading: false });
  }

  render() {    
    
    var url = window.location.pathname;
    return (
      <div>
        <Form horizontal >
        <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>              
            </Col>
            <Col sm={10}>
               <strong>Sign in with your ESRF's user account</strong>              
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={4}>
              Login
            </Col>
            <Col sm={8}>
              <FormControl type="text" placeholder="LoginID" autoFocus required inputRef={(ref) => { this.loginID = ref; } }/>
            </Col>
          </FormGroup>

          <FormGroup>
            <Col componentClass={ControlLabel} sm={4}>Password</Col>
            <Col sm={8}>
              <FormControl type="password" placeholder="Password" onKeyPress={this.handleKeyPress}
                inputRef={(ref) => { this.password = ref; } } />
            </Col>
          </FormGroup>
        </Form>
        <div className="center-block text-center">
          <Button type="submit" loading={this.props.user.isAuthenticating} bsStyle="primary" onClick={this.onSignInClicked}>
            <Glyphicon glyph="glyphicon glyphicon-log-in" /> Log in
          </Button>
          <br /><br /> 
          <span><Link onClick={this.onSignInAsAnonymous} to={url}> or Log in as anonymous</Link></span>
        </div>
        <div className="center-block text-center" style={{ marginTop: '10px' }}>
          <LoginAlertMessage error={this.props.user.error}></LoginAlertMessage>
        </div>
      </div>

    );
  }
}

class LoginAlertMessage extends React.Component { 
  render() {
    if (!this.props.error) {
      return null;
    }
    return <Alert bsStyle="warning"><h4>{this.props.error}</h4></Alert>;
  }
};


LoginForm.propTypes = {
  username: PropTypes.string
}

export default LoginForm;