/**
 * @swagger
 *
 * components:
 *   schemas:
 *     dataset:
 *       required: 
 *         - "parameters"
 *         - "investigation"
 *         - "id"
 *         - "name"
 *         - "startDate"
 *         - "endDate"
 *         - "location"
 *         - "sampleName"
 *       properties: 
 *         parameters: 
 *           type: "array"
 *           items: 
 *             type: "object"
 *             properties: 
 *               name: 
 *                 type: "string"
 *               value: 
 *                 type: "string"
 *         investigation: 
 *           required: 
 *             - "name"
 *             - "id"
 *           properties: 
 *             name: 
 *               type: "string"
 *             id: 
 *               type: "number"
 *           type: "object"
 *         id: 
 *           type: "number"
 *         name: 
 *           type: "string"
 *         startDate: 
 *           type: "string"
 *         endDate: 
 *           type: "string"
 *         location: 
 *           type: "string"
 *         sampleName: 
 *           type: "string"
 */
    