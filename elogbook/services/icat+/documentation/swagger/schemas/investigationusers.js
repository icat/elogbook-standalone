/**
 * @swagger
 *
 * components:
 *   schemas:    
 *     investigationusers:
 *       required: 
 *         - "name"
 *         - "fullName"
 *         - "role"
 *         - "investigationName"
 *         - "investigationId"
 *       properties: 
 *         name: 
 *           type: "string"
 *         fullName: 
 *           type: "string"
 *         role: 
 *           type: "string"
 *         investigationName: 
 *           type: "string"
 *         investigationId: 
 *           type: "number"
 */