/**
 * @swagger
 *
 * components:
 *   schemas:
 *     event:
 *       description: an event in the logbook
 *       type: object
 *       required:
 *         - type
 *         - category
 *         - investigationId
 *       properties:
 *         _id:
 *           type: integer
 *         investigationId:
 *           type: integer
 *         investigationName:
 *           type: string
 *         instrumentName:
 *           type: string
 *         datasetId:
 *           type: integer
 *         type:
 *           type: string
 *           description: Type of the event
 *           enum:
 *             - notification
 *             - annotation
 *         tags:
 *           type: array
 *           items:
 *             type: string
 *         title:
 *           type: string
 *         category:
 *           type: string
 *           description: Category of the event
 *           enum:
 *             - commandline
 *             - comment
 *             - error
 *             - debug
 *             - info
 *         filepath:
 *           type: string
 *         filename:
 *           type: string
 *         fileSize:
 *           type: string
 *         username:
 *           type: string
 *         content:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *               format:
 *                 type: string
 *                 description: the format of the text
 *               text:
 *                 type: string
 *                 description: the text itself
 *         software:
 *           type: string
 *         machine:
 *           type: string
 *         creationDate:
 *           type: string
 *           format: date
 *         contentType:
 *           type: string
 *         file:
 *           type: string
 *         previousVersionEvent:
 *           type: object
 *     tag:
 *       description: a tag in the logbook
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         _id:
 *           type: integer
 *         name:
 *           type: string
 *         color: 
 *           type: string
 *         description:
 *           type: string
 *         beamlineName:
 *           type: string
 *         investigationId:
 *           type: string
 */