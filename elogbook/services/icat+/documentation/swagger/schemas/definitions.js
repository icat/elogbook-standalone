/**
 * @swagger
 *
 * components:
 *   schemas:    
 *     datacollection:
 *       required: 
 *         - "id"
 *         - "parameters"
 *       properties: 
 *         id: 
 *           type: number
 *         createId: 
 *           type: string
 *         createTime: 
 *           type: string
 *         modId: 
 *           type: string
 *         modTime: 
 *           type: string
 *         dataCollectionDatafiles: 
 *           type: array
 *           items: 
 *             type: "undefined"
 *         dataCollectionDatasets: 
 *           type: array
 *           items: 
 *             type: "undefined"
 *         doi: 
 *           type: string
 *         jobsAsInput: 
 *           type: "array"
 *           items: 
 *             type: "undefined"
 *         jobsAsOutput: 
 *           type: "array"
 *           items: 
 *             type: "undefined"
 *         parameters: 
 *           type: "array"
 *           items: 
 *             type: "object"
 *             properties: 
 *               name: 
 *                 type: string
 *               value: 
 *                 type: string
 *     json-datacite:
 *       required: 
 *         - "doi"
 *         - "url"
 *         - "publicationYear"
 *         - "creators"
 *         - "titles"
 *         - "publisher"
 *         - "subjects"
 *         - "dates"
 *       properties: 
 *         doi: 
 *           type: "string"
 *         url: 
 *           type: "string"
 *         publicationYear: 
 *           type: "string"
 *         creators: 
 *           type: "array"
 *           items: 
 *             type: "object"
 *             properties: 
 *               name: 
 *                 type: "string"
 *         titles: 
 *           required: 
 *             - "title"
 *           properties: 
 *             title: 
 *               type: "string"
 *           type: "object"
 *         publisher: 
 *           type: "string"
 *         subjects: 
 *           type: "array"
 *           items: 
 *             type: "object"
 *             properties: 
 *               subjectScheme: 
 *                 type: "string"
 *         dates: 
 *           type: "array"
 *           items: 
 *             type: "object"
 *             properties: 
 *               date: 
 *                 type: "string"
 *               dateType: 
 *                 type: "string"
 *     cacheStats:
 *       required: 
 *           - "hits"
 *           - "misses"
 *           - "keys"
 *           - "ksize"
 *           - "vsize"
 *       properties: 
 *         hits: 
 *           type: "number"
 *         misses: 
 *           type: "number"
 *         keys: 
 *           type: "number"
 *         ksize: 
 *           type: "number"
 *         vsize: 
 *           type: "number"       
 *     cacheUserStorage:
 *         required: 
 *           - "username"
 *           - "myInvestigations"
 *           - "investigations"
 *           - "investigationsId"
 *         properties: 
 *           username: 
 *             type: "string"
 *           myInvestigations: 
 *             type: "array"
 *             items:
 *               $ref: '#/components/schemas/investigation'
 *           investigations: 
 *             type: "array"
 *             items:
 *               $ref: '#/components/schemas/investigation'
 *           investigationsId: 
 *             type: "array"
 *             items:
 *               $ref: '#/components/schemas/investigation'
 *     session:
 *       type: object
 *       properties:
 *         sessionId:
 *           type: string
 *         login:
 *           type: string
 *         name:
 *           type: string
 *         fullname:
 *           type: string
 *         lifeTimeMinutes:
 *           type: integer
 *         isAdministrator:
 *           type: boolean   
 *       example:
 *         sessionId: b1a04f33-3989-4a12-81b8-6d589ff0c01a
 *         login: reader
 *         name : "reader"
 *         fullName: Anonymous
 *         lifeTimeMinutes: 720
 *         isAdministrator: false    
 *     credentials:
 *       type: object
 *       properties:
 *         username:
 *           type: string
 *         password:
 *           type: string
 *         plugin:
 *           type: string
 *       example:
 *         plugin: db
 *         username: myLogin
 *         password : "myPassword"
 *   requestBodies:
 *     findEvents:
 *       description: Search parameters used to retreive relevant events from the logbook
 *       content:
 *         'application/json':
 *           schema:
 *             description: Find parameters used to search events
 *             properties:
 *               find: 
 *                 description: Contains the mongoose query for the search
 *                 type: object
 *               sort:
 *                 description: Contains the mongoose query indicating how events are sorted.
 *                 type: object
 *               skip:
 *                 description: Number indicating how many events to skip from beginning of the search result
 *                 type: integer
 *               limit:
 *                 description: Maximum number or events to retrieve
 *                 type: integer
 *             example:
 *               find: 
 *                 $and:
 *                   - $or:
 *                     - type: annotation
 *                     - type: notification
 *               sort:
 *                 createdAt: -1
 *               skip: 0
 *               limit: 400
 *       required: true
 *     eventToCreate:
 *       description: New event to create
 *       content:
 *         application/json:
 *           schema: 
 *             $ref: '#/components/schemas/event'
 *     eventToUpdate:
 *       description: Updated event attributes to use to update the event with the corresponding eventId
 *       content:
 *         'application/json':
 *           schema:
 *             type: object
 *             properties:
 *               event:
 *                 allOf:
 *                   - $ref: '#/components/schemas/event'
 *                   - required:
 *                     - _id
 */