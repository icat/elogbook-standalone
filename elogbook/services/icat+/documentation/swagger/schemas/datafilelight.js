/**
 * @swagger
 *
 * components:
 *   schemas:
 *     datafilelight:
 *       required: 
 *         - "id"
 *       properties: 
 *         id: 
 *           type: "number"
 *         fileSize: 
 *           type: "number"
 *         location: 
 *           type: "string"
 *         name: 
 *           type: "string"
 */
    