/**
 * @swagger
 *
 * components:
 *   responses:
 *     tags:
 *       description: Tags of the logbook
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/tag'
 *     tagCreated:
 *       description: Tag which has just been created
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/tag'
 *     tagUpdated:
 *       description: Tag which has just been updated
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/tag'
 *     events:
 *       description: Events corresponding to the search request
 *       content:
 *         application/json:
 *           schema:
 *             type: array
 *             items:
 *               $ref: '#/components/schemas/event'
 *     eventCreated:
 *       description: Event which has just been created
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/event'
 *     sessionInformation:
 *       description: Information about the session
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties: 
 *               remainingMinutes:
 *                 type: number
 *                 format: float
 *               userName:
 *                 type: string
 *           example:
 *             remainingMinutes: 2.1
 *             username: 1259  
 *     error400:
 *       description: Bad request. Some parameters are missing.
 *       content:
 *         text/plain:
 *           schema:
 *             type: string 
 *           example:
 *             Bad request. Some parameters are missing.
 *     error401:
 *       description: Authentication required. The user is not logged in the server.
 *       content:
 *         text/plain:
 *           schema:
 *             type: string 
 *           example:
 *             Authentication required. The user is not logged in the server.
 *     error403:
 *       description: The request was valid, but the server is refusing action. The user might not have the necessary permissions for a resource, or may need an account of some sort.
 *       content:
 *         text/plain:
 *           schema:
 *             type: string 
 *           example:
 *             The user is logged in but does not have the permission to do the required action.
 *     error500:
 *       description: Internal server error.
 *       content:
 *         text/plain:
 *           schema:
 *             type: string 
 *           example:
 *             An error occured on the server side.
 */