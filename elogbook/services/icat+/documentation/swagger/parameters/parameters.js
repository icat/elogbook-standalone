/**
 * @swagger
 *
 * components:
 *   parameters:
 *     investigationId:
 *       in: path
 *       description: Metadata catalogue investigation identifier
 *       name: investigationId
 *       schema: 
 *         type: string
 *       example: 148845606
 *       required: true
 *     sessionId:
 *       in: path
 *       description: session identifier provided by the metadata catalogue
 *       name: sessionId
 *       schema: 
 *         type: string
 *       example: 83825a51-69b8-4eg58-b5fb-bcf79bb412be
 *       required: true
 *     investigationName:
 *       in: path
 *       description: the name of the investigation
 *       name: investigationName
 *       schema: 
 *         type: string
 *       example: MD-837
 *       required: true
 *     instrumentName:
 *       in: path
 *       description: the name of the beamline
 *       name: instrumentName
 *       schema: 
 *         type: string
 *       example: ID01
 *       required: true
 *     datasetId:
 *       in: path
 *       description : Metadata catalogue dataset identifier
 *       name: datasetId
 *       schema : 
 *         type : string
 *       example : 132123     
 *       required: true 
 *     datasetIds:
 *       in: path
 *       description : Metadata catalogue dataset identifier list (comma separated)
 *       name: datasetIds
 *       schema : 
 *         type : string
 *       example : 132123,1523433     
 *       required: true
 *     tagId:
 *       in: path
 *       description: logbook tag identifier
 *       name: tagId
 *       schema:
 *         type: string
 *       example: 5c8fbc5c8504536783dc2bc5
 *       required: true
 */