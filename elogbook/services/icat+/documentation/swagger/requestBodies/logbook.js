/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     createTag:
 *       description: tag to be created
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/tag'
 *     updateTag:
 *       description: updated tag
 *       content:
 *         'application/json':
 *           schema:
 *             $ref: '#/components/schemas/tag'
 */