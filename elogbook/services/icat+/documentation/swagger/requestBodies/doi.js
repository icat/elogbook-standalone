/**
 * @swagger
 *
 * components:
 *   requestBodies:
 *     doiMinting:
 *       description: Metadata required to mint a DOI
 *       content:
 *         'application/json':
 *           schema:
 *             type: object
 *             required:
 *               - title
 *               - abstract
 *               - datasetIdList
 *               - authors
 *             properties:
 *               title:
 *                 description: Title describing the DOI associated data
 *                 type: string
 *               abstract:
 *                 description: Abstract describing the DOI associated data
 *                 type: string
 *               datasetIdList:
 *                 description: Dataset list for which a DOI needs to be minted.
 *                 type: array
 *                 items:
 *                   type: integer
 *                   description: dataset identifier
 *               authors:
 *                 description: Author list
 *                 type: array
 *                 items:
 *                   type: object
 *                   properties:
 *                     name:
 *                       type: string
 *                       description: First name of the author
 *                     surname:
 *                       type: string
 *                       description: Last name of the author
 *                     id:
 *                       type: string
 *                       description: firstName_lastName
 *           example:
 *             title: Structural Evidence for a Role of the Multi-functional Human Glycoprotein Afamin in Wnt Transport
 *             abstract: This is an abstract
 *             datasetIdList:
 *               02566121
 *               1144512
 *             authors:
 *               - John 
 *                 Doe
 *                 John_Doe
 * 
 */