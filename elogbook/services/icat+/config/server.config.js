/**
 * Server configuration
 * @module config
 * @type {string}
 */
module.exports = {
    server: {
        port: 8000,
        url: 'http://localhost:8000',        
        API_KEY: 'be70ac55-fd08-4840-9b29'
    },
    
    database: {
        /** mondgodb URL connection */
         uri: 'mongodb://localhost:27017/icatplus?authSource=admin'
    },

    /** Elastic search configuration section */
    elasticsearch : {    
        enabled : false,        
        server : "http://kolla-vm019.esrf.fr:9200"
    },


    /** Loggin section describing parameters for logging */
    logging : {
        /** Configuration to graylog */
        graylog: {
            /** If false graylog will be ignored */
            enabled: true,
            /** Graylog server host */
            host: 'graylog-dau.esrf.fr',
            /** Graylog server port */
            port: 12205,
            /** Name of the facility */
            facility: 'ESRF'
        },
        console: {
            level: 'silly'
        },
        file : {
            enabled: true,
            filename : '/tmp/server.log',
            level : 'silly'        
        }
    },
    datacite: {
        prefix: '********',
        suffix: '********',
        username: '********',
        password: '********',
        mds: 'https://mds.datacite.org',
        landingPage: 'https://doi.esrf.fr/',
        publisher: 'European Synchrotron Radiation Facility',
        proxy: {
            host: 'proxy.esrf.fr',
            port: '3128'

        }
    },
    ids : {
        server: 'https://ids.esrf.fr'        
    },
    icat: {        
        server: 'https://localhost:8181',           
        maxQueryLimit: 10000,
        authorizations : {

            /**
            * adminUsersReader is capable to get the list of users that are administrators
            * adminUsersReader needs access to tables user and userGroup in order to know who are administrators
            * User can read electronic lookbok for all proposals
            */
            adminUsersReader : {
                administrationGroups: ['admin'],                
                user : {
                    plugin: "db",
                    credentials: [
                        /*
                        { "username": "minter" },
                        { "password": "givemeadoi" }
                        */
                       { username: "icat" },
                       { password: "icat" }
                    ]
                }
            },
            /**
             * This user is allowed to mint DOI's and needs some permission on ICAT tables
             */
            minting : {
                user : {
                    plugin: "db",
                    credentials: [
                        /*
                        { "username": "minter" },
                        { "password": "givemeadoi" }
                        */
                       { username: "icat" },
                       { password: "icat" }                    
                    ]
                },
            },
            parameterListReader : {
                user : {
                    plugin: "db",
                    credentials: [
                        /*
                        { "username": "minter" },
                        { "password": "givemeadoi" }
                        */
                       { username: "icat" },
                       { password: "icat" }
                    ]
                }
            },
             /**
             * notifier needs to get access to all investigations as it will convert investigationName and beamline into investigationId
             */
            notifier: {
                user : {
                    plugin: "db",
                    credentials: [
                        /*{ username: "notifier" },
                        { password: "notifyme" }*/
                        { username: "root" },
                        { password: "root" }
                    ]
                }
            }

        },
       
       
        anonymous: {
            "plugin": "db",
            "credentials": [
                { "username": "reader" },
                { "password": "reader" }
            ]
        }
       
    }
}
