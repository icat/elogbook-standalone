/** Remove this in production */

"use strict";
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const express = require('express');
const bodyParser = require('body-parser');

const swaggerUi = require('swagger-ui-express');
const app = express();
const Grid = require('gridfs-stream');
const cors = require('cors');
const proxy = require('http-proxy-middleware');

global.gConfig = require('./config/server.config.js');

if (new String(process.env.NODE_ENV).toUpperCase() == 'TEST') {
    global.gConfig = require('./test/config/server.config.js');
}


global.gLogger = require('./app/logging/logger.js').logger;


const mongoose = require('mongoose');

const cache = require('./app/cache/cache.js');



global.gLogger.info("Connecting to ICAT " + global.gConfig.icat.server);


app.use(bodyParser.urlencoded({ limit: '500mb', extended: true }))
app.use(bodyParser.text({ type: 'application/x-ndjson' })); // Needed for elastic search msearch
app.use(bodyParser.json({ limit: '500mb', extended: true }))


/** For static content */
app.use(express.static('public'))
app.use('/static', express.static('public'))

/**mongoose */
mongoose.Promise = global.Promise;

// Connecting to the database 
let uri = global.gConfig.database.uri;

if (new String(process.env.NODE_ENV).toUpperCase() == 'TEST') {
    uri = process.env.MONGO_URI;
}

mongoose.connect(uri, { useNewUrlParser: true })
    .then(() => {
        global.gLogger.debug("Successfully connected to the database");
    }).catch(err => {
        global.gLogger.error('Could not connect to the database. Exiting now...', {error : err});
        process.exit();
    });


// use it before all route definitions
app.use(cors({ origin: '*' }));


/** Routes */
app.get('/', (req, res) => {
    res.json({ "message": "Welcome to ICAT+. Take notes quickly. Organize and keep track of all your events and resources." });
});

global.gLogger.debug("Loading routes...");
require('./app/routes/resource.routes.js')(app);
require('./app/routes/session.routes.js')(app);
require('./app/routes/cache.routes.js')(app);
require('./app/routes/logbook.routes.js')(app);
require('./app/routes/catalogue.routes.js')(app);
require('./app/routes/elasticsearch.routes.js')(app);
require('./app/routes/panosc.routes.js')(app);

/** Express Error handling 
 * https://expressjs.com/en/guide/error-handling.html
*/
app.use(function (error, req, res, next) {  
    console.log(error);  
    if (error) {
        if (error.response) {
            if (error.response.status) {
                if (error.response.data) {
                    if (error.response.data.message) {
                        res.status(error.response.status).send(error.response.data.message);
                        return;
                    }
                }
                res.status(error.response.status).send("An error with no message has been produced");
                return;
            }
        }
    }
    if (error.status) {
        res.status(error.status).send("An unknown error has been produced");
        return;
    }
    res.status(500).send("An unknown error has been produced");
}
);




// listen for requests
var server = app.listen(global.gConfig.server.port, () => {
    global.gLogger.info("ICAT+ is listening on port " + global.gConfig.server.port);
});

server.onCacheInitialized = function () {
}
global.gLogger.debug("Init cache...");
/** Init cache */
cache.init().then(function (response) {
    global.gLogger.info("Cache has been initialized ");
    server.onCacheInitialized();
});


var swaggerJSDoc = require('swagger-jsdoc');

let swaggerDefinition = {
    openapi: '3.0.0',
    info: {
        title: 'ICAT+ API',

        description: 'ICAT+ RESTful API with Swagger',
        version: '1.0.0',
        contact: {
            name: " Alejandro De Maria Antolinos",
            email: "demariaa@esrf.fr"
        },
        license: {
            name: "MIT",
            url: "https://opensource.org/licenses/MIT"
        }
    },
    servers: [{
        url: global.gConfig.server.url
    }],
    tags: [
        {
            name: "Session",
            description: "all API methods related to session",
        }
    ]
}

// options for the swagger docs
var options = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: ['./documentation/swagger/**/*.js', './**/routes/*.js'],// pass all in array 
};
// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

app.get('/swagger.json', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});

/** Swagger */
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, {exporer : true}));



module.exports = server;
