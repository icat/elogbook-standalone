global.gResource = require("./resources/global.resource.js");
global.gResource.investigations = require("./resources/investigations.resource.js");
global.chai = require('chai');
global.expect = require('chai').expect;
global.chaiHttp = require('chai-http');
global.should = chai.should();
global.chai.use(chaiHttp);

global.mongoUnit = require('mongo-unit');

process.env.NODE_ENV = 'test'


before(function (done) {

    // starts a mongodb instance which will stay in memory
    try{
        global.mongoUnit.start({                
                 verbose: false  })
            .then(mongoUri => {
                console.log('Started mongoDB for all tests. URI is : ' + mongoUri);
                process.env.MONGO_URI = mongoUri;

                //initialize the icat+ server 
                global.gServer = require("../server.js");
                global.gServer.onCacheInitialized = function () {
                    global.gRequester = chai.request(global.gServer).keepOpen();
                    console.log("Server is listening...")
                    setTimeout(function () { done(); }, 5000);
                };
            })
            .catch(e => {
                console.log("Could not start the mongo instance for tests.")
                console.log(e);
                done();
            });
    }catch(e){
            console.log("Could not start the mongo instance for tests.")
            console.log(e);
            done();
    };
});

after(() => {
    global.gRequester.close();
    console.log("Server is closed")
    global.mongoUnit.stop();
    console.log("MongoDB is stopped.");

})


beforeEach(function () {
});

describe('Global', () => {
    describe('Users', () => {
        it('[Allowed Users are defined on global.resources.js]', () => {
            expect(global.gResource).to.have.property("users");
            expect(global.gResource.users).to.have.property("allowed");
            expect(global.gResource.users.allowed).to.be.an("array");
            expect(global.gResource.users.allowed.length).to.be.above(0);
        })
        it('[Unallowed Users are defined on global.resources.js]', () => {
            expect(global.gResource).to.have.property("users");
            expect(global.gResource.users).to.have.property("denied");
            expect(global.gResource.users.denied).to.be.an("array");
            expect(global.gResource.users.denied.length).to.be.above(0);
        })
    })
})



