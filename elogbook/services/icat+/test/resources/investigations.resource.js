module.exports = {
    normalize: [{
            "input": "MX0000",
            "expected": "MX-0000"
        },
        {
            "input": "ihls3203",
            "expected": "IH-LS-3203"
        },
        {
            "input": "ih-ls3203",
            "expected": "IH-LS-3203"
        },
        {
            "input": "ih-ls-3203",
            "expected": "IH-LS-3203"
        },
        {
            "input": "ev280",
            "expected": "EV-280"
        },
        {
            "input": "ip0001",
            "expected": "IP-0001"
        }

    ],
    investigationusers: [{
        user: {
            "plugin": "db",
            "username": "reader",
            "password": "reader"
        },
        investigationId: 63335100
    }],
    investigation: [{
        user: {
            "plugin": "db",
            "username": "reader",
            "password": "reader"
        },
        investigationId: 63335100
    }]

}