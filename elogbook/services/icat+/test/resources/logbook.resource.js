module.exports = {

    createEvent: {
        success: //all tests which must succeed
            [
                {
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    event: {
                        investigationId: 63335100,
                        type: "annotation",
                        title: "a title",
                        category: 'commment',
                        content: [
                            {
                                "format": "plainText",
                                "text": "this is my comment",
                            },
                            {
                                "format": "html",
                                "text": "<p>this is my comment</p>"
                            }
                        ],
                        creationDate: "2018-09-01T00:00:01.000Z"
                    },
                    expected: {
                        investigationId: 63335100,
                        type: "annotation",
                        title: "a title",
                        category: 'commment',
                        content: [
                            {
                                "format": "plainText",
                                "text": "this is my comment",
                            },
                            {
                                "format": "html",
                                "text": "<p>this is my comment</p>"
                            }
                        ],
                        creationDate: "2018-09-01T00:00:01.000Z"
                    }
                },
                { // a notification sent by metadataManager
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    event: {
                        investigationId: 63335100,
                        type: "notification",
                        title: "notification title",
                        category: 'error',
                        content: [
                            {
                                format: "plainText",
                                text: "beam lost. No line break",
                            }
                        ],
                        creationDate: "2018-09-01T00:00:01.000Z"
                    },
                    expected: {
                        investigationId: 63335100,
                        type: "notification",
                        title: "notification title",
                        category: 'error',
                        content: [
                            {
                                format: "plainText",
                                text: "beam lost. No line break",
                            }
                        ],
                        creationDate: "2018-09-01T00:00:01.000Z"
                    }
                },
                { // a notification sent by metadataManager with has one \n
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    event: {
                        investigationId: 63335100,
                        type: "notification",
                        title: "notification title",
                        category: 'error',
                        content: [
                            {
                                format: "plainText",
                                text: "beam lost. Line one\nLine two",
                            }
                        ],
                        creationDate: "2018-09-01T00:00:01.000Z"
                    },
                    expected: {
                        investigationId: 63335100,
                        type: "notification",
                        title: "notification title",
                        category: 'error',
                        content: [
                            {
                                format: "plainText",
                                text: "beam lost. Line one\nLine two",
                            },
                            {
                                format: "html",
                                text: "beam lost. Line one<br>\nLine two",
                            }
                        ],
                        creationDate: "2018-09-01T00:00:01.000Z"
                    }
                }
            ],
        failure: //all tests which will fail
            []
    },

    updateEvent: {
        input: {
            user: {
                plugin: "db",
                username: "reader",
                password: "reader"
            }, // the user who is allowed
            investigationId: 63335100, //the investigationId the user is allowed to access
            initialEvent: {
                investigationId: 63335100,
                type: "annotation",
                title: "a title",
                category: 'commment',
                content: [{
                    "format": "plainText",
                    "text": "this is my comment",
                },
                {
                    "format": "html",
                    "text": "<p>this is my comment</p>"
                }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            }, // event used to create the event which will be modified afterwards
            newEventVersion: {
                investigationId: 63335100,
                type: "annotation",
                title: "a title",
                category: 'commment',
                content: [{
                    "format": "plainText",
                    "text": "this is my updated comment",
                },
                {
                    "format": "html",
                    "text": "<p>this is my updated comment</p>"
                }
                ],
                creationDate: "2018-09-01T00:00:02.000Z",
                previousVersionEvent: null,
            }
        }
    },

    getEvent: {
        success: {
            emptyLogbook: {
                input: {
                    query: {
                        find: {
                            $and: [{
                                $or: [{
                                    type: "annotation"
                                },
                                {
                                    type: "notification"
                                }
                                ]
                            }]
                        },
                        sort: {
                            creationDate: -1
                        },
                        skip: 0,
                        limit: 6
                    },
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                },
                expected: {}
            },


            notEmptyLogbook: {
                input: {
                    initialEvent: {
                        investigationId: 63335100,
                        type: "annotation",
                        title: "a title",
                        category: 'commment',
                        content: [{
                            "format": "plainText",
                            "text": "this is my comment",
                        },
                        {
                            "format": "html",
                            "text": "<p>this is my comment</p>"
                        }
                        ],
                        creationDate: "2018-09-01T00:00:01.000Z"
                    }, // event used to create the event which will be modified afterwards
                    body: {
                        find: {
                            $and: [{
                                $or: [{
                                    type: "annotation"
                                },
                                {
                                    type: "notification"
                                }
                                ]
                            }]
                        },
                        sort: {
                            creationDate: -1
                        },
                        skip: 0,
                        limit: 6
                    },
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                }
            },


        },
        failure: {
            input: {
                body: {
                    find: {
                        $and: [{
                            $or: [{
                                type: "annotation"
                            },
                            {
                                type: "notification"
                            }
                            ]
                        }]
                    },
                    sort: {
                        creationDate: -1
                    },
                    skip: 0,
                    limit: 6
                },
                user: {
                    plugin: "db",
                    username: "reader",
                    password: "reader"
                }, // the user who is denied
                investigationId: 2483226, //the investigationId the user is not allowed to access  
            },
        }
    },

    createEventWithInvestigationNameAndInstrumentName: {
        success: [{
            investigationName: "id010100",
            instrumentName: "id01",
            event: {
                type: "notification",
                title: "a title",
                category: 'commment',
                content: [{
                    "format": "plainText",
                    "text": "this is my comment",
                },
                {
                    "format": "html",
                    "text": "<p>this is my comment</p>"
                }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            },
            expected: {
                event: {
                    investigationId: 63335100,
                    investigationName: "ID010100",
                    instrumentName: "id01",
                    type: "notification",
                    title: "a title",
                    category: 'commment',
                    content: [{
                        "format": "plainText",
                        "text": "this is my comment",
                    },
                    {
                        "format": "html",
                        "text": "<p>this is my comment</p>"
                    }
                    ],
                    creationDate: "2018-09-01T00:00:01.000Z"
                }
            }
        },
        {
            investigationName: "id110000",
            instrumentName: "id11",
            event: {
                type: "notification",
                title: "a title",
                category: 'commment',
                content: [{
                    "format": "plainText",
                    "text": "this is my comment",
                },
                {
                    "format": "html",
                    "text": "<p>this is my comment</p>"
                }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            },
            expected: {
                event: {
                    investigationName: "ID110000",
                    instrumentName: "id11",
                    investigationId: 117597385,
                    type: "notification",
                    title: "a title",
                    category: 'commment',
                    content: [{
                        "format": "plainText",
                        "text": "this is my comment",
                    },
                    {
                        "format": "html",
                        "text": "<p>this is my comment</p>"
                    }
                    ],
                    creationDate: "2018-09-01T00:00:01.000Z"
                }
            }
        },
        { // notification with \n
            investigationName: "id010100",
            instrumentName: "id01",
            event: {
                type: "notification",
                title: "a title",
                category: 'error',
                content: [{
                    "format": "plainText",
                    "text": "beam lost\ntoo bad",
                }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            },
            expected: {
                event: {
                    investigationName: "ID010100",
                    instrumentName: "id01",
                    investigationId: 63335100,
                    type: "notification",
                    title: "a title",
                    category: 'error',
                    content: [{
                        "format": "plainText",
                        "text": "beam lost\ntoo bad",
                    },
                    {
                        "format": "html",
                        "text": "beam lost<br>\ntoo bad"
                    }
                    ],
                    creationDate: "2018-09-01T00:00:01.000Z"
                }
            }
        },
        { // annotation with two \n
            investigationName: "id010100",
            instrumentName: "id01",
            event: {
                type: "annotation",
                title: "a title",
                category: 'comment',
                content: [{
                    "format": "plainText",
                    "text": "this is the first line of the comment\nthis is the second line\nthis is the third line",
                }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            },
            expected: {
                event: {
                    investigationName: "ID010100",
                    instrumentName: "id01",
                    investigationId: 63335100,
                    type: "annotation",
                    title: "a title",
                    category: 'comment',
                    content: [{
                        "format": "plainText",
                        "text": "this is the first line of the comment\nthis is the second line\nthis is the third line",
                    },
                    {
                        "format": "html",
                        "text": "this is the first line of the comment<br>\nthis is the second line<br>\nthis is the third line"
                    }
                    ],
                    creationDate: "2018-09-01T00:00:01.000Z"
                }
            }
        }
        ],
        Error403: [{
            investigationName: "id010100",
            instrumentName: "id01",
            token: 'fake'
        }],

        Error404: [{
            investigationName: "id010100",
            instrumentName: "id01",
            token: ''
        }]
    },

    /*
    createNotificationWihInvestigationName: [{
            investigationName: "id010100",
            instrumentName: "id01",
            event: {
                type: "notification",
                title: "a title",
                category: 'commment',
                content: [{
                        "format": "plainText",
                        "text": "this is my comment",
                    },
                    {
                        "format": "html",
                        "text": "<p>this is my comment</p>"
                    }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            },
            expected: {
                investigationId: 63335100,
            }
        },
        {
            
            investigationName: "id010100",
            instrumentName: "id01",
            event: {
                type: "notification",
                title: "a title",
                category: 'commment',
                content: [{
                        "format": "plainText",
                        "text": "this is my comment",
                    },
                    {
                        "format": "html",
                        "text": "<p>this is my comment</p>"
                    }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            },
            expected: {
                investigationId: 63335100,
            }
        }
    ],
 
    createNotificationWihInvestigationNameError403: [{
        investigationName: "id010100",
        instrumentName: "id01",
        token : 'fake'
    }],
 
    createNotificationWihInvestigationNameError404: [{
        investigationName: "id010100",
        instrumentName: "id01",
        token : ''
    }],
*/

    getPDF: {
        input: {
            investigationId: 63335100,
            user: {
                plugin: "db",
                username: "reader",
                password: "reader"
            }, // the user who is allowed
            query: {
                find: {
                    $and: [{
                        $or: [{
                            type: "annotation"
                        },
                        {
                            type: "notification"
                        }
                        ]
                    }]
                },
                sort: {
                    creationDate: -1
                },
                skip: 0,
                limit: 6
            },
            event: {
                investigationId: 63335100,
                type: "annotation",
                title: "a title",
                category: 'commment',
                content: [{
                    "format": "plainText",
                    "text": "this is my comment",
                },
                {
                    "format": "html",
                    "text": "<p>this is my comment</p>"
                }
                ],
                creationDate: "2018-09-01T00:00:01.000Z"
            }
        },
    },


    createInvestigationTag: {
        success: //all tests which must succeed
            [
                {
                    testDescription: "investigation tag, +color, +description",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        investigationId: 63335100,
                    },
                    expected: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        investigationId: 63335100,
                    }
                },
                {
                    testDescription: "investigation tag, +description, -color",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        investigationId: 63335100,
                    },
                    expected: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        investigationId: 63335100,
                    }
                },
                {
                    testDescription: "investigation tag, -description, -color",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tag: {
                        name: 'a tag name',
                        investigationId: 63335100,
                    },
                    expected: {
                        name: 'a tag name',
                        investigationId: 63335100,
                    }
                },
                {
                    testDescription: "investigation tag, -description, -color, -investigationId",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tag: {
                        name: 'a tag name',
                    },
                    expected: {
                        name: 'a tag name',
                        investigationId: 63335100,
                    }
                },
            ],
        failure: //all tests which will fail
            [
                {
                    testDescription: "beamlineName must not be present in the body 1",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        investigationId: 63335100,
                        beamlineName: "id02"
                    },
                    expected: {
                        errorStatus: 400 //investigationId and beamlineNane are mutually exclusive
                    }
                },
                {
                    testDescription: "beamlineName must not be present in the body 2",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        beamlineName: "id02"
                    },
                    expected: {
                        errorStatus: 400 //investigationId and beamlineNane are mutually exclusive
                    }
                },
                {
                    testDescription: "error403 User is not a participant of the investigation",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is denied
                    investigationId: 2483226, //the investigationId the user is not allowed to access
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        investigationId: 2483226,
                    },
                    expected: {
                        errorStatus: 403
                    }
                }
            ]
    },

    createBeamlineTag: {
        success: //all tests which must succeed
            [
                {
                    testDescription: "beamline tag, +color, +description",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    instrumentName: "id01",
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        beamlineName: "id01"
                    },
                    expected: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        beamlineName: "id01"
                    }
                },
                {
                    testDescription: "beamline tag, +description, -color",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    instrumentName: "id01",
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        beamlineName: "id01",
                    },
                    expected: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        beamlineName: "id01",
                    }
                },
                {
                    testDescription: "beamline tag, -description, -color",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    instrumentName: "id01",
                    tag: {
                        name: 'a tag name',
                        beamlineName: "id01",
                    },
                    expected: {
                        name: 'a tag name',
                        beamlineName: "id01",
                    }
                },
                {
                    testDescription: "beamline tag, -description, -color, -beamlineName",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    instrumentName: "id01",
                    tag: {
                        name: 'a tag name',
                    },
                    expected: {
                        name: 'a tag name',
                        beamlineName: "id01",
                    }
                },
            ],
        failure: //all tests which will fail
            [
                {
                    testDescription: "investigationId must not be present in the body 1",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    instrumentName: "id01",
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        investigationId: 63335100,
                        beamlineName: "id01"
                    },
                    expected: {
                        errorStatus: 400 //investigationId and beamlineNane are mutually exclusive
                    }
                },
                {
                    testDescription: "investigationId must not be present in the body 2",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    instrumentName: "id01",
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        investigationId: 63335100,
                    },
                    expected: {
                        errorStatus: 400
                    }
                },
                {
                    testDescription: "this user does not have the permission to create a beamline tag",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    instrumentName: "id02",
                    tag: {
                        name: 'a tag name',
                        description: ' this is a description of the tag',
                        color: 'blue',
                        investigationId: 63335100,
                    },
                    expected: {
                        errorStatus: 403
                    }
                },
            ]
    },

    getTags: {
        success: //all tests which must succeed
            [
                {
                    testDescription: "GET 1 investigation tag",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tagsToFillDB: [
                        {
                            name: 'a tag name',
                            description: ' this is a description of the tag',
                            color: 'blue',
                            investigationId: 63335100,
                        },
                    ],
                    expected: [
                        {
                            name: 'a tag name',
                            description: ' this is a description of the tag',
                            color: 'blue',
                            investigationId: 63335100,
                        }
                    ]
                },
                {
                    testDescription: "GET 2 investigation tags",
                    user: {
                        plugin: "db",
                        username: "reader",
                        password: "reader"
                    }, // the user who is allowed
                    investigationId: 63335100, //the investigationId the user is allowed to access
                    tagsToFillDB: [
                        {
                            name: 'an investigation tag',
                            description: ' this is a description of the investigation tag',
                            color: 'blue',
                            investigationId: 63335100,
                        },
                        {
                            name: 'a beamline tag',
                            description: ' this is a description of the beamline tag',
                            color: 'green',
                            investigationId: 63335100,
                        },
                    ],
                    expected: [
                        {
                            name: 'an investigation tag',
                            description: ' this is a description of the investigation tag',
                            color: 'blue',
                            investigationId: 63335100,
                        },
                        {
                            name: 'a beamline tag',
                            description: ' this is a description of the beamline tag',
                            color: 'green',
                            investigationId: 63335100,
                        }
                    ]
                },

            ],
        failure: [
            {
                testDescription: "error403 User is not a participant of the investigation",
                userToPopulate: {
                    plugin: "db",
                    username: "test_scientistInInv2483226",
                    password: "testPassword"
                }, // the user who is adding tags
                userToGet: {
                    plugin: "db",
                    username: "reader",
                    password: "reader"
                }, // the user who is getting tags (but not allowed)
                investigationId: 2483226, //the investigationId the user is not allowed to access
                tag: {
                    name: 'a tag name',
                    description: ' this is a description of the tag',
                    color: 'blue',
                    investigationId: 2483226,
                },
                expected: {
                    errorStatus: 403
                }
            }
        ]
    }
}