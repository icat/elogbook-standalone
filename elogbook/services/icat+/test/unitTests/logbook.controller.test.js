require('it-each')({ testPerIteration: true });

const resources = require('./resources/logbook.controller.resource');
const configForTests = require('../config/server.config');
global.gConfig = configForTests;
const eventController = require('../../app/controllers/logbook.controller');

describe("Units tests on logbook.controller.js", () => {
    describe('replaceImageSrc()', () => {
        it.each(resources.replaceImageSrc, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {

                let updatedEvents = eventController.replaceImageSrc(element.events, element.sessionId, element.investigationId, element.serverURI);

                expect(updatedEvents).to.deep.equal(element.expected);
                next();
            })
    })

    describe("translateEventContentToHtml()", () => {
        it.each(resources.translateEventContentToHtml, '[content is %o]', ['content'],
            function (element, next) {

                let translatedEventContent = eventController.translateEventContentToHtml(element.content);

                expect(translatedEventContent).to.deep.equal(element.expected);
                next();
            })
    })
});