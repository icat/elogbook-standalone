require('it-each')({ testPerIteration: true });

describe('Session', () => {
    describe('/session', () => {
        it.each(global.gResource.users.denied, '[Wrong credentials for %s]', ['username'], function (element, next) {
            chai.request(global.gServer)
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element)
                .end((err, res) => {
                    expect(res.status).to.equal(403);
                    next();
                });
        })

        it.each(global.gResource.users.allowed, '[Right credentials for %s]', ['username'], function (element, next) {
            chai.request(global.gServer)
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body).to.have.property("sessionId").to.be.a("string").to.have.length(36);
                    next();
                });
        })
    })

})