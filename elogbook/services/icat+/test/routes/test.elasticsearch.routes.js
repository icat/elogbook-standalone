const elasticResource = require("../resources/elasticsearch.resource.js");

require('it-each')({
    testPerIteration: true
});

describe('ElasticSearch', () => {
    describe('/elasticsearch/:sessionId/datasets/_msearch', () => {
    
        it.each(elasticResource.searchBy, '[Search All]', ['query'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {       
                    function doTest(){
                        global.gRequester
                        .post('/elasticsearch/'+ res.body.sessionId +'/datasets/_msearch')
                        .set('Content-Type', 'application/json')
                        .send({query : element.query})
                        .end((err, response) => {                                                        
                            expect(response.status).to.equal(200);                            
                            next();
                        });
                    }
                    /** 
                     * We need some seconds for cache to be filled
                     * Datacollections need to be retrieved                      
                     **/
                    setTimeout(function () { doTest(); }, 5000);             
                    
                })
        })
    });


});