
require('it-each')({ testPerIteration: true });
let datafileResource = require('../resources/datafile.resource.js');

describe('Datafile', () => {
    describe('/catalogue/:sessionId/dataset/id/:datasetId/datafile', () => {      
        it.each(datafileResource.byDatasets.success, '[Get datafiles by dataset id %s]', ['datasetId'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {                                     
                    global.gRequester
                        .get('/catalogue/' + res.body.sessionId + '/dataset/id/' + element.datasetId +'/datafile')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {                                                 
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            next();
                        });
                })

        });

         it.each(datafileResource.byDatasets.permissionError, '[Get datafiles by dataset id %s]', ['datasetId'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {                                     
                    global.gRequester
                        .get('/catalogue/' + res.body.sessionId + '/dataset/id/' + element.datasetId +'/datafile')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {                                                 
                            expect(response.status).to.equal(401);                            
                            next();
                        });
                })

        });
  
    });


});
