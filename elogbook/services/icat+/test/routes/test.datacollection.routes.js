
require('it-each')({ testPerIteration: true });


describe('Datacollections', () => {
    describe('/catalogue/:sessionId/datacollection', () => {

      

        it.each(global.gResource.users.allowed, '[Get datacollections for %s]', ['username'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element)
                .end((err, res) => {                                     
                    global.gRequester
                        .get('/catalogue/' + res.body.sessionId + '/datacollection')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {                                                 
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            next();
                        });
                })

        })
  
    });



});
