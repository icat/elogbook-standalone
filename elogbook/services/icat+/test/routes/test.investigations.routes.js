
require('it-each')({ testPerIteration: true });
let investigationsResource = require('../resources/investigations.resource.js');

describe('Investigations', () => {
    describe('/catalogue/:sessionId/investigation', () => {
        it.each(global.gResource.users.allowed, '[Get investigations for %s]', ['username'], function (element, next) {
             global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element)
                .end((err, res) => {
                    global.gRequester
                        .get('/catalogue/' +  res.body.sessionId +'/investigation')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            next();
                        });
                })

        })      
    });

    describe('/catalogue/:sessionId/investigation/status/embargoed/investigation', () => {
        it.each(global.gResource.users.allowed, '[Get investigations for %s]', ['username'], function (element, next) {
             global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element)
                .end((err, res) => {
                    global.gRequester
                        .get('/catalogue/' +  res.body.sessionId +'/investigation/status/embargoed/investigation')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");                            
                            for (var i = 0; i < response.body.length; i++){                                                               
                                should.exist(response.body[i].Investigation.releaseDate);
                                expect(new Date(response.body[i].Investigation.releaseDate) > new Date()).to.be.true;
                            }
                            next();
                        });
                })

        })      
    });

    describe('/catalogue/investigation/name/:investigationName/normalize', () => {
        it.each(investigationsResource.normalize, '[Normalize %s]', ['input'], function (element, next) {
            global.gRequester
                .get('/catalogue/investigation/name/' + element.input + '/normalize')
                .set('Content-Type', 'application/json')
                .send()
                .end((err, response) => {
                   expect(response.status).to.equal(200); 
                   expect(response.text).to.equal(element.expected);                   
                   next();
                });
        })
    })

     describe('/catalogue/:sessionId/investigation/id/:investigationId/investigation', () => {
       it.each(investigationsResource.investigation, '[Get investigations by id %s]', ['investigationId'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {                                       
                    global.gRequester
                        .get('/catalogue/' + res.body.sessionId + '/investigation/id/' + element.investigationId + "/investigation")
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {        
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            next();
                        });
                })

        }) 
    })



    describe('/catalogue/:sessionId/investigation/id/:investigationId/investigationusers', () => {
        it.each(investigationsResource.investigationusers, '[ Get investigationusers forinvestigationId]', ['investigationId'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {
                    global.gRequester                    
                        .get('/catalogue/' + res.body.sessionId + '/investigation/id/' + element.investigationId + '/investigationusers')                        
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            next();
                        });
                })
        })
    });
});
