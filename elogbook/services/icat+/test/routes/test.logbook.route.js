require('it-each')({ testPerIteration: true });

const _ = require('lodash');
const serverConfig = require('../config/server.config');
const eventResource = require('../resources/logbook.resource');

describe('Logbook', () => {
    // Drop the database before each tests on this 'describe' block and all the nested 'describe' blocks
    beforeEach(() => {
        global.mongoUnit.drop();
    })

    describe('/logbook/:sessionId/investigation/id/:investigationIds/event/create', () => {
        it.each(eventResource.createEvent.success,
            '[Create an event. InvestigationId: %s, Type: %s, Category: %s ]', ['investigationId', 'event.type', 'event.category'],
            function (element, next) {
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(element.user)
                    .end((err, res) => {
                        global.gRequester
                            .post('/logbook/' + res.body.sessionId + '/investigation/id/' + element.investigationId + '/event/create')
                            .set('Content-Type', 'application/json')
                            .send(element.event)
                            .end((err, response) => {
                                expect(response.status).to.equal(200);
                                testEvent(response.body, element.expected, ['title']);
                                next();
                            });
                    })
            })
    })

    describe('/logbook/:sessionId/investigation/id/:investigationIds/event/update', () => {
        it("Updates an event", (done) => {
            // first we create an event and store the id of the created event
            // second we updated the newly created event
            let input = eventResource.updateEvent.input;

            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(input.user)
                .end((err, res) => {

                    global.gRequester
                        .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/create')
                        .set('Content-Type', 'application/json')
                        .send(input.initialEvent)
                        .end((err, response1) => {
                            //create and send a new version of the event
                            let newEventVersion = input.newEventVersion;
                            newEventVersion._id = response1.body._id;
                            newEventVersion.previousVersionEvent = response1.body._id;

                            global.gRequester
                                .put('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/update')
                                .set('Content-Type', 'application/json')
                                .send({ event: newEventVersion })
                                .end((err, response2) => {
                                    expect(response2.status).to.equal(200);

                                    // test the latest version of the event
                                    testEvent(response2.body, newEventVersion, ['title', 'previousVersionEvent', '_id']);

                                    // test the intial version of the event
                                    testEvent(response2.body.previousVersionEvent, response1.body, ['title', '_id']);
                                    done();
                                })
                        });
                })
        })

    })


    describe('/logbook/:sessionId/investigation/id/:investigationId/event/query', () => {
        describe('user has permission for the investigation', () => {
            it("GET no event when logbook is empty", (done) => {
                let input = eventResource.getEvent.success.emptyLogbook.input;

                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(input.user)
                    .end((err, res) => {
                        global.gRequester
                            .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/query')
                            .set('Content-Type', 'application/json')
                            .send(input.query)
                            .end((err, response) => {
                                expect(response.status).to.equal(200);
                                expect(response.body).to.be.an("array");
                                expect(response.body).to.have.length(0);
                                done();
                            });
                    })
            });

            it("GET events when logbook is not empty", (done) => {
                let input = eventResource.getEvent.success.notEmptyLogbook.input;

                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(input.user)
                    .end((err, res) => {

                        global.gRequester
                            .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/create')
                            .set('Content-Type', 'application/json')
                            .send(input.initialEvent)
                            .end((err, response1) => {

                                global.gRequester
                                    .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/query')
                                    .set('Content-Type', 'application/json')
                                    .send(input.body)
                                    .end((err, response2) => {
                                        expect(response2.status).to.equal(200);

                                        expect(response2.body).to.be.an("array");
                                        expect(response2.body).to.have.length(1);

                                        testEvent(response2.body[0], response1.body, ['title', '_id']);
                                        done();
                                    });
                            })
                    })
            });
        })


        describe('user does not have permission for the investigation', () => {
            it("GET events", (done) => {
                let input = eventResource.getEvent.failure.input;
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(input.user)
                    .end((err, res) => {
                        global.gRequester
                            .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/query')
                            .set('Content-Type', 'application/json')
                            .send(input.body)
                            .end((err, response) => {
                                expect(response.status).to.equal(403);
                                done();
                            });
                    })
            });
        });
    });


    describe('/logbook/:sessionId/investigation/id/:investigationId/event/count', () => {
        describe('user has permission for the investigation', () => {
            it("GET event count when logbook is empty", (done) => {
                let input = eventResource.getEvent.success.emptyLogbook.input;

                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(input.user)
                    .end((err, res) => {
                        global.gRequester
                            .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/count')
                            .set('Content-Type', 'application/json')
                            .send(input.query)
                            .end((err, response) => {
                                expect(response.status).to.equal(200);
                                expect(response.body).to.be.an('object').to.be.empty;
                                expect(response.text).to.be.an('string').to.equal('0');

                                done();
                            });
                    })
            });

            it("GET event count when logbook is not empty", (done) => {
                let input = eventResource.getEvent.success.notEmptyLogbook.input;

                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(input.user)
                    .end((err, res) => {

                        global.gRequester
                            .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/create')
                            .set('Content-Type', 'application/json')
                            .send(input.initialEvent)
                            .end((err, response1) => {

                                global.gRequester
                                    .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/count')
                                    .set('Content-Type', 'application/json')
                                    .send(input.body)
                                    .end((err, response2) => {
                                        expect(response2.status).to.equal(200);
                                        expect(response2.body).to.be.an('object').to.be.empty;
                                        expect(response2.text).to.be.an('string').to.equal('1');
                                        done();
                                    });
                            })
                    })
            });

        })

        describe('user does not have permission for the investigation', () => {
            it("GET events", (done) => {
                let input = eventResource.getEvent.failure.input;
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(input.user)
                    .end((err, res) => {
                        global.gRequester
                            .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/count')
                            .set('Content-Type', 'application/json')
                            .send(input.body)
                            .end((err, response) => {
                                expect(response.status).to.equal(403);
                                done();
                            });
                    })
            });
        });
    });


    describe('/logbook/:sessionId/investigation/name/:investigationName/instrument/name/:instrumentName/event', () => {
        describe('Create a notification', () => {
            it.each(eventResource.createEventWithInvestigationNameAndInstrumentName.success, '[New notification for investigationName %s and instrumentName %s]', ['investigationName', 'instrumentName'], function (element, next) {
                global.gRequester
                    .post('/logbook/' + serverConfig.server.API_KEY + '/investigation/name/' + element.investigationName + '/instrument/name/' + element.instrumentName + '/event')
                    .set('Content-Type', 'application/json')
                    .send(element.event)
                    .end((err, response) => {
                        expect(response.status).to.equal(200);

                        let expectedEvent = element.expected.event;
                        testEvent(response.body, expectedEvent, ['title', 'investigationName', 'instrumentName']);
                        next();
                    });
            })
        });

        describe('Create notification with no permissions', () => {
            it.each(eventResource.createEventWithInvestigationNameAndInstrumentName.Error403, '[New notification with token %s]', ['token'], function (element, next) {
                global.gRequester
                    .post('/logbook/' + element.token + '/investigation/name/' + element.investigationName + '/instrument/name/' + element.instrumentName + '/event')
                    .set('Content-Type', 'application/json')
                    .send({})
                    .end((err, response) => {
                        expect(response.status).to.equal(403);
                        next();
                    });
            })
        });

        describe('Create notification with no token', () => {
            it.each(eventResource.createEventWithInvestigationNameAndInstrumentName.Error404, '[New notification with token %s]', ['token'], function (element, next) {
                global.gRequester
                    .post('/logbook/' + element.token + '/investigation/name/' + element.investigationName + '/instrument/name/' + element.instrumentName + '/event')
                    .set('Content-Type', 'application/json')
                    .send({})
                    .end((err, response) => {
                        expect(response.status).to.equal(404);
                        next();
                    });
            })
        });
    });

    describe('/logbook/:sessionId/investigation/id/:investigationId/event/pdf', () => {
        it("Get a PDF", (done) => {
            let input = eventResource.getPDF.input;

            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(input.user)
                .end((err, res) => {

                    global.gRequester
                        .post('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/create')
                        .set('Content-Type', 'application/json')
                        .send(input.event)
                        .end((err, response1) => {

                            global.gRequester
                                .get('/logbook/' + res.body.sessionId + '/investigation/id/' + input.investigationId + '/event/pdf')
                                .query({
                                    find: JSON.stringify(input.query.find),
                                    sort: JSON.stringify(input.query.sort),
                                    skip: JSON.stringify(input.query.skip),
                                    limit: JSON.stringify(input.query.limit)
                                })
                                .end((err, response2) => {
                                    expect(response2.status).to.equal(200);
                                    expect(response2.text).to.be.not.empty; //that's where the file is.
                                    expect(response2.body).to.be.empty;
                                    done();
                                });
                        })
                })
        })
    });

    describe('/logbook/:sessionId/investigation/id/:investigationId/tag/create', () => {
        it.each(eventResource.createInvestigationTag.success,
            '[Create a tag. Test description: %s]', ['testDescription'],
            function (element, next) {
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(element.user)
                    .end((err, res1) => {
                        global.gRequester
                            .post('/logbook/' + res1.body.sessionId + '/investigation/id/' + element.investigationId + '/tag/create')
                            .set('Content-Type', 'application/json')
                            .send(element.tag)
                            .end((err, res2) => {
                                expect(res2.status).to.equal(200);
                                testTag(res2.body, element.expected);
                                next();
                            });
                    })
            })

        it.each(eventResource.createInvestigationTag.failure,
            '[Create a tag. Test description: %s]', ['testDescription'],
            function (element, next) {
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(element.user)
                    .end((err, res1) => {
                        global.gRequester
                            .post('/logbook/' + res1.body.sessionId + '/investigation/id/' + element.investigationId + '/tag/create')
                            .set('Content-Type', 'application/json')
                            .send(element.tag)
                            .end((err, res2) => {
                                global.
                                    expect(res2.status).to.equal(element.expected.errorStatus);
                                next();
                            });
                    })
            })
    });

    describe('/logbook/:sessionId/instrument/name/:instrumentName/tag/create', () => {
        it.each(eventResource.createBeamlineTag.success,
            '[Create a tag. Test description: %s]', ['testDescription'],
            function (element, next) {
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(element.user)
                    .end((err, res1) => {
                        global.gRequester
                            .post('/logbook/' + res1.body.sessionId + '/instrument/name/' + element.instrumentName + '/tag/create')
                            .set('Content-Type', 'application/json')
                            .send(element.tag)
                            .end((err, res2) => {
                                expect(res2.status).to.equal(200);
                                testTag(res2.body, element.expected);
                                next();
                            });
                    })
            })

        it.each(eventResource.createBeamlineTag.failure,
            '[Create a tag. Test description: %s]', ['testDescription'],
            function (element, next) {
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(element.user)
                    .end((err, res1) => {
                        global.gRequester
                            .post('/logbook/' + res1.body.sessionId + '/instrument/name/' + element.instrumentName + '/tag/create')
                            .set('Content-Type', 'application/json')
                            .send(element.tag)
                            .end((err, res2) => {
                                expect(res2.status).to.equal(element.expected.errorStatus);
                                next();
                            });
                    })
            })
    })

    describe('/logbook/:sessionId/investigation/id/:investigationId/tag', () => {
        it.each(eventResource.getTags.success,
            '[Get tags for investigation %s. Test description: %s]', ['investigationId', 'testDescription'],
            function (element, next) {
                // first we get a session
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(element.user)
                    .end((err, res1) => {
                        // then we prepare the requests to populate the DB. There can be one or many.
                        // All populating requests must be finished before we start tests
                        // I did not use promise.all because server response can come at any time and corresponding log lines be mixed with other tests. thus being less readable.

                        let populatingFunctionArray = []; // contains the function which will make one request
                        element.tagsToFillDB.forEach(itemToAdd => {
                            populatingFunctionArray.push(
                                () => global.gRequester.post('/logbook/' + res1.body.sessionId + '/investigation/id/' + element.investigationId + '/tag/create')
                                    .set('Content-Type', 'application/json')
                                    .send(itemToAdd));
                        })

                        async function populateDBThenTest() {
                            // then we populate the DB with each requests, one after the other. Note, we can not use forEach/await combo
                            for (let item of populatingFunctionArray) {
                                await item();
                            }

                            // then the real tests start here
                            await startTest()
                        }

                        function startTest() {
                            global.gRequester
                                .get('/logbook/' + res1.body.sessionId + '/investigation/id/' + element.investigationId + '/tag')
                                .set('Content-Type', 'application/json')
                                .end((err, res3) => {
                                    expect(res3.status).to.equal(200);
                                    expect(JSON.parse(res3.text).tags).to.be.a('array').to.have.length(element.expected.length)
                                    next()
                                })
                        }
                        populateDBThenTest()
                    });
            })

        it.each(eventResource.getTags.failure,
            '[Get tags for investigation %s. Test description: %s]', ['investigationId', 'testDescription'],
            function (element, next) {
                // first we get a session for the user who will add the tag
                global.gRequester
                    .post('/session')
                    .set('Content-Type', 'application/json')
                    .send(element.userToPopulate)
                    .end((err, res1) => {

                        // then we post the tag
                        global.gRequester
                            .post('/logbook/' + res1.body.sessionId + '/investigation/id/' + element.investigationId + '/tag/create')
                            .set('Content-Type', 'application/json')
                            .send(element.tag)
                            .end((err, res2) => {

                                // then we get a new session for a user who is not participant
                                global.gRequester
                                    .post('/session')
                                    .set('Content-Type', 'application/json')
                                    .send(element.userToGet)
                                    .end((err, res3) => {

                                        // then we try to get the tags
                                        global.gRequester
                                            .get('/logbook/' + res3.body.sessionId + '/investigation/id/' + element.investigationId + '/tag')
                                            .set('Content-Type', 'application/json')
                                            .end((err, res4) => {
                                                expect(res4.status).to.equal(element.expected.errorStatus);
                                                next()
                                            })
                                    });
                            })
                    })
            })
    })
});

/**
 * Test function which tests that an actual event corresponds to the expected event.
 * @param {object} actualEvent the event to test
 * @param {object} expectedEvent the expected event
 * @param {array} optionalfieldsToCheck fields of event object which are not null so need to be tested
 */
function testEvent(actualEvent, expectedEvent, optionalfieldsToCheck) {
    // Here we test mandatory fields
    expect(actualEvent._id).to.be.a('string');
    expect(actualEvent.investigationId).to.be.an('number').to.equal(expectedEvent.investigationId);
    expect(actualEvent.type).to.be.a('string').to.equal(expectedEvent.type);
    expect(actualEvent.category).to.be.a('string').to.equal(expectedEvent.category);
    expect(actualEvent.creationDate).to.be.a('string').to.equal(expectedEvent.creationDate);

    expect(actualEvent.content).to.be.an('array').to.have.length(expectedEvent.content.length);
    _.map(actualEvent.content, (item) => {
        if (item.format === 'plainText') {
            expect(item.text).to.equal(_.find(expectedEvent.content, item => item.format === 'plainText').text);
        } else if (item.format === 'html') {
            expect(item.text).to.equal(_.find(expectedEvent.content, item => item.format === 'html').text);
        } else {
            assert("an unexpected format was detected : " + item.format)
        }
    })

    // Here we possibly tests optional fields
    if (_.find(optionalfieldsToCheck, item => item === 'title')) {
        expect(actualEvent.title).to.be.a('string').to.equal(expectedEvent.title);
    }

    if (_.find(optionalfieldsToCheck, item => item === 'previousVersionEvent')) {
        expect(actualEvent.previousVersionEvent).to.not.be.a('null');
    }

    // Here we have expectations on the value of _.id
    if (_.find(optionalfieldsToCheck, item => item === '_id')) {
        expect(actualEvent._id).to.equal(expectedEvent._id);
    }

    if (_.find(optionalfieldsToCheck, item => item === 'investigationName')) {
        expect(actualEvent.investigationName).to.equal(expectedEvent.investigationName);
    }

    if (_.find(optionalfieldsToCheck, item => item === 'instrumentName')) {
        expect(actualEvent.instrumentName).to.equal(expectedEvent.instrumentName);
    }
}

/**
 * Test function which tests that an actual tag corresponds to the expected tag.
 * @param {object} actualTag the tag to test
 * @param {object} expectedTag the expected tag
 */
function testTag(actualTag, expectedTag) {
    expect(actualTag._id).to.be.a('string');

    _.forIn(expectedTag, (value, key) => {
        if (key === 'name') {
            expect(actualTag.name).to.be.a('string').to.equal(expectedTag.name);
        }
        if (key === 'description') {
            expect(actualTag.description).to.be.a('string').to.equal(expectedTag.description);
        }
        if (key === 'color') {
            expect(actualTag.color).to.be.a('string').to.equal(expectedTag.color);
        }
        if (key === 'investigationId') {
            expect(actualTag.investigationId).to.be.a('number').to.equal(expectedTag.investigationId);
        }
        if (key === 'beamlineName') {
            expect(actualTag.beamlineName).to.be.a('string').to.equal(expectedTag.beamlineName);
        }
    })

}


