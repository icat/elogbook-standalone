const datasetResource = require("../resources/dataset.resource.js");

require('it-each')({
    testPerIteration: true
});

describe('Datasets', () => {
    describe('/catalogue/:sessionId/dataset/id/:datasetId/datacollection', () => {    
        it.each(datasetResource.getDataCollectionByDataSetId, '[Get datacollection by datasetId: %s]', ['datasetId'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {

                    global.gRequester
                        .get('/catalogue/' + res.body.sessionId + '/dataset/id/' + element.datasetId + '/datacollection')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            next();
                        });
                })
        })
    });

    describe('/catalogue/:sessionId/investigation/id/:investigationId/dataset', () => {        
        it.each(datasetResource.getDatasetByInvestigationId, '[Get datasets for investigation %s]', ['investigationId'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {                                        
                    global.gRequester
                        .get('/catalogue/' + res.body.sessionId + '/investigation/id/' + element.investigationId + '/dataset')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {                            
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            next();
                        });
                })
        })
    });

    describe('/catalogue/:sessionId/dataset/id/:datasetIds/dataset', () => {        
        it.each(datasetResource.getDatasetByDatasetIds, '[Get datasets for datasetIds: %s]', ['datasetIds'], function (element, next) {
            global.gRequester
                .post('/session')
                .set('Content-Type', 'application/json')
                .send(element.user)
                .end((err, res) => {
                    global.gRequester
                        .get('/catalogue/' + res.body.sessionId + '/dataset/id/' + element.datasetIds + '/dataset')
                        .set('Content-Type', 'application/json')
                        .send()
                        .end((err, response) => {                            
                            expect(response.status).to.equal(200);
                            expect(response.body).to.be.an("array");
                            expect(response.body).to.be.an("array").to.have.lengthOf.above(0);
                            next();
                        });
                })
        })
    });



});