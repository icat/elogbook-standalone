var winston = require('winston');
var WinstonGraylog2 = require('winston-graylog2');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint, printf } = format;


function parseParameter(info){  
    var msg = "\n  ";
    for(var i in info){
        if (i != "level" & i != "message" & (i != "timestamp")){
            msg = msg + "\t\t\t\t " + i + " : " +  info[i] + "\n";
        } 
    }
    return msg;
};

const myFormat = printf(info => {  
    if (info instanceof Error) {
        return  `${info.timestamp} [${info.level}]: ${info.message} ${info.stack}`;
    }
  return `${info.timestamp} [${info.level}]: ${info.message} ${parseParameter(info)}`;
});


function getWinstonGrayLogTransport() {
    try {
        let options = {
            name: 'Graylog', 
            level : 'silly',   
            silent: false,
            handleExceptions: true,
            graylog: {
                servers: [{ host: global.gConfig.logging.graylog.host, 
                            port: global.gConfig.logging.graylog.port }],
                hostname: 'ICAT+',
                facility: global.gConfig.logging.graylog.facility,
                bufferSize: 1400
            }
        };
        return new (WinstonGraylog2)(options);       
    }
    catch (e) {
        return null;
    } 
}

/**
 * Adding the transport propotocols into winston
 * Graylog needs to be enabled to be used
 */
function getTransportProtocols() {
    var transportProtocols = [];
    transportProtocols.push(new winston.transports.Console({level: global.gConfig.logging.console.level}));
    if (global.gConfig.logging.file.enabled) {           
        transportProtocols.push(new winston.transports.File({ filename: global.gConfig.logging.file.filename, level: global.gConfig.logging.file.level }));
    }
    if (global.gConfig.logging.graylog.enabled) {
        var graylogTransport = getWinstonGrayLogTransport();
        if (graylogTransport) {
            transportProtocols.push(graylogTransport);
        }
    }
    return transportProtocols;
};


module.exports = {
    logger: winston.createLogger({        
        format:combine(winston.format.colorize(), timestamp(),  myFormat),
        transports: getTransportProtocols()
    })
};