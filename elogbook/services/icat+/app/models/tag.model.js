/**
 * MongoDB schema
 */
/** @module mongoDBSchema */

const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

/** Tag schema */
const TagSchema = mongoose.Schema({
    /** Tag name exple 'alignement' */
    name: String,
    /** Tag descrption */
    description: String,
    /** Tag color. What color refers to is volontarily not described. */
    color: String,
    /** Beamline name. 
     * When a tag has a beamlineName property, the tag scope extends to all investigations of this beamline. 
     * When this value is null, this tag's scope is not at investigation level.
     * When this value is null, and investigationId is null, this tag's scope is global
     */
    beamlineName: String,
    /** Investigation identifier. 
     * When a tag has an investigationId property, the tag scope extends to the corresponding investigation. 
     * When a tag has beamlineName and investigationId, then beamlineName is ignored. This should not happen.
     * When this value is null, this tag's scope is not at investigation level.
     * When this value is null, and investigationId is null, this tag's scope is global
     */
    investigationId: Number

}, {
        timestamps: true
    });

module.exports = mongoose.model('Tag', TagSchema);