const cache = require('../cache/cache.js');

exports.normalize = (investigationName) => {    
  /** Get the proposal type */
  var investigationTypes= cache.getInvestigationTypes();  
  var normalizedInvestigationType = null;
  var normalizedInvestigationNumber = null;

  for (var i=0 ; i < investigationTypes.length; i++){
      var investigationTypeName = (investigationTypes[i].InvestigationType.name);
      /** replacing - */

      var investigationNameClear = investigationTypeName.replace(new RegExp('-', 'g'), "").toUpperCase();
      investigationName = investigationName.replace(new RegExp('-', 'g'), "").toUpperCase();
      
      if (investigationName.toUpperCase().startsWith(investigationNameClear)){
            normalizedInvestigationType = investigationTypeName;
            normalizedInvestigationNumber = investigationName.toUpperCase().replace(investigationNameClear, "").replace(new RegExp('-', 'g'), "");
      }     
  }
  if (normalizedInvestigationType == null || normalizedInvestigationNumber == null){
      global.gLogger.error("Investigation name can not be normalized. ", {investigationName:investigationName, normalizedInvestigationType:normalizedInvestigationType, normalizedInvestigationNumber:normalizedInvestigationNumber})
      throw "Investigation name can not be normalized"
  }

  /** ID are testing investigations and does not follow same format **/
  if (normalizedInvestigationType.toUpperCase().startsWith("ID")){
      return investigationName;
  }

  return  normalizedInvestigationType+"-"+normalizedInvestigationNumber;    
};