const _ = require('lodash');
const icat = require('../api/icat.js');
const cache = require('../cache/cache.js');
const ICAT = global.gConfig.icat;


exports.doAnonymousLogin = (req, res, next) => {
    global.gLogger.info("Panosc anonymous login");
      
    var onsuccess = (sessionId) => {        
        global.gLogger.info("Log in successfully as anonymous login as sessionId", sessionId);
        req.params["sessionId"] = sessionId;
        req.query["sessionId"] = sessionId;
        next();
    };
    var onerror = (sessionId) => {
        var err = new Error('Error when log in as anonymous ' + ICAT.anonymous);
        err.status = 403;
        onError(err);
    };
    global.gLogger.info("Panosc anonymous login", ICAT.anonymous);
    icat.getSession(ICAT.anonymous, onsuccess, onerror);
}
/**
 * This method will check if the release date is < now
 */
function isInvestigationUnderEmbargo(sessionId, investigationId, next, onError) {
    function onSuccess(response) {
        if (response) {
            if (response[0]) {
                if (response[0].Investigation) {
                    if (response[0].Investigation.releaseDate) {
                        if (new Date(response[0].Investigation.releaseDate) < new Date()) {
                            global.gLogger.info("Investigation is not under embargo", { name: response[0].Investigation.name, releaseDate: response[0].Investigation.releaseDate });
                            next();
                            return;
                        }
                    }
                }
            }
        }
        var err = new Error('Investigation is under embargo');
        err.status = 403;
        onError(err);
    }
    icat.getInvestigationById(sessionId, investigationId, onSuccess, onError);
}


exports.allowAdministrators = (req, res, next) => {
    var sessionId = getSessionId(req);
    if (cache.isAdministrator(getSessionId(req))) {
        global.gLogger.info("Allowed as sessionId corresponds to an administrator", { sessionId: sessionId, username: cache.getUsernameBySessionId(sessionId) });
        next();
        return;
    }
    next(new Error("Unauthorized. Not administrator privileges"))
}

exports.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser = (req, res, next) => {
    var _this = this;

    global.gLogger.info("allowsInvestigationAfterEmbargoOrAllowsInvestigationUser");
    var sessionId = getSessionId(req);
    if (cache.isAdministrator(sessionId)) {
        global.gLogger.info("Allowed as sessionId corresponds to an administrator", { sessionId: sessionId, username: cache.getUsernameBySessionId(sessionId) });
        next();
        return;
    }

    /** This happens if investigation is not anymore under embargo then it allows */
    function isEmbargoEnd() {
        global.gLogger.info("isEmbargoEnd");
        next();
    }

    /** This happens when investigation is under embargo then 
     * it allows if and only if user is a participant (InvestigationUser)     
     */
    function isUnderEmbargo(error) {
        global.gLogger.info("isUnderEmbargo");
        _this.allowsInvestigationUserOrAdministrators(req, res, next);
    }
    isInvestigationUnderEmbargo(getSessionId(req), getInvestigationId(req), isEmbargoEnd, isUnderEmbargo);
}


/**
* Permission validator by investigationId: it allows the following 'person' (identified by its sessionId) to access the resource associated to a given investigationId:
* - administrators
* - investigationUsers , ie participants of the investigation indentified by investigationId
* @param {Request} req http request. It should contain sessionId and investigationId
* @param {Response} res http response
* @param {callback} next the next function
*/
exports.allowsInvestigationUserOrAdministrators = (req, res, next) => {
    let sessionId = getSessionId(req);
    let investigationId = getInvestigationId(req);

    if (cache.isAdministrator(sessionId)) {
        global.gLogger.info("Allowed as sessionId corresponds to an administrator", { sessionId: sessionId, username: cache.getUsernameBySessionId(sessionId) });
        next();
        return;
    }
    if (sessionId) {
        function onInvestigationUserRetrieved(investigationUsers) {
            global.gLogger.info("Investigation users retrieved by session is", { investigationUsers: investigationUsers });
            if (_.find(investigationUsers, function (inv) { return inv.investigationId == investigationId })) {
                global.gLogger.info("User is a participant of the investigation", { investigationUsers: investigationUsers });
                next();
                return;
            }
            else {
                global.gLogger.error('User is not a participant of the investigation');
                var err = new Error('User is not a participant of the investigation');
                err.status = 403;
                next(err);
                return;
            }
        }
        function onInvestigationUserError(error) {

            var err = new Error('Error produced when retrieving investigation user');
            err.status = 403;
            next(err);
            return;
        }

        // Ask icat server to provide all participations (ie as participant) of the user identified by sessionId
        icat.getInvestigationUserBySessionId(sessionId, onInvestigationUserRetrieved, onInvestigationUserError);
    }
}


/**
* Permission validator by instrumentName: it allows the following 'person' (identified by its sessionId) to access the resource associated to a given investigationId:
* - administrators
* - beamline responsible, 
* @param {Request} req http request. It should contain sessionId and instrumentName
* @param {Response} res http response
* @param {callback} next the next function
*/
exports.allowsBeamlineResponsibleOrAdministrators = (req, res, next) => {
    let sessionId = getSessionId(req);
    let beamlineName = getInstrumentName(req).toUpperCase();

    if (sessionId) {
        if (beamlineName) {
            onSuccess = (users) => {
                global.gLogger.info(JSON.stringify(users))

                // check whether the current user identified by its sessionId is listed in the response

                let requesterUsername = cache.getSessionById(sessionId).username;
                global.gLogger.info(JSON.stringify(requesterUsername))

                let foundUser = _.find(users, (user) => {
                    global.gLogger.info(JSON.stringify(user))
                    global.gLogger.info(JSON.stringify(user.User))
                    global.gLogger.info(JSON.stringify(user.User.name))

                    return user.User.name === requesterUsername
                })

                global.gLogger.info(JSON.stringify(foundUser))

                if (foundUser) {
                    //requesterUsername is a instrumentscientist for the given beamline. Access granted
                    next()
                } else {
                    res.status(403).send("You are not allowed to create a bemline tag.")
                }
            }

            onError = (error) => {
                global.gLogger.error("getIsAInstrumentScientistForInstrumentName request failed.")
                global.gLogger.error(JSON.stringify(error))
                res.status(500).send("Could not determine whether the user is has permission to create a beamline tag.")

            }

            icat.getIsAInstrumentScientistForInstrumentName(sessionId, beamlineName, onSuccess, onError)
        } else {
            res.status(400).send("Error 400 - Please provide an instrument name")
        }
    } else {
        res.status(403).send("Error 403 - Please provide a sessionId")
    }
}



function getInvestigationId(req) {
    if (req.params) {
        if (req.params.investigationId) {
            return req.params.investigationId;
        }
    }
    if (req.header) {
        if (req.header.investigationId) {
            return req.header.investigationId;
        }
    }
    if (req.body) {
        if (req.body.investigationId) {
            return req.body.investigationId;
        }
    }
    if (req.query) {
        if (req.query.investigationId) {
            return req.query.investigationId;
        }
    }
    global.gLogger.error("No investigationId @ header, body or query");
    return null;
}

function getInstrumentName(req) {
    if (req.params) {
        if (req.params.instrumentName) {
            return req.params.instrumentName;
        }
    }
    if (req.header) {
        if (req.header.instrumentName) {
            return req.header.instrumentName;
        }
    }
    if (req.body) {
        if (req.body.instrumentName) {
            return req.body.instrumentName;
        }
    }
    if (req.query) {
        if (req.query.instrumentName) {
            return req.query.instrumentName;
        }
    }
    global.gLogger.error("No instrumentName @ header, body or query");
    return null;
}

function getSessionId(req) {
    if (req.params) {
        if (req.params.sessionId) {
            return req.params.sessionId;
        }
    }

    if (req.header) {
        if (req.headers.sessionid) {
            return req.headers.sessionid;
        }
    }
    if (req.body) {
        if (req.body.sessionId) {
            return req.body.sessionId;
        }
    }
    if (req.query) {
        if (req.query.sessionId) {
            return req.query.sessionId;
        }
    }
    global.gLogger.error("No sessionId @ header, body or query");
    return null;
}

/**
 * Validates that the provided Token. 
 * Error 400 not handled because this method is not called when sessionId is missing
 * Triggers error 403 when the token is not valid
 */
exports.requiresMagicToken = (req, res, next) => {
    /** Change this */
    let sessionId = getSessionId(req);
    if (sessionId == global.gConfig.server.API_KEY) {
        next();
        return;
    }
    let err = new Error('SessionId does not match API_KEY.');
    err.status = 403;
    return next(err);
}

exports.requiresSession = (req, res, next) => {
    /** Change this */
    var sessionId = getSessionId(req);    
    if (sessionId == null) {
        var err = new Error('You must be logged in to view this page.');
        err.status = 400;
        return next(err);
    }

    next();
}

exports.requiresLogin = (req, res, next) => {
    /** Change this */
    var sessionId = getSessionId(req);
    var investigationId = getInvestigationId(req);

    if (sessionId == null) {
        var err = new Error('You must be logged in to view this page.');
        err.status = 400;
        return next(err);
    }
    if (investigationId == null) {
        var err = new Error('InvestigationId is missing');
        err.status = 400;
        return next(err);
    }

    validateInvestigation(sessionId, investigationId, next, req, res);
}


exports.validateSession = (req, res, next) => {
    /** Change this */
    var sessionId = getSessionId(req);
    global.gLogger.info("Validating session", { sessionId: sessionId });
    if (sessionId == null) {
        var err = new Error('You must be logged in to view this page');
        err.status = 401;
        return next(err);
    }

    function onError(error) {
        global.gLogger.error("Invalid session" + sessionId)
        var err = new Error("Invalid session" + sessionId);
        err.status = 401;
        return next(err);
    }

    function onSuccess(data) {
        /** Session is valid */
        next();
    }
    icat.getSessionInformation(sessionId, onSuccess, onError);

}


/**
 * Check that the user identified by sessionID has permission to read investigationId
 * @param {string} sessionId sessionId identifying the user
 * @param {string} investigationId investigation identifier
 * @param {function} next express callback function 
 */
function validateInvestigation(sessionId, investigationId, next) {

    if (sessionId) {
        var session = cache.getSessionById(sessionId);
        if (cache.isAdministrator(sessionId)) {
            next();
            return;
        }
        if (session) {
            var found = cache.getMyInvestigationsBySessionId(sessionId).find(function (element) {
                return Number(element.Investigation.id) == Number(investigationId);
            });
            if (found) {
                global.gLogger.debug("Allowed investigation", { sessionId: sessionId, investigationId: investigationId });
                next();
                return;
            }
            else {
                global.gLogger.debug("Investigation not allowed");
                var err = new Error('Investigation not allowed');
                err.status = 401;
                next(err);
                return;
            }

        }
    }
    global.gLogger.debug("Session not found in the cache: " + sessionId);
    var err = new Error('Session not found in the cache');
    err.status = 401;
    next(err);
    return;
}
