


function getParameters(dataset){
  var params = [];
  for (let i = 0; i < dataset.Dataset.parameters.length; i++) {
      let parameter =   dataset.Dataset.parameters[i];
      params.push({
        name  : parameter.type.name,
        value : parameter.stringValue
      })
  }
  return params;
}


exports.parse = (datasets) => {  
  var parsed = [];
  
  for (let i = 0; i < datasets.length; i++) {
    const element = datasets[i];
    parsed.push({
      id                : element.Dataset.id,
      name              : element.Dataset.name,
      startDate         : element.Dataset.startDate,
      endDate           : element.Dataset.endDate,
      location          : element.Dataset.location,
      investigation     :  element.Dataset.investigation,
      sampleName        : element.Dataset.sample.name,
      parameters        : getParameters(element)

    })
  }
  return parsed;
  
    
};
