var cache = require("../../cache/cache.js");

 
exports.parse = (investigations) => {
  try {
    debugger
    var parameterTypes = cache.getParameterTypeDictionary();
    var result = [];
    for (var i in investigations) {
      var investigation = investigations[i];
      var parameters = [];
      if (investigation) {
        if (investigation.Investigation) {
          if (investigation.Investigation.parameters) {
            for (var j = 0; j < investigation.Investigation.parameters.length; j++) {
              var parameterTypeId = investigation.Investigation.parameters[j].type.id;              
              var parameterTypeName = parameterTypes[parameterTypeId];
              parameters.push({
                "name": parameterTypeName,
                "value": investigation.Investigation.parameters[j].stringValue
              });
            }
          }
        }
      }
      investigation.Investigation.parameters = parameters;
      result.push(investigation);
    }
  
  }
  catch (e) {
    global.gLogger.error(e)
  }
  return result;
};