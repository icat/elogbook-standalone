var cache = require("../../cache/cache.js");
exports.parse = (datacollections) => {  
  var parameterTypes = cache.getParameterTypeDictionary();
  var result = [];
  for (var i =0; i < datacollections.length; i++){
      if (datacollections[i]){
        if (datacollections[i].DataCollection){ 
            if (datacollections[i].DataCollection.parameters){
              var parameters = [];
              for (var j =0; j < datacollections[i].DataCollection.parameters.length; j++){                
                 var parameterTypeId = datacollections[i].DataCollection.parameters[j].type.id; 
                  parameters.push({
                    "name": parameterTypes[parameterTypeId],
                    "value": datacollections[i].DataCollection.parameters[j].stringValue
                  });
              }
            }
            datacollections[i].DataCollection.parameters = parameters;
            result.push( datacollections[i].DataCollection);
        }
      }
  }
  return result;    
    
};