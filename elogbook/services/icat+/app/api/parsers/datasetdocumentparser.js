_ = require('lodash');


/**
 * This functions unflatter a parameter name and value
**/
function mergeParameterNameAndValue(parameterName, name, value){   
    function parseToFloat(value){
        try{
            return parseFloat(value);
        }
        catch(e){
            return NaN;
        }
    }

    if (parameterName){
        if(name){
            if(value){
                var names = name.trim().split(" ");
                var values = value.trim().split(" ");
                if (names.length == values.length){
                    var merge = [];
                    for (var i = 0; i < names.length; i++){
                        merge.push({
                            name : names[i],
                            numericValue : parseToFloat(values[i]),
                            stringValue : values[i]
                        });                        
                    }                   
                    return {
                        name : parameterName.replace("_name", ""),
                        value : merge
                    };
                }
            }
        }
    }
    return null;
    
}

/**
 * This function convert a dataset into a dataset_document
**/
exports.parse = (datasets) => {  
    return _.map(datasets, function (dataset){ 
        try{
            var parametersMerged = []; 
            var parametersDictionary = {};
            for (var i = 0; i < dataset.parameters.length; i++){
                parametersDictionary[dataset.parameters[i].name] = dataset.parameters[i].value;
                /** parameters starting by __ are not allowed on elastic search */
                dataset[dataset.parameters[i].name.replace("__", "")] = dataset.parameters[i].value;

                /** Converting string to number */
                if (dataset.parameters[i].name === "InstrumentMonochromator_energy"){
                    dataset[dataset.parameters[i].name.replace("__", "")] = parseFloat(dataset.parameters[i].value);
                }
                if (dataset.parameters[i].name === "InstrumentMonochromator_wavelength"){
                    dataset[dataset.parameters[i].name.replace("__", "")] = parseFloat(dataset.parameters[i].value);
                }
                if (dataset.parameters[i].name === "InstrumentSource_current"){
                    dataset[dataset.parameters[i].name.replace("__", "")] = parseFloat(dataset.parameters[i].value);
                }
                
            }
            
            /** parameters ending by _name */
            let composedParameters = _.filter(dataset.parameters, function(parameter){ return parameter.name.endsWith("_name")});       
            
            for (var i = 0; i < composedParameters.length; i++){
               var parameterMerged = mergeParameterNameAndValue(composedParameters[i].name, parametersDictionary[composedParameters[i].name], parametersDictionary[composedParameters[i].name.replace("_name", "_value")]);
               if (parameterMerged){
                    parametersMerged.push(parameterMerged);
               }
            }
           
            for (var i = 0; i < parametersMerged.length; i++){
                dataset[parametersMerged[i].name] = parametersMerged[i].value;
            }            
            
            /** Investigation parameters */                        
            dataset["investigationId"] = dataset.investigation.id;
            dataset["investigationName"] = dataset.investigation.name;
            dataset["investigationSummary"] = dataset.investigation.summary;
            dataset["investigationTitle"] = dataset.investigation.title;
            dataset["investigationVisitId"] = dataset.investigation.visitId;
            dataset["investigationReleaseDate"] = dataset.investigation.releaseDate;
            
            function notNull(value){
                if ((!value) || value == ""){
                    return "";
                }
                return value;
            }
            dataset["escompactsearch"] = notNull(dataset.investigation.visitId) + " " +  
                                         notNull(dataset.investigation.summary) + " " +
                                         notNull(dataset.investigation.name) + " " +
                                         notNull(dataset.InstrumentMonochromatorCrystal_reflection) + " " +
                                         notNull(dataset.InstrumentMonochromatorCrystal_type) + " " +
                                         notNull(dataset.InstrumentMonochromatorCrystal_usage) + " " +
                                         notNull(dataset.definition) + " "
                                         notNull(dataset.name) + " "
                                         notNull(dataset.Sample_name);

            dataset["estype"] = "dataset";

            delete dataset.parameters;
            delete dataset.investigation;            
            return dataset;
        }
        catch(e){
            console.log(e);
        }
        return null;
        
    
    });
}