"use strict";

const ICAT = global.gConfig.icat;
const IDS = global.gConfig.ids;

const GET_SESSION = "sessionId=:sessionId";

const ICAT_ENTITY_MANAGER = ICAT.server + "/icat/entityManager?";

const SERVER = "&server=" + ICAT.server;

const GET_INVESTIGATIONS_ID = "SELECT distinct(Inv.id) from Investigation Inv";


const GET_DATASETS_IDS_BY_INVESTIGATION_ID = "SELECT dataset.id FROM Dataset dataset JOIN dataset.investigation investigation where investigation.id =:investigationId";

const GET_DATASETS_DATASET_IDS= "SELECT dataset FROM Dataset dataset JOIN dataset.investigation investigation where dataset.id IN (:datasetIDs) INCLUDE dataset.investigation investigation, dataset.parameters parameters, parameters.type, dataset.sample" ;

const GET_INVESTIGATION_BY_DATASETS_ID = "SELECT Inv from Investigation Inv JOIN Inv.datasets ds where ds.id in (:datasetIds) include Inv.investigationInstruments ii, ii.instrument";

const GET_INVESTIGATIONUSER_BY_INVESTIGATION_ID = "SELECT user.name, user.fullName, investigationUser.role, inv.name, inv.id FROM InvestigationUser investigationUser, investigationUser.user as user, investigationUser.investigation as inv where inv.id in (:investigationIds) ";

const GET_INVESTIGATIONUSER_BY_USERNAME = "SELECT user.name, user.fullName, investigationUser.role, inv.name, inv.id FROM InvestigationUser investigationUser, investigationUser.user as user, investigationUser.investigation as inv where user.name = ':username'";

const GET_INVESTIGATION_BY_INSTRUMENTSCIENTIST = "select distinct investigation from Investigation investigation , investigation.investigationInstruments as investigationInstrument, investigationInstrument.instrument as instrument, instrument.instrumentScientists as instrumentScientists, instrumentScientists.user as user where user.name = ':username' order by investigation.startDate DESC include investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type ";


exports.getPropertiesQuery = () => {
    return ICAT.server + "/icat/properties";
};

exports.getSessionQuery = (sessionId) => {
    return GET_SESSION.replace(":sessionId", sessionId);
};

exports.getSession = (sessionId) => {
    return ICAT.server + "/icat/session";
};

exports.getSessionInformationURL = (sessionId) => {
    return this.getSession() + "/" + sessionId;
};

exports.getInvestigationUserByInvestigationIdURL = (sessionId, investigationIds) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_INVESTIGATIONUSER_BY_INVESTIGATION_ID.replace(":investigationIds", investigationIds) + SERVER;
};

exports.getInvestigationUserByUserNameURL = (sessionId, username) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_INVESTIGATIONUSER_BY_USERNAME.replace(":username", username) + SERVER;
};

exports.getInvestigations = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_INVESTIGATIONS_ID + SERVER;
};

exports.getInvestigationsByDatasetListIdsURL = (sessionId, datasetIds) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_INVESTIGATION_BY_DATASETS_ID.replace(":datasetIds", datasetIds) + SERVER;
};

exports.getInvestigationByInstrumentScientist = (sessionId, username) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_INVESTIGATION_BY_INSTRUMENTSCIENTIST.replace(":username", username) + SERVER;
};


exports.getCreateDataCollectionQuery = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId);
};

exports.getParameterTypeList = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=Select p from ParameterType p" + SERVER;
};

exports.getInvestigationTypes = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=Select p from InvestigationType p" + SERVER;
};

exports.getInvestigationByName = (sessionId, investigationName) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT Inv from Investigation Inv where UPPER(inv.name) = UPPER('" + investigationName + "') INCLUDE Inv.investigationInstruments i, i.instrument, Inv.parameters" + SERVER;
};

exports.getInvestigationById = (sessionId, investigationId) => {                                                                                        
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT Inv from Investigation Inv where inv.id in (" + investigationId + ") INCLUDE Inv.investigationInstruments i, i.instrument, Inv.parameters p, p.type" + SERVER;
};

exports.getAllInvestigationsBySessionId = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=select distinct investigation from Investigation investigation  where investigation.releaseDate < CURRENT_TIMESTAMP order by investigation.startDate DESC include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type" + SERVER;
};

exports.getEmbargoedInvestigationsBySessionId = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=select distinct investigation from Investigation investigation  where investigation.releaseDate > CURRENT_TIMESTAMP order by investigation.startDate DESC include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type" + SERVER;
};

exports.getReleasedInvestigationsBySessionId = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=select distinct investigation from Investigation investigation  where investigation.releaseDate < CURRENT_TIMESTAMP and investigation.doi <> null order by investigation.startDate DESC include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type" + SERVER;
};

exports.getInvestigationsBySessionIdAndUsername = (sessionId, username) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=select distinct investigation from Investigation investigation , investigation.investigationUsers as investigationUserPivot ,  investigationUserPivot.user as investigationUser where investigationUser.name = '" + username + "' order by investigation.startDate DESC include  investigation.investigationInstruments investigationInstruments, investigationInstruments.instrument instrument, investigation.parameters p, p.type" + SERVER;
};

exports.getEntityDatasetsByInvestigationId = (sessionId, investigationId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT dataset FROM Dataset dataset JOIN dataset.investigation investigation where investigation.id =:investigationId INCLUDE  dataset.investigation investigation, dataset.parameters parameters, parameters.type, dataset.sample".replace(":investigationId", investigationId) + SERVER;
};

/**
 * @Deprecated it takes too long, we presume because of rules
 */
exports.getDatasetsByDatasetIds = (sessionId, datasetIds) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_DATASETS_DATASET_IDS.replace(":datasetIDs", datasetIds) + SERVER;
};

exports.getDatasetIdsByInvestigationId = (sessionId, investigationId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_DATASETS_IDS_BY_INVESTIGATION_ID.replace(":investigationId", investigationId) + SERVER;
};

exports.getDataColletionByDatasetId = (sessionId, datasetId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT dc from DataCollection dc JOIN dc.dataCollectionDatasets dcds  JOIN dcds.dataset ds where dc.doi is not null and ds.id=" + datasetId + " include dc.parameters p, p.type" + SERVER;
};

exports.getDataColletionsBySessionId = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT distinct(dc) from DataCollection dc JOIN dc.dataCollectionDatasets dcds JOIN dcds.dataset ds where dc.doi is not null include dc.parameters p, p.type, dc.dataCollectionDatasets dcds, dcds.dataset" + SERVER;
};

exports.getInvestigationIdByDOI = (sessionId, doi) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT inv.id from Investigation inv where inv.doi='" + doi + "'" + SERVER;
};


exports.getInvestigationByDOI = (sessionId, doi) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT inv from Investigation inv where inv.doi='" + doi + "' INCLUDE inv.investigationUsers invUser, invUser.user, inv.type" + SERVER;
};

exports.getDataColletionIdListByDOI = (sessionId, doi) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT ds.id from Dataset ds JOIN ds.dataCollectionDatasets dcds  JOIN dcds.dataCollection dc where dc.doi='" + doi + "'" + SERVER;
};

/**
 * Get the url to retrieve datasets from a dataset list
 * @param {string} datasetIds a comma separated string containing different datasetId
 * @param {string} sessionId the sessionId
 */
exports.getDatasetsByIdList = (sessionId, datasetIds) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + GET_DATASETS_DATASET_IDS.replace(":datasetIDs", datasetIds) + SERVER;
};

exports.getUsersByGroupName = (sessionId, groupNames) => {
    var query = "SELECT user from User user JOIN user.userGroups userGroup JOIN userGroup.grouping grouping where  grouping.name in (':groupNames')";
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + query.replace(":groupNames", groupNames) + SERVER;
};

exports.getUsers = (sessionId) => {
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=SELECT user from User user" + SERVER;
};

exports.getDatafilesByDatasetId = (sessionId, datasetIds) => {
    var query = "select distinct datafile from Datafile datafile , datafile.dataset as dataset  where dataset.id IN (:datasetIds)";
    global.gLogger.debug(ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + query.replace(":datasetIds", datasetIds) + SERVER);
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + query.replace(":datasetIds", datasetIds) + SERVER;
};

exports.getStatusByDatasetIds = (sessionId, datasetIds) => {
    var query = IDS.server + "/ids/getStatus?sessionId=:sessionId&datasetIds=:datasetIds"
        .replace(":sessionId", sessionId)
        .replace(":datasetIds", datasetIds)
    global.gLogger.debug(query);
    return query;
};

exports.getStatusByIvestigationIds = (sessionId, investigationIds) => {
    var query = IDS.server + "/ids/getStatus?sessionId=:sessionId&investigationIds=:investigationIds"
        .replace(":sessionId", sessionId)
        .replace(":investigationIds", investigationIds)
    global.gLogger.debug(query);
    return query;
};


/**
 * JPQL query which is used to find out whether a given user (identifier by its sessionID) is a instrument scientist for a given intrumentName (=beamline). 
 * This query does not return all the instrumentscientists for the given intrumentName
 */
exports.getIsAInstrumentScientistForInstrumentName = (sessionId, instrumentName) => {
    let query = "SELECT u FROM InstrumentScientist insts JOIN insts.user u WHERE insts.instrument.name=':InstrumentName'"
        .replace(':InstrumentName', instrumentName)

    global.gLogger.debug(query);
    return ICAT_ENTITY_MANAGER + this.getSessionQuery(sessionId) + "&" + "query=" + query + SERVER;
}



