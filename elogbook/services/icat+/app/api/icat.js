const axios = require('axios-https-proxy-fix');
const datasetParser = require('./parsers/datasetparser.js');
const investigationParser = require('./parsers/investigationparser.js');
const datacollectionParser = require('./parsers/datacollectionparser.js');

var qs = require('qs');
const cache = require('../cache/cache.js');
const icatQuery = require('./query.js');
const proxy = false

exports.getPropertiesQuery = (onSuccess, onError) => {
    doGet(icatQuery.getPropertiesQuery(), onSuccess, onError);
}


function parseInvestigationUserToObject(investigationUserRecord) {
    return { name: investigationUserRecord[0], fullName: investigationUserRecord[1], role: investigationUserRecord[2], investigationName: investigationUserRecord[3], investigationId: investigationUserRecord[4] };
}
/**
 * This method will retrieve an array of objects with the information concerning the investigation user
 * 
 * Example : [{"name":"reader","role":"Principal investigator","investigationName":"IH-CH-1336","investigationId":77570462}]
 * 
 */
exports.getInvestigationUserByInvestigationId = (sessionId, investigationIds, onSuccess, onError) => {
    axios.get(icatQuery.getInvestigationUserByInvestigationIdURL(sessionId, investigationIds), { proxy })
        .then(response => {
            /** Parsing the data **/
            onSuccess(response.data.map(function (o) { return parseInvestigationUserToObject(o); }));
        })
        .catch(error => {
            try {
                var err = new Error(error.response.data.message);
                err.status = error.response.status;
                onError(err);
            }
            catch (e) {
                global.gLogger.error(e);
            }
        });
};

/**
 * This method will retrieve an array of objects with the information concerning the investigation user, his role and investigation.
 * @example 
 * [{"name":"reader","role":"Principal investigator","investigationName":"IH-CH-1336","investigationId":77570462}]
 * @param {string} sessionId
 * @param {string} username - The second color, in hexadecimal format.
 * @param {function} onSuccess - callback function if succeed
 * @param {function} onError - callback function if error
 * @return {array} Array of objects containing: name, role, investigationName and investigationId
 */
exports.getInvestigationUserByUserName = (sessionId, username, onSuccess, onError) => {
    axios.get(icatQuery.getInvestigationUserByUserNameURL(sessionId, username), { proxy })
        .then(response => {
            onSuccess(response.data.map(function (o) { return parseInvestigationUserToObject(o); }));
        })
        .catch(error => {
            console.log(error)
            try {
                var err = new Error(error.response.data.message);
                err.status = error.response.status;
                onError(err);
            }
            catch (e) {
                global.gLogger.error(e);
            }
        });
};

/**
 * Get the information of the sessionId or return 403 if session is not found. Documentation: https://repo.icatproject.org/site/icat/server/4.8.0/miredot/index.html#669420320
 * @example 
 * {"userName":"db/root","remainingMinutes":117. 87021666666666}
 * @param {string} sessionId
 * @param {function} onSuccess - callback function if succeed
 * @param {function} onError - callback function if error
 * @return {json} JSON object with username and remainingMinutes fields
 */
exports.getSessionInformation = (sessionId, onSucess, onError) => {
    axios.get(icatQuery.getSessionInformationURL(sessionId), { proxy })
        .then(response => {
            global.gLogger.info("Session information received successfully", { data: response.data });
            onSucess(response.data)
        })
        .catch(error => {
            try {
                global.gLogger.error("[ERROR] Session information reception failed", { error: error });
                global.gLogger.error(error.response.data.message);
                onError(error);
            }
            catch (e) {
                global.gLogger.error(e);
            }
        });
};

/**
 * Get all participations of a user as experimental team member
 * @param {string} sessinId sessionIdentifier which characterize the user
 * @param {*} onSuccess callback function triggered on success
 * @param {*} onError callback function triggered on error
 */
exports.getInvestigationUserBySessionId = (sessionId, onSuccess, onError) => {
    var success = (data) => {
        this.getInvestigationUserByUserName(sessionId, data.userName, onSuccess, onError);
    };
    var error = (error) => {
        try {
            onError(error);
        }
        catch (e) {
            global.gLogger.error(e);
        }
    };
    this.getSessionInformation(sessionId, success, error);
};


exports.getProxy = () => {
    return false;
};



/**
 * Create an ICAT session given a credential
 * @param {*} credentials the crendential to create the new session with
 * @param {*} onSuccess callback triggered when the request succeeded
 * @param {*} onError callback triggered when the request failed
 */
exports.getSession = (credentials, onSuccess, onError) => {
    const data = qs.stringify({
        json: JSON.stringify(credentials)
    });
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    };
   

    axios.post(icatQuery.getSession(), data, headers)
        .then(function (response) {
            onSuccess(response.data.sessionId);
        })
        .catch(function (error) {
           
            onError(error);
        });
};

exports.updateDataCollectionDOI = (authentication, dataCollectionId, doi, onSuccess, onError) => {
    function onSessionSuccess(sessionId) {
        var dataCollection = [
            {
                'DataCollection': {
                    'id': dataCollectionId,
                    'doi': doi
                }
            }
        ];
        const data = qs.stringify({
            sessionId: sessionId,
            entities: JSON.stringify(dataCollection)
        });

        function onDataCollectionCreated(dataCollectionId) {
            global.gLogger.info("[SUCCESS] Data Collection has been updated");
            onSuccess(dataCollectionId);
        }

        function onDataCollectionError(error) {
            global.gLogger.error("[ERROR] Data Collection could not be updated", error);
            onError(error);
        }
        doPost(icatQuery.getCreateDataCollectionQuery(sessionId), data, onDataCollectionCreated, onDataCollectionError);
    }
    /** It gets a new session for the minter user */
    this.getSession(authentication, onSessionSuccess);
};


function doPost(endPoint, data, onSuccess, onError) {
    const headers = {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    };
    axios.post(endPoint, data, headers)
        .then(function (response) {
            onSuccess(response.data);

        })
        .catch(function (error) {
            onError(error);
        });
}

function createParameterType(parameterType, value) {
    return {
        type: { id: parameterType[0].ParameterType.id },
        stringValue: value.toString()
    };
}
/**
 * This method will create the data collection composed by the datasetIdList
 */
exports.mint = (authentication, datasetIdList, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName, next, onError) => {  
    if (abstract.length > 4000){
        global.gLogger.debug('[mint] Abstract is longer than 4000 chars', { abstract: abstract });
    }  
    function onSuccess(sessionId) {
        
        global.gLogger.info('[mint ICAT SUCCESS] New session for the minter user received', { sessionId: sessionId });
        var parameters = [];
        /** Getting parameters: title, abstract */
        var parameterTypes = cache.getParameterTypes();
        if (parameterTypes) {
            var titleParameterType = (cache.getParameterTypes().filter(function (p) { return p.ParameterType.name == "title"; }));
            var abstractParameterType = (cache.getParameterTypes().filter(function (p) { return p.ParameterType.name == "abstract"; }));
            var mintedByNameParameterType = (cache.getParameterTypes().filter(function (p) { return p.ParameterType.name == "mintedByName"; }));
            var mintedByFullNameParameterType = (cache.getParameterTypes().filter(function (p) { return p.ParameterType.name == "mintedByFullName"; }));
            var investigationNamesParameterType = (cache.getParameterTypes().filter(function (p) { return p.ParameterType.name == "investigationNames"; }));
            var instrumentNamesParameterType = (cache.getParameterTypes().filter(function (p) { return p.ParameterType.name == "instrumentNames"; }));
            /** Setting dataCollection Parameters */
            if (titleParameterType) {
                parameters.push(createParameterType(titleParameterType, title));
            }
            else {
                global.gLogger.warn("ParameterType title has not been found");
            }
            if (abstractParameterType) {
                parameters.push(createParameterType(abstractParameterType, abstract));
            }
            else {
                global.gLogger.warn("ParameterType abstract has not been found");
            }

            if (investigationNamesParameterType) {
                parameters.push(createParameterType(investigationNamesParameterType, investigationNames));
            }
            else {
                global.gLogger.warn("ParameterType investigationNames has not been found");
            }

            if (instrumentNamesParameterType) {
                parameters.push(createParameterType(instrumentNamesParameterType, instrumentsNames));
            }
            else {
                global.gLogger.warn("ParameterType instrumentNames has not been found");
            }

            if (mintedByNameParameterType) {
                parameters.push(createParameterType(mintedByNameParameterType, mintedByName));
            }
            else {
                global.gLogger.warn("ParameterType mintedByName has not been found");
            }

            if (mintedByFullNameParameterType) {
                parameters.push(createParameterType(mintedByFullNameParameterType, mintedByFullName));
            }
            else {
                global.gLogger.warn("ParameterType mintedByFullName has not been found");
            }
        }
        else {
            global.gLogger.warn("There are no parameter types retrieved in cache");
        }

        var dataCollection = [
            {
                'DataCollection': {
                    'dataCollectionDatasets': datasetIdList.map(function (id) { return { 'dataset': { id: id } }; }),
                    "parameters": parameters
                }
            }
        ];
        const data = qs.stringify({
            sessionId: sessionId,
            entities: JSON.stringify(dataCollection)
        });

        function onDataCollectionCreated(dataCollectionId) {
            global.gLogger.info('[SUCCESS] DataCollection created in the metadata catalogue');
            next(dataCollectionId);
        }

        function onDataCollectionError(error) {
            global.gLogger.error('[ERROR] DataCollection creation failed');
            onError(error);
        }
        /** This creates a datacollection */
        doPost(icatQuery.getCreateDataCollectionQuery(sessionId), data, onDataCollectionCreated, onDataCollectionError);

    }
    
    /** It gets a new session for the minter user */
    this.getSession(authentication, onSuccess);
};


/** Return all datasets by investigationId 
 * @Deprecated as it is too slow 
exports.getDatasetsByInvestigationId = (sessionId, investigationId, onSucess, onError) => {
    function successParsing(response) {
        try{
            global.gLogger.info("successParsing", {entities : response.length});
            onSucess(datasetParser.parse(response));
        }
        catch(e){
            global.gLogger.error(e);
            onError(e);    
        }
    }

    function onError(response) {
        console.log(error);
        onError(response);
    }
    global.gLogger.info(icatQuery.getDatasetsByInvestigationId(sessionId, investigationId));
    doGet(
        icatQuery.getDatasetsByInvestigationId(sessionId, investigationId),
        successParsing,
        onError);
}; 
**/

exports.getDatasetsByInvestigationId = (sessionId, investigationId, onSucess, onError) => {
    function successParsing(response) {
        try{
            global.gLogger.info("Datasets retrieved", {entities : response.length});
            onSucess(datasetParser.parse(response));
        }
        catch(e){
            global.gLogger.error(e);
            onError(e);    
        }
    }

    function onError(error) {
        console.log(error);
        onError(error);
    }
    
    global.gLogger.info(icatQuery.getEntityDatasetsByInvestigationId(sessionId, investigationId));
    doGet(
        icatQuery.getEntityDatasetsByInvestigationId(sessionId, investigationId),
        successParsing,
        onError);
}; 


/*
exports.getDatasetsByInvestigationId = (sessionId, investigationId, onSucess, onError) => {
    function onGetIdSuccess(response){         
        if (response.length > 0){   
            console.log("Retrieving....") 
            console.log(icatQuery.getDatasetsByDatasetIds(sessionId, response.slice(0, 700)))           
            doGet(
                icatQuery.getDatasetsByDatasetIds(sessionId, response.slice(0, 700)),
                successParsing,
                onError);
        }
        else{
            onSucess([]);
        }
    }
    function successParsing(response) {
        try{                             
            var investigation = cache.getInvestigationById(sessionId, investigationId);                            
            onSucess(datasetParser.parse(response, investigation));
        }
        catch(e){
            global.gLogger.error(e);
            onError(e);    
        }
    }
    function onError(response) {  
        console.log(response)     
        onError(response);
    }
    global.gLogger.info(icatQuery.getDatasetsByInvestigationId(sessionId, investigationId));
    doGet(
        icatQuery.getDatasetIdsByInvestigationId(sessionId, investigationId),
        onGetIdSuccess,
        onError);
};
*/


/** Return all datasets by DOI, it means there are in a collection or public investigation */
exports.getDatasetsByDOI = (sessionId, doi, onSucess, onError) => {
    var _this = this;
    function successDataCollectionSearch(datasetIds) {
        if (datasetIds) {
            if (datasetIds.length > 0) {
                global.gLogger.info("DOI found in datacollection", { "doi": doi, "datasetIds": datasetIds });
                /** Found as DataCollection */
                _this.getDatasetsById(sessionId, datasetIds, onSucess, onError);
            }
            else {
                var successInvestigationSearch = (investigationId) => {
                    global.gLogger.info("DOI found in investigation.", { "doi": doi, "investigationId": investigationId });
                    /** Found as Investigation */
                    _this.getDatasetsByInvestigationId(sessionId, investigationId, onSucess, onError);
                };
                /** Look for investigation */
                global.gLogger.info("DOI not found in datacollections.", { "doi": doi });
                global.gLogger.info("Looking doi in investigations. ", { "doi": doi });
              
                doGet(
                    icatQuery.getInvestigationIdByDOI(sessionId, doi),
                    successInvestigationSearch,
                    onError);

            }
        }
    }

    global.gLogger.info("Looking doi in datacollections. doi=" + doi);
    doGet(
        icatQuery.getDataColletionIdListByDOI(sessionId, doi),
        successDataCollectionSearch,
        onError);
};

/** Return all datasets by DOI, it means there are in a collection or public investigation */

exports.getInvestigationByDOI = (authentication, doi, onSucess, onError) => {

    function onSessionSuccess(sessionId) {
        doGet(
            icatQuery.getInvestigationByDOI(sessionId, doi),
            onSucess,
            onError);
    }
    /** It gets a new session for the minter user */
    this.getSession(authentication, onSessionSuccess, onError);
};



/** Return all datasets by investigationId */
exports.getDatasetsById = (sessionId, datasetIds, onSucess, onError) => {
    function successParsing(response) {
        try{                        
            onSucess(datasetParser.parse(response));
        }
        catch(e){
            onError(e);
        }
    }
    if (datasetIds) {
        if (datasetIds.length > 0) {  
            global.gLogger.info(icatQuery.getDatasetsByIdList(sessionId, datasetIds));      
            doGet(                
                icatQuery.getDatasetsByIdList(sessionId, datasetIds),
                successParsing,
                onError);
        }
        else {
            onSucess([]);
        }
    }
};

/** Return all DOI's related to a dataset */
exports.getDataColletionByDatasetId = (sessionId, datasetId, onSucess, onError) => {
    function parse(data) {
        onSucess(datacollectionParser.parse(data));
    }
    doGet(
        icatQuery.getDataColletionByDatasetId(sessionId, datasetId),
        onSucess,
        onError);
};


/** Return all DOI's related to a dataset */
exports.getDataColletionsBySessionId = (sessionId, onSucess, onError) => {
    function parse(data) {
        onSucess(datacollectionParser.parse(data));
    }

    global.gLogger.debug(icatQuery.getDataColletionsBySessionId(sessionId), {method : "getDataColletionsBySessionId"});
    doGet(
        icatQuery.getDataColletionsBySessionId(sessionId),
        parse,
        onError);
};


exports.getInvestigations = (sessionId, onSuccess, onError) => {
    doGet(
        icatQuery.getInvestigations(sessionId),
        onSuccess,
        onError
    );
};

/**
 * Gets the investigation that belongs to a user, it means a user is a participant in the investigation
 * @param {string} sessionId
 * @return {array} Array with the investigations
 * @example
 *  [{Investigation: 
   { id: 123398449,
     createId: 'root',
     createTime: '2018-11-06T14:34:16.386+01:00',
     modId: 'root',
     modTime: '2018-11-06T14:34:16.386+01:00',
     datasets: [],
     investigationGroups: [],
     investigationInstruments: [],
     investigationUsers: [],
     keywords: [],
     name: 'ID212020',
     parameters: [],
     publications: [],
     samples: [],
     shifts: [],
     studyInvestigations: [],
     summary: 'DCM',
     title: 'DCM',
     visitId: 'id21' } }]
 * 
 */
exports.getInvestigationsBySessionId = (sessionId, onSucess, onError) => {    
    var parse = (data) => {
        onSucess(investigationParser.parse(data));
    };

    var username = cache.getUsernameBySessionId(sessionId);

    /** It might happen that username is not loaded in the cache */
    if (username == null) {
        var onSessionSuccess = (data) => {
            doGet(
                icatQuery.getInvestigationsBySessionIdAndUsername(sessionId, data.userName),
                parse,
                onError);

        };

        var onSessionError = (error) => {
            global.gLogger.error("Error retrieving session.", { sessionId: sessionId, error: error });
        };

        this.getSessionInformation(sessionId, onSessionSuccess, onSessionError);
    }
    else {
        global.gLogger.info("getInvestigationsBySessionId", { "query" : icatQuery.getInvestigationsBySessionIdAndUsername(sessionId, username)})
        doGet(
            icatQuery.getInvestigationsBySessionIdAndUsername(sessionId, username),
            parse,
            onError);
    }
};

/** 
 *  @DEPRECATED
 * Return all investigations by sessionId */
exports.getAllInvestigationsBySessionId = (sessionId, onSucess, onError) => {

    var parse = (data) => {
        global.gLogger.debug("getAllInvestigationsBySessionId done", { sessionId: sessionId, count: data.length });
        onSucess(investigationParser.parse(data));
    };
    var onRequestError = (error) => {
        onError(error);
    }
    global.gLogger.debug("getAllInvestigationsBySessionId started", { sessionId: sessionId, query: icatQuery.getAllInvestigationsBySessionId(sessionId) });
    doGet(
        icatQuery.getAllInvestigationsBySessionId(sessionId),
        parse,
        onRequestError);
};


/** Return all investigations under embargo by sessionId */
exports.getEmbargoedInvestigationsBySessionId = (sessionId, onSucess, onError) => {

    var parse = (data) => {
        global.gLogger.debug("getEmbargoedInvestigationsBySessionId done", { sessionId: sessionId, count: data.length });
        onSucess(investigationParser.parse(data));
    };
    var onRequestError = (error) => {
        onError(error);
    }
    global.gLogger.debug("getEmbargoedInvestigationsBySessionId started", { sessionId: sessionId, query: icatQuery.getEmbargoedInvestigationsBySessionId(sessionId) });
    doGet(
        icatQuery.getEmbargoedInvestigationsBySessionId(sessionId),
        parse,
        onRequestError);
};



/** Return all investigations under embargo by sessionId */
exports.getInvestigationByInstrumentScientist = (sessionId, onSucess, onError) => {
    var username = cache.getUsernameBySessionId(sessionId);
    var parse = (data) => {
        global.gLogger.debug("getInvestigationByInvestigationsScientist done", { sessionId: sessionId, count: data.length });
        onSucess(investigationParser.parse(data));
    };
    var onRequestError = (error) => {
        onError(error);
    }
    global.gLogger.debug("getInvestigationByInvestigationsScientist started", { sessionId: sessionId, query: icatQuery.getInvestigationByInstrumentScientist(sessionId) });
    doGet(
        icatQuery.getInvestigationByInstrumentScientist(sessionId, username),
        parse,
        onRequestError);
};

/** Return all investigations under embargo by sessionId */
exports.getReleasedInvestigationsBySessionId = (sessionId, onSucess, onError) => {

    var parse = (data) => {
        global.gLogger.debug("getReleasedInvestigationsBySessionId done", { sessionId: sessionId, count: data.length });
        onSucess(investigationParser.parse(data));
    };
    var onRequestError = (error) => {
        onError(error);
    }
    global.gLogger.debug("getReleasedInvestigationsBySessionId started", { sessionId: sessionId, query: icatQuery.getReleasedInvestigationsBySessionId(sessionId) });
    doGet(
        icatQuery.getReleasedInvestigationsBySessionId(sessionId),
        parse,
        onRequestError);
};




function doGet(url, onSucess, onError) {
    axios.get(url, { proxy })
        .then(response => {
            onSucess(response.data);
        })
        .catch(error => {
            try {
                onError(error);
            }
            catch (e) {
                global.gLogger.error(error);
            }
        });
}

/** Return an investigation by investigationId */
exports.getInvestigationById = (sessionId, investigationId, onSucess, onError) => {
    doGet(
        icatQuery.getInvestigationById(sessionId, investigationId),
        onSucess,
        onError);
};

/** Return an investigation by investigation name */
exports.getInvestigationByName = (sessionId, investigationName, onSucess, onError) => {
    doGet(
        icatQuery.getInvestigationByName(sessionId, investigationName),
        onSucess,
        onError);
};



/** Return all datasets by investigationId */
exports.getParameterTypeList = (authentication, onSucess, onError) => {
    function onSessionSuccess(sessionId) {
        global.gLogger.info("Getting parameterTypeList.", { username: authentication.credentials[0].username, sessionId: sessionId });
        doGet(
            icatQuery.getParameterTypeList(sessionId),
            onSucess,
            onError);
    }
    /** It gets a new session for the minter user */
    this.getSession(authentication, onSessionSuccess, onError);
};


/** Return all datasets by investigationId */
exports.getInvestigationTypes = (authentication, onSucess, onError) => {
    function onSessionSuccess(sessionId) {
        global.gLogger.info("Getting getInvestigationTypes.", { username: authentication.credentials[0].username, sessionId: sessionId });
        doGet(
            icatQuery.getInvestigationTypes(sessionId),
            onSucess,
            onError);
    }
    /** It gets a new session for the minter user */
    this.getSession(authentication, onSessionSuccess, onError);
};

/** Return all users by group name */
exports.getUsersByGroupName = (authentication, groupName, onSucess, onError) => {
    function onSessionSuccess(sessionId) {
        global.gLogger.info("Getting getUsersByGroupName.", { username: authentication.credentials[0].username, sessionId: sessionId });
        doGet(
            icatQuery.getUsersByGroupName(sessionId, groupName),
            onSucess,
            onError);
    }
    this.getSession(authentication, onSessionSuccess, onError);
};



/** Returns all users by group name */
exports.getUsers = (authentication, onSucess, onError) => {
    function onSessionSuccess(sessionId) {
        global.gLogger.info("Getting getUsers.", { username: authentication.credentials[0].username, sessionId: sessionId });
        doGet(
            icatQuery.getUsers(sessionId),
            onSucess,
            onError);
    }
    this.getSession(authentication, onSessionSuccess, onError);
};


/**
 * Get a list of data files by dataset id
 * @param {string} sessionId
 * @param {string} datasetIds A comma separated list of dataset ids. Example: "2342,1323,234234"
 */
exports.getDatafilesByDatasetId = (sessionId, datasetIds, onSucess, onError) => {
    global.gLogger.info("getDatafilesByDatasetId", { sessionId: sessionId, datasetIds: datasetIds });
    doGet(icatQuery.getDatafilesByDatasetId(sessionId, datasetIds),
        onSucess,
        onError);


};

/**
 * Return the archive status of the data files specified by the  datasetIds  along with a sessionId. 
 * @param {string} sessionId A sessionId returned by a call to the icat server. If the sessionId is omitted or null the ids reader account will be used which has read access to all data
 * @param {string} datasetIds A comma separated list of dataset ids. Example: "2342,1323,234234"
 */
exports.getStatusByDatasetIds = (sessionId, datasetIds, onSucess, onError) => {
    global.gLogger.info("getStatus", { sessionId: sessionId, datasetIds: datasetIds });
    doGet(icatQuery.getStatusByDatasetIds(sessionId, datasetIds),
        onSucess,
        onError);
};

/**
 * Return the archive status of the data files specified by the investigationIds along with a sessionId. 
 * @param {string} sessionId A sessionId returned by a call to the icat server. If the sessionId is omitted or null the ids reader account will be used which has read access to all data
 * @param {string} investigationIds A comma separated list of investigation ids. Example: "2342,1323,234234"
 */
exports.getStatusByIvestigationIds = (sessionId, investigationIds, onSucess, onError) => {
    global.gLogger.info("getStatus", { sessionId: sessionId, datasetIds: datasetIds });
    doGet(icatQuery.getStatusByIvestigationIds(sessionId, investigationIds),
        onSucess,
        onError);
};

/**
 * Get whether a given user is a permanent user for a given instrument (=beamline).
 * When this is the case, a single entity is returned. Not all permanent users for the given instrument are returned. 
 * When this is not the case, return an empty array
 * @param {string} sessionId session identifier identifying the user.
 * @param {string} instrumentName instrument name identifying the instrument (=beamline)
 * @param {} onSuccess callback function called when the http request succeeded
 * @param {} onError callback function called when the http request failed
 */
exports.getIsAInstrumentScientistForInstrumentName = (sessionId, instrumentName, onSucess, onError) => {
    global.gLogger.info("getPermanentUserByInstrumentName (filtered for the user)", { sessionId: sessionId, instrumentName: instrumentName });
    doGet(icatQuery.getIsAInstrumentScientistForInstrumentName(sessionId, instrumentName),
        onSucess,
        onError);
};