module.exports = (app) => {
    
  const controller = require('../controllers/datacollections.controller.js');
  const auth = require('../authentication/icat.js');


  app.get('/datacollections', auth.validateSession, controller.getDataColletionsBySessionId); 
}


