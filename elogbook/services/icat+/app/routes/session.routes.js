module.exports = (app) => {
    const session = require('../controllers/session.controller.js');

    /**
     * @swagger
     * /session/{sessionId}:
     *   get:
     *     summary: Gets information about the session in ICAT
     *     parameters:
     *       - in: path
     *         description : string with the ICAT's sessionId
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true
     *     responses:
     *       '200':
     *         $ref: '#/components/responses/sessionInformation'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Session
     */
    app.get('/session/:sessionId', session.getSessionInformation);

    // the following API method is not used
    // app.get('/session/:sessionId/investigationusers', session.getInvestigationUserByInvestigationId);

    /**
     * @swagger
     * /session:
     *   post:
     *     summary: This is used to log into the app
     *     requestBody:
     *       description: The credentials to log in
     *       content:
     *         'application/json':
     *           schema:
     *             $ref: '#/components/schemas/credentials'
     *       required: true
     *     responses:
     *       '200':
     *         description: Information about the newly created session
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/session' 
     *       '403':
     *         $ref: '#/components/responses/error403'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Session
     */
    app.post('/session', session.login);

} 
