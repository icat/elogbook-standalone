
module.exports = (app) => {
      
    const cache = require('../controllers/cache.controller.js'); 
     const auth = require('../authentication/icat.js');


     /**
     * @swagger
     * /cache:
     *   get: 
     *     summary: Gets the cache of a user given a specific sessionId 
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             type: array
     *             schema:
     *               $ref: '#/components/schemas/cacheUserStorage'
     *     tags:
     *       - Cache
     */
    app.get('/cache', cache.getSessionById);

     /**
     * @swagger
     * /cache/stats:
     *   get: 
     *     description: Gets the statistics for the use of cache
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             type: array
     *             schema:
     *               $ref: '#/components/schemas/cacheStats'
     *     tags:
     *       - Cache
     */
    app.get('/cache/stats', cache.getStats);

     /**
     * @swagger
     * /cache/keys:
     *   get: 
     *     description: Gets the keys stored in the cache
     *     responses:
     *       '200':
     *         description: 'Ok'
     *     tags:
     *       - Cache
     */
    app.get('/cache/keys', auth.allowAdministrators, cache.getCacheKeys);

     /**
     * @swagger
     * /cache/keys/:key:
     *   get: 
     *     description: Gets the content of a key
     *     responses:
     *       '200':
     *         description: 'Ok'
     *     tags:
     *       - Cache
     */
    app.get('/cache/keys/:key', auth.allowAdministrators, cache.getCacheKey);
    
 
}