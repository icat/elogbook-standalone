const investigations = require('../controllers/investigations.controller.js');
const auth = require('../authentication/icat.js');

module.exports = (app) => {
   app.get('/investigations/:investigationId/investigationusers', auth.requiresLogin, investigations.getInvestigationUserByInvestigationId);

   app.get('/investigations/:investigationId', auth.requiresLogin, investigations.getInvestigationById);  

   app.get('/investigations', auth.requiresSession, investigations.getInvestigationsBySessionId);  

   app.get('/investigations/:investigationName/normalize', investigations.normalize);     
}


