module.exports = (app) => {

    const auth = require('../authentication/icat.js');
    const logbook = require('../controllers/logbook.controller.js');


    /**
     * @swagger
     * /logbook/{sessionId}/investigation/id/{investigationId}/event/createfrombase64:
     *   post:
     *     summary: Create an event given a base64 string
     *     parameters:
     *       - $ref: '#/components/parameters/investigationId'
     *       - $ref: '#/components/parameters/sessionId'
     *     requestBody:
     *       $ref: '#/components/requestBodies/eventToCreate'
     *     responses:
     *       '200':
     *         $ref: '#/components/responses/eventCreated'
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '401':
     *         $ref: '#/components/responses/error401' 
     *       '403':
     *         $ref: '#/components/responses/error403'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Logbook
     */
    app.post('/logbook/:sessionId/investigation/id/:investigationId/event/createfrombase64', auth.requiresLogin, auth.allowsInvestigationUserOrAdministrators, logbook.createfrombase64);

    /**
     * @swagger
     * /logbook/{sessionId}/investigation/id/{investigationId}/event/create:
     *   post:
     *     summary: Create an event
     *     parameters:
     *       - $ref: '#/components/parameters/investigationId'
     *       - $ref: '#/components/parameters/sessionId'
     *     requestBody:
     *       $ref: '#/components/requestBodies/eventToCreate'
     *     responses:
     *       '200':
     *         $ref: '#/components/responses/eventCreated'
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '401':
     *         $ref: '#/components/responses/error401' 
     *       '403':
     *         $ref: '#/components/responses/error403'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Logbook
     */
    app.post('/logbook/:sessionId/investigation/id/:investigationId/event/create', auth.allowsInvestigationUserOrAdministrators, logbook.createEvent);

    /**
     * @swagger
     * /logbook/{sessionId}/investigation/id/{investigationId}/event/update:
     *   put:
     *     summary: Updates an event
     *     parameters:
     *       - $ref: '#/components/parameters/investigationId'
     *       - $ref: '#/components/parameters/sessionId'
     *     requestBody:
     *       $ref: '#/components/requestBodies/eventToUpdate'
     *     responses:
     *       '200':
     *         $ref: '#/components/responses/eventCreated'
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '401':
     *         $ref: '#/components/responses/error401' 
     *       '403':
     *         $ref: '#/components/responses/error403'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Logbook
     */
    app.put('/logbook/:sessionId/investigation/id/:investigationId/event/update', auth.allowsInvestigationUserOrAdministrators, logbook.modifyEvent);

    /**  @deprecated **/
    app.get('/investigations/:investigationId/events', auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, logbook.findAllEventsByInvestigationId);

    /** @deprecated creates a PDF **/
    app.get('/investigations/:investigationId/events/pdf', auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, logbook.generatePDFByInvestigation);

    /**
     * @swagger
     * /logbook/{sessionId}/investigation/name/{investigationName}/instrument/name/{instrumentName}/event:
     *   post:
     *     summary: Method used to create a notification type of event when investigationId is not accessible (from metadata manager). The investigation is identified by investigationName + instrumentName
     *     parameters:
     *       - $ref: '#/components/parameters/investigationName'
     *       - $ref: '#/components/parameters/instrumentName'
     *       - $ref: '#/components/parameters/sessionId'
     *     requestBody:
     *       $ref: '#/components/requestBodies/eventToCreate'
     *     responses:
     *       '200':
     *         $ref: '#/components/responses/eventCreated'
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '401':
     *         $ref: '#/components/responses/error401' 
     *       '403':
     *         $ref: '#/components/responses/error403'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Logbook
     */
    app.post('/logbook/:sessionId/investigation/name/:investigationName/instrument/name/:instrumentName/event', auth.requiresMagicToken, logbook.createEventWithInvestigationNameAndInstrumentName);

    /**
     * @swagger
     * /logbook/{sessionId}/investigation/id/{investigationId}/event/query:
     *   post:
     *     summary: Method used to retreive logbook events for given search parameters
     *     parameters:
     *       - $ref: '#/components/parameters/investigationId'
     *       - $ref: '#/components/parameters/sessionId'
     *     requestBody:
     *       $ref: '#/components/requestBodies/findEvents'
     *     responses:
     *       '200':
     *         $ref: '#/components/responses/events'
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '401':
     *         $ref: '#/components/responses/error401' 
     *       '403':
     *         $ref: '#/components/responses/error403'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Logbook
     */
    app.post('/logbook/:sessionId/investigation/id/:investigationId/event/query', auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, logbook.findEvent);

    /**
    * @swagger
    * /logbook/{sessionId}/investigation/id/{investigationId}/event/count:
    *   post:
    *     summary: Method used to retreive the number of logbook events for given search parameters
    *     parameters:
    *       - $ref: '#/components/parameters/investigationId'
    *       - $ref: '#/components/parameters/sessionId'
    *     requestBody:
    *       $ref: '#/components/requestBodies/findEvents'
    *     responses:
    *       '200':
    *         description: Number of events found for the given search parameters
    *         content:
    *           'text/plain':
    *             schema:
    *               type: integer
    *       '400':
    *         $ref: '#/components/responses/error400'
    *       '401':
    *         $ref: '#/components/responses/error401' 
    *       '403':
    *         $ref: '#/components/responses/error403'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Logbook
    */
    app.post('/logbook/:sessionId/investigation/id/:investigationId/event/count', auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, logbook.countFind);

    /**
    * @swagger
    * /logbook/{sessionId}/investigation/id/{investigationId}/event/pdf:
    *   get:
    *     summary: Method used to retreive logbook events for given search parameters under the form of a PDF.
    *     parameters:
    *       - $ref: '#/components/parameters/investigationId'
    *       - $ref: '#/components/parameters/sessionId'
    *       - in: query
    *         description: selection filter in mongoDB context used to find events (url encoded)
    *         name: find
    *         schema: 
    *           type: object
    *           example: "%7B%22find%22:%7B%22$and%22:%5B%7B%22$or%22:%5B%7B%22type%22:%22annotation%22%7D,%7B%22type%22:%22notification%22%7D%5D%7D%5D%7D,%22sort%22:%7B%22creationDate%22:-1%7D,%22skip%22:0,%22limit%22:6%7D"    
    *       - in: query
    *         description: selection filter in mongoDB context used to sort events
    *         name: sort
    *         schema: 
    *           type: string
    *           example: "%7B%22find%22:%7B%22$and%22:%5B%7B%22$or%22:%5B%7B%22type%22:%22annotation%22%7D,%7B%22type%22:%22notification%22%7D%5D%7D%5D%7D,%22sort%22:%7B%22creationDate%22:-1%7D,%22skip%22:0,%22limit%22:6%7D" 
    *       - in: query
    *         description: skips a number of events from the mongo search result
    *         name: skip
    *         schema: 
    *           type: string
    *           example: 0
    *       - in: query
    *         description: limit the maximum number of events from the mongo search result
    *         name: limit
    *         schema: 
    *           type: string
    *           example: 200
    *     responses:
    *       '200':
    *         description: PDF sent successfully
    *         content:
    *           'application/pdf':
    *              schema:
    *                type: string
    *                format: binary
    *       '400':
    *         $ref: '#/components/responses/error400'
    *       '401':
    *         $ref: '#/components/responses/error401' 
    *       '403':
    *         $ref: '#/components/responses/error403'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Logbook
    */
    app.get('/logbook/:sessionId/investigation/id/:investigationId/event/pdf', auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, logbook.generatePDFByQuery);

    /**
    * @swagger
    * /logbook/{sessionId}/investigation/id/{investigationId}/tag:
    *   get:
    *     summary: GET all tags available, (ie used in the investigation or not) for a given logbook including global, beamline specific and investigation specific tags. Tags available but not used are also retrieved.
    *     parameters:
    *       - $ref: '#/components/parameters/investigationId'
    *       - $ref: '#/components/parameters/sessionId'
    *     responses:
    *       '200':
    *         $ref: '#/components/responses/tags'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *       '401':
    *         $ref: '#/components/responses/error401' 
    *       '403':
    *         $ref: '#/components/responses/error403'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Logbook
    */
    app.get('/logbook/:sessionId/investigation/id/:investigationId/tag', auth.allowsInvestigationAfterEmbargoOrAllowsInvestigationUser, logbook.getTags);

    /**
    * @swagger
    * /logbook/{sessionId}/investigation/id/{investigationId}/tag/create:
    *   post:
    *     summary: Create an investigation tag for a given investigationId
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *       - $ref: '#/components/parameters/investigationId'
    *     requestBody:
    *       $ref: '#/components/requestBodies/createTag'
    *     responses:
    *       '200':
    *         $ref: '#/components/responses/tagCreated'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *       '401':
    *         $ref: '#/components/responses/error401' 
    *       '403':
    *         $ref: '#/components/responses/error403'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Logbook
    */
    app.post('/logbook/:sessionId/investigation/id/:investigationId/tag/create', auth.allowsInvestigationUserOrAdministrators, logbook.createInvestigationTag);

    /**
    * @swagger
    * /logbook/{sessionId}/instrument/name/{instrumentName}/tag/create:
    *   post:
    *     summary: Create a beamline tag for a given instrumentName (=beamline)
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *       - $ref: '#/components/parameters/instrumentName'
    *     responses:
    *       '200':
    *         $ref: '#/components/responses/tagCreated'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *       '401':
    *         $ref: '#/components/responses/error401' 
    *       '403':
    *         $ref: '#/components/responses/error403'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Logbook
    */
    app.post('/logbook/:sessionId/instrument/name/:instrumentName/tag/create', auth.allowsBeamlineResponsibleOrAdministrators, logbook.createBeamlineTag);

    /**
    * @swagger
    * /logbook/{sessionId}/investigation/id/{investigationId}/tag/id/{tagId}/tag/update:
    *   put:
    *     summary: Update an investigation tag
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *       - $ref: '#/components/parameters/investigationId'
    *       - $ref: '#/components/parameters/tagId'
    *     requestBody:
    *       $ref: '#/components/requestBodies/updateTag'
    *     responses:
    *       '200':
    *         $ref: '#/components/responses/tagUpdated'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *       '401':
    *         $ref: '#/components/responses/error401' 
    *       '403':
    *         $ref: '#/components/responses/error403'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Logbook
    */
    app.put('/logbook/:sessionId/investigation/id/:investigationId/tag/id/:tagId/tag/update', auth.allowsInvestigationUserOrAdministrators, logbook.updateInvestigationTag);

}