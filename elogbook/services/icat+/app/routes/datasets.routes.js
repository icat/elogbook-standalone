module.exports = (app) => {
    
   const datasetController = require('../controllers/datasets.controller.js');
   const auth = require('../authentication/icat.js');

  app.get('/investigations/:investigationId/datasets', auth.requiresLogin, datasetController.getDatasetsByInvestigationId);

  app.get('/datasets/:datasetIds', auth.validateSession, datasetController.getDatasetsById); 

  app.get('/datasets/:datasetId/datacollection', auth.validateSession, datasetController.getDataColletionByDatasetId); 
}


