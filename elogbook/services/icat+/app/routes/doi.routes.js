module.exports = (app) => {

    const doi = require('../controllers/doi.controller.js');
    const datasetsController = require('../controllers/datasets.controller.js');
    const auth = require('../authentication/icat.js');

    /**
    * @swagger
    * /doi/{sessionId}/mint:
    *   post:
    *     summary: Mint a DOI
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *     requestBody:
    *       $ref: '#/components/requestBodies/doiMinting'
    *     responses:
    *       '200':
    *         $ref: '#/components/responses/doiMinted'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *       '401':
    *         $ref: '#/components/responses/error401' 
    *       '403':
    *         $ref: '#/components/responses/error403'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - DOI
    */
    app.post('/doi/:sessionId/mint', auth.validateSession, doi.mint);

    app.get('/doi/:prefix/:suffix/datasets', auth.validateSession, datasetsController.getDatasetsByDOI);


    /**
     * @swagger
     * /doi/{prefix}/{suffix}/json-datacite:
     *   get:
     *     summary: Returns a document in a json-datacite format with the description of the DOI
     *     parameters:
     *       - in: path
     *         description : prefix of the doi
     *         name: prefix
     *         schema : 
     *           type : string
     *         example : 10.15151
     *         required: true
     *       - in: path
     *         description : suffix of the doi
     *         name: suffix
     *         schema : 
     *           type : string
     *         example : ESRF-ES-142846529
     *         required: true
     *     responses:
     *       '200':
     *         $ref: '#/components/responses/json-datacite'
     *       '400':
     *         description: 'No description'
     *       '500':
     *         description: 'No description'
     *     tags:
     *       - DOI
     */
    app.get('/doi/:prefix/:suffix/json-datacite', doi.getJSONDataciteByDOI);
}