
module.exports = (app) => {
      
    const controller = require('../controllers/elasticsearch.controller.js'); 
    const auth = require('../authentication/icat.js');


     /**
     * @swagger
     * /elasticsearch/{sessionId}/datasets/_msearch:
     *   get: 
     *     summary: Proxy to elastic search server 
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             type: array
     *             schema:
     *               $ref: '#/components/schemas/cacheUserStorage'
     *     tags:
     *       - Elastic Search
     */
    app.post('/elasticsearch/:sessionId/datasets/_msearch', controller.query);

}