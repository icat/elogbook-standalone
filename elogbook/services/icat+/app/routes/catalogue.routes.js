const investigationController = require('../controllers/investigations.controller.js');
const datasetController = require('../controllers/datasets.controller.js');
const dataCollectionController = require('../controllers/datacollections.controller.js');
const doiController = require('../controllers/doi.controller.js');
const datasfileController = require('../controllers/datafile.controller.js');
const auth = require('../authentication/icat.js');

module.exports = (app) => {
    /**
     * @swagger
     * /catalogue/{sessionId}/investigation/id/{investigationId}/investigationusers:
     *   get: 
     *     summary: Gets users involved in a investigation
     *     parameters:
     *       - in: path
     *         description : A valid session identifier
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true  
     *       - in: path
     *         description : Metadata Catalogue investigation identifier
     *         name: investigationId
     *         schema : 
     *           type : integer     
     *         required: true         
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/investigationusers'     
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '401':
     *         $ref: '#/components/responses/error401'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/:sessionId/investigation/id/:investigationId/investigationusers', auth.requiresSession, investigationController.getInvestigationUserByInvestigationId);

    /**
     * @swagger
     * /catalogue/{sessionId}/investigation:
     *   get:
     *     summary: Gets all investigations
     *     parameters:
     *       - in: query
     *         description : string with the ICAT's sessionId
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true    
     *       - in: query   
     *         name: opendata 
     *         schema:
     *           type : boolean  
     *         example : true                     
     *         description : If opendata then it will return all investigations which I have reading permission even if I am not a participant otherwise it will return only my investigations
     *       - in: query   
     *         name: useris 
     *         description : It will return the investigation attached to the user as participant or instrument scientists (beamline responsible)
     *         schema:
     *           type : string 
     *           enum: [participant, instrumentscientist]
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/investigation' 
     *       '400':
     *         $ref: '#/components/responses/error400'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/:sessionId/investigation', auth.requiresSession, investigationController.getInvestigationsBySessionId);


     /**
     * @swagger
     * /catalogue/{sessionId}/investigation/useris/instrumentscientist/investigation:
     *   get:
     *     summary: Gets all investigations where I am instrument scientist
     *     parameters:
     *       - in: query
     *         description : string with the ICAT's sessionId
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true       
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/investigation' 
     *       '400':
     *         $ref: '#/components/responses/error400'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/:sessionId/investigation/useris/instrumentscientist/investigation', auth.requiresSession, investigationController.getInvestigationByInstrumentScientist);
    


    /**
     * @swagger
     * /catalogue/{sessionId}/investigation/id/{investigationId}/investigation:
     *   get:
     *     summary: Get investigation by identifier
     *     parameters:
     *       - in: path
     *         description : A valid session identifier
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true  
     *       - in: path
     *         description : Investigation's identifier 
     *         name: investigationId
     *         schema : 
     *           type : integer        
     *         required: true    
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/investigation'  
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/:sessionId/investigation/id/:investigationId/investigation', auth.requiresLogin, investigationController.getInvestigationById);


     /**
     * @swagger
     * /catalogue/{sessionId}/investigation/status/embargoed/investigation:
     *   get:
     *     summary: Gets all investigations that are under embargo. releaseDate > NOW
     *     parameters:
     *       - in: query
     *         description : string with the ICAT's sessionId
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true    
     *       - in: query   
     *         name: opendata 
     *         schema:
     *           type : string  
     *         example : true                     
     *         description : If opendata then it will return all investigations which I have reading permission even if I am not a participant otherwise it will return only my investigations
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/investigation' 
     *       '400':
     *         $ref: '#/components/responses/error400'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/:sessionId/investigation/status/embargoed/investigation', auth.requiresSession, investigationController.getEmbargoedInvestigations);

         /**
     * @swagger
     * /catalogue/{sessionId}/investigation/status/released/investigation:
     *   get:
     *     summary: Gets all investigations which content is public
     *     parameters:
     *       - in: query
     *         description : string with the ICAT's sessionId
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true    
     *       - in: query   
     *         name: opendata 
     *         schema:
     *           type : string  
     *         example : true                     
     *         description : If opendata then it will return all investigations which I have reading permission even if I am not a participant otherwise it will return only my investigations
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/investigation'
     *       '400':
     *         $ref: '#/components/responses/error400'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/:sessionId/investigation/status/released/investigation', auth.requiresSession, investigationController.getReleasedInvestigations);

   /**
     * @swagger
     * /catalogue/{sessionId}/investigation/id/{investigationId}/investigation:
     *   get:
     *     summary: Get investigation by identifier
     *     parameters:
     *       - in: path
     *         description : A valid session identifier
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true  
     *       - in: path
     *         description : Investigation's identifier 
     *         name: investigationId
     *         schema : 
     *           type : integer        
     *         required: true    
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/investigation'  
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/:sessionId/investigation/id/:investigationId/investigation', auth.requiresLogin, investigationController.getInvestigationById);


    /**
     * @swagger
     * /catalogue/{sessionId}/datacollection:
     *   get:
     *     description: Gets all datacollection for a given sessionId
     *     parameters:
     *       - in: path
     *         description : string with the ICAT's sessionId
     *         name: sessionId
     *         schema : 
     *           type : string
     *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
     *         required: true
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/datacollection'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Datacollection
     */
    app.get('/catalogue/:sessionId/datacollection', auth.validateSession, dataCollectionController.getDataColletionsBySessionId);

    /**
     * @swagger
     * /catalogue/investigation/name/{investigationName}/normalize:
     *   get:
     *     description: Most of users don't write their proposal names using the same convention so it might happen that a given proposal can be written in many different ways. This method will homogenize such name.  Example IH-LS-0001, ihls0001, ih-LS-0001 are normalized like IH-LS-0001.  
     *     parameters:
     *       - in: path
     *         description : name of the investigation to normalize
     *         name: investigationName
     *         schema : 
     *           type : string
     *         example : ihls0001
     *         required: true    
     *     responses:
     *       '200':
     *         description : It returns a string with the normalized proposal name
     *         content:
     *           application/json:
     *             schema : 
     *               type : string
     *       '400':
     *         $ref: '#/components/responses/error400'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Investigations
     */
    app.get('/catalogue/investigation/name/:investigationName/normalize', investigationController.normalize);

    /**
     * @swagger
     * /catalogue/{sessionId}/investigation/id/{investigationId}/dataset:
     *   get:
     *     description: Gets datasets for a given investigationId
     *     parameters:
     *       - $ref: '#/components/parameters/sessionId'
     *       - $ref: '#/components/parameters/investigationId'
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               type: array
     *               items:
     *                 $ref: '#/components/schemas/dataset'
     *       '401':
     *         $ref: '#/components/responses/error401'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Catalogue
     */
    
    app.get('/catalogue/:sessionId/investigation/id/:investigationId/dataset',  datasetController.getDatasetsByInvestigationId);


     /**
     * @swagger
     * /catalogue/{sessionId}/investigation/id/{investigationId}/dataset:
     *   get:
     *     description: Gets all dataset documents for a given investigationId
     *     parameters:
     *       - $ref: '#/components/parameters/sessionId'
     *       - $ref: '#/components/parameters/investigationId'
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               type: array
     *               items:
     *                 $ref: '#/components/schemas/dataset_document'
     *       '401':
     *         $ref: '#/components/responses/error401'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Catalogue
     */
    app.get('/catalogue/:sessionId/investigation/id/:investigationId/dataset_document', auth.requiresLogin, datasetController.getDatasetDocumentsByInvestigationId);

     /**
     * @swagger
     * /catalogue/{sessionId}/dataset/id/{datasetIds}/dataset:
     *   get:
     *     description: Get a list of dataset from a datasetId list
     *     parameters:
     *       - $ref: '#/components/parameters/sessionId'
     *       - $ref: '#/components/parameters/datasetIds'
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               type: array
     *               items:
     *                 $ref: '#/components/schemas/dataset'
     *       '401':
     *         $ref: '#/components/responses/error401'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Catalogue
     */
    app.get('/catalogue/:sessionId/dataset/id/:datasetIds/dataset', auth.validateSession, datasetController.getDatasetsById);


     /**
     * @swagger
     * /catalogue/{sessionId}/dataset/id/{datasetIds}/dataset_document:
     *   get:
     *     description: Get a list of dataset documents from a datasetId list. A dataset document is used for indexing a dataset in elastic search. It unflatters dataset's parameters.
     *     parameters:
     *       - $ref: '#/components/parameters/sessionId'
     *       - $ref: '#/components/parameters/datasetIds'
     *     responses:
     *       '200':
     *         content:
     *           application/json:
     *             schema:
     *               type: array
     *               items:
     *                 $ref: '#/components/schemas/dataset_document'
     *       '401':
     *         $ref: '#/components/responses/error401'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Catalogue
     */
    app.get('/catalogue/:sessionId/dataset/id/:datasetIds/dataset_document', auth.validateSession, datasetController.getDatasetsDocumentsById);



    /**
    * @swagger
    * /catalogue/{sessionId}/dataset/id/{datasetId}/datacollection:
    *   get:
    *     description: Returns datacollections related to a given dataset along with a datasetId
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *       - $ref: '#/components/parameters/datasetId'
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               type: array
    *               items:
    *                 $ref: '#/components/schemas/datacollection'
    *       '401':
    *         $ref: '#/components/responses/error401'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Catalogue
    */
    app.get('/catalogue/:sessionId/dataset/id/:datasetId/datacollection', auth.validateSession, datasetController.getDataColletionByDatasetId);

    /**
    * @swagger
    * /catalogue/{sessionId}/dataset/id/{datasetIds}/status:
    *   get:
    *     description: Return the archive status of the data files specified by the datasetIds along with a sessionId.
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *       - $ref: '#/components/parameters/datasetId'
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               items:
    *                 $ref: '#/components/schemas/status'
    *       '401':
    *         $ref: '#/components/responses/error401'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Catalogue
    */
    app.get('/catalogue/:sessionId/dataset/id/:datasetIds/status', auth.validateSession, datasetController.getStatusByDatasetIds);



    /**
    * @swagger
    * /catalogue/{sessionId}/dataset/id/{datasetId}/datafile:
    *   get:
    *     description: Get datacollection for a given dataset id
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *       - $ref: '#/components/parameters/datasetId'
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               type: array
    *               items:
    *                 $ref: '#/components/schemas/datafile'
    *       '401':
    *         $ref: '#/components/responses/error401'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Catalogue
    */
    app.get('/catalogue/:sessionId/dataset/id/:datasetId/datafile', auth.validateSession, datasfileController.getDatafilesByDatasetId);



    /**
    * @swagger
    * /doi/{sessionId}/mint:
    *   post:
    *     summary: It mints a DOI
    *     parameters:
    *       - in: path
    *         description : string with the ICAT's sessionId
    *         name: sessionId
    *         schema : 
    *           type : string
    *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
    *         required: true
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/dataset'
    *       '400':
    *         description: 'No description'
    *       '500':
    *         description: 'No description'
    *     tags:
    *       - DOI
    */
    app.post('/doi/:sessionId/mint', auth.validateSession, doiController.mint);

    /**
    * @swagger
    * /doi/{prefix}/{suffix}/datasets:
    *   get:
    *     description: Returns a set of datasets given a DOI. 
    *     parameters:
    *       - in: path
    *         description : string with the ICAT's sessionId
    *         name: sessionId
    *         schema : 
    *           type : string
    *         example : 83825a51-69b8-4eg58-b5fb-bcf79bb412be
    *         required: true
    *       - in: path
    *         description : prefix of the doi
    *         name: prefix
    *         schema : 
    *           type : string
    *         example : 10.15151
    *         required: true
    *       - in: path
    *         description : suffix of the doi
    *         name: suffix
    *         schema : 
    *           type : string
    *         example : ESRF-ES-142846529
    *         required: true
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/dataset'
    *       '400':
    *         description: 'No description'
    *       '500':
    *         description: 'No description'
    *     tags:
    *       - DOI
    */
    app.get('/doi/:prefix/:suffix/datasets', auth.validateSession, datasetController.getDatasetsByDOI);


    /**
     * @swagger
     * /doi/{prefix}/{suffix}/json-datacite:
     *   get:
     *     description: Returns a document in a json-datacite format with the description of the DOI
     *     parameters:
     *       - in: path
     *         description : prefix of the doi
     *         name: prefix
     *         schema : 
     *           type : string
     *         example : 10.15151
     *         required: true
     *       - in: path
     *         description : suffix of the doi
     *         name: suffix
     *         schema : 
     *           type : string
     *         example : ESRF-ES-142846529
     *         required: true
     *     responses:
     *       '200':
     *         description: 'Return a json-datacite compliant format'
     *         schema:
     *           $ref: '#/components/schemas/json-datacite'
     *       '400':
     *         description: 'No description'
     *       '500':
     *         description: 'No description'
     *     tags:
     *       - DOI
     */
    app.get('/doi/:prefix/:suffix/json-datacite', doiController.getJSONDataciteByDOI);

}