module.exports = (app) => {
   const resource = require('../controllers/resource.controller.js');
   const auth = require('../authentication/icat.js');

     /**
     * @swagger
     * /resource/{sessionId}/file/investigation/id/{investigationId}/upload:
     *   post:
     *     summary: Uploads a file to ICAT+
     *     parameters:
     *       - $ref: '#/components/parameters/investigationId'
     *       - $ref: '#/components/parameters/sessionId'
     *     responses:
     *       '200':
     *         description: 'Ok'
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/event'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Resource
     */   
   app.post('/resource/:sessionId/file/investigation/id/:investigationId/upload',  resource.upload);

     /**
     * @swagger
     * /resource/{sessionId}/file/id/{fileId}/investigation/id/{investigationId}/download:
     *   get:
     *     summary: Downloads a resource
     *     parameters:
     *       - $ref: '#/components/parameters/investigationId'
     *       - $ref: '#/components/parameters/sessionId'
     *       - in: path
     *         description : File's identifier
     *         name: fileId
     *         schema :  
     *           type : string     
     *         required: true
     *     responses:
     *       '200':
     *         description : 'Ok'
     *       '500':
     *         $ref: '#/components/responses/error500'
     *     tags:
     *       - Resource
     */   
   app.get('/resource/:sessionId/file/id/:resourceId/investigation/id/:investigationId/download',  resource.download);
      
}


