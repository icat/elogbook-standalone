module.exports = (app) => {

    
    const investigations = require('../controllers/investigations.controller.js');
    const datafile = require('../controllers/datafile.controller.js');
    const datasets = require('../controllers/datasets.controller.js');
    const auth = require('../authentication/icat.js');
    

    /**
    * @swagger
    * /proposals:
    *   get:
    *     summary: Returns all proposals or investigations
    *     responses:           
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/investigationlight'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *     tags:
    *       - Panosc
    */
    app.get('/proposals', auth.doAnonymousLogin, investigations.getReleasedInvestigations);

    /**
    * @swagger
    * /proposals/{proposalId}:
    *   get:
    *     summary: Gets Specific proposal  
    *     parameters:
    *       - in: path
    *         description : Investigation's identifier 
    *         name: proposalId
    *         schema : 
    *           type : integer        
    *         required: true   
    *     responses:           
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/investigationlight'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *     tags:
    *       - Panosc
    */
   app.get('/proposals/:investigationId', auth.doAnonymousLogin, investigations.getInvestigationLightById);

    /**
    * @swagger
    * /proposals/{proposalId}/schedules:
    *   get:
    *     summary: Gets scheduless (session or visit) for a proposal  
    *     parameters:
    *       - in: path
    *         description : Proposal's identifier 
    *         name: proposalId
    *         schema : 
    *           type : integer        
    *         required: true   
    *     responses:           
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/investigationlight'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *     tags:
    *       - Panosc
    */
   app.get('/proposals/:investigationId/schedules', auth.doAnonymousLogin, investigations.getInvestigationLightById);

    /**
    * @swagger
    * /proposals/{proposalId}/schedules/{scheduleId}/datasets:
    *   get:
    *     summary: Gets datasets for a proposal  
    *     parameters:
    *       - in: path
    *         description : proposal's identifier 
    *         name: proposalsId
    *         schema : 
    *           type : integer        
    *         required: true   
    *       - in: path
    *         description : schedule's identifier 
    *         name: scheduleId
    *         schema : 
    *           type : integer        
    *         required: true   
    *     responses:           
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/dataset'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *     tags:
    *       - Panosc
    */
   app.get('/proposals/:investigationId/schedules/:scheduleId/datasets', auth.doAnonymousLogin, datasets.getDatasetsByInvestigationId);

    /**
    * @swagger
    * /proposals/{investigationId}/schedules/{scheduleId}/datasets/{datasetId}/files:
    *   get:
    *     summary: Gets a list of files associated to a dataset
    *     parameters:
    *       - in: path
    *         description : investigationId
    *         name: investigationId
    *         schema : 
    *           type : integer        
    *         required: true  
    *       - in: path
    *         description : scheduleId
    *         name: scheduleId
    *         schema : 
    *           type : integer        
    *         required: true 
    *       - in: path
    *         description : dataset's identifier 
    *         name: datasetId
    *         schema : 
    *           type : integer        
    *         required: true   
    *     responses:           
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/datafilelight'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *     tags:
    *       - Panosc
    */
    app.get('/proposals/:investigationId/schedules/:scheduleId/datasets/:datasetId/files', auth.doAnonymousLogin, datafile.getDatafilesByDatasetId);

    /**
    * REMOVED
    * /datasets/{datasetIds}:
    *   get:
    *     summary: Gets a dataset by id
    *     parameters:
    *       - in: path
    *         description : dataset's identifier 
    *         name: datasetIds
    *         schema : 
    *           type : integer        
    *         required: true   
    *     responses:           
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/dataset'
    *       '400':
    *         $ref: '#/components/responses/error400'
    *     tags:
    *       - Panosc
    */
   //app.get('/datasets/:datasetIds', auth.doAnonymousLogin, datasets.getDatasetsById);

    /**
    * @swagger
    * /datasets:    
    *   get:
    *     summary: Returns a set of datasets given a DOI. 
    *     parameters:
    *       - in: query
    *         description :  doi
    *         name: prefix
    *         schema : 
    *           type : string
    *         example : 10.15151
    *         required: true
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/dataset'
    *       '400':
    *         description: 'No description'
    *       '500':
    *         description: 'No description'
    *     tags:
    *       - Panosc
    */
   app.get('/datasets', auth.doAnonymousLogin, datasets.getDatasetsByDOI);


    /**
    * @swagger
    * /datasets/{datasetIds}/status:
    *   get:
    *     description: Return the archive status of the data files specified by the datasetIds along with a sessionId.
    *     parameters:
    *       - $ref: '#/components/parameters/sessionId'
    *       - $ref: '#/components/parameters/datasetId'
    *     responses:
    *       '200':
    *         content:
    *           application/json:
    *             schema:
    *               items:
    *                 $ref: '#/components/schemas/status'
    *       '401':
    *         $ref: '#/components/responses/error401'
    *       '500':
    *         $ref: '#/components/responses/error500'
    *     tags:
    *       - Panosc
    */
   app.get('/catalogue/:sessionId/dataset/id/:datasetIds/status', auth.doAnonymousLogin, datasets.getStatusByDatasetIds);





    /**
    * @swagger
    * /instruments:
    *   get:
    *     summary: Returns all instruments
    *     responses:           
     *       '400':
    *         $ref: '#/components/responses/error400'
    *     tags:
    *       - Panosc
    */
   app.get('/instruments', auth.doAnonymousLogin, investigations.getReleasedInvestigations);
}