module.exports = {        
       xml : `<?xml version="1.0" encoding="UTF-8"?>
<resource xsi:schemaLocation="http://datacite.org/schema/kernel-3 http://schema.datacite.org/meta/kernel-3/metadata.xsd" xmlns="http://datacite.org/schema/kernel-3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<identifier identifierType="DOI">%DOI%</identifier>
	<creators>
		%AUTHORS%
	</creators>
	<titles>
		<title>%TITLE%</title>
	</titles>
	<publisher>European Synchrotron Radiation Facility</publisher>
	<publicationYear>%PUBLICATION_YEAR%</publicationYear>
	<subjects>		
    	<subject subjectScheme="Proposal">%INVESTIGATION_NAME%</subject>
    	<subject subjectScheme="Instrument">%INSTRUMENT_NAME%</subject>
	</subjects>
	<language>en</language>
	<resourceType resourceTypeGeneral="Dataset">Datacollection</resourceType>
	<version>1</version>
	<descriptions>
		<description descriptionType="Abstract">%ABSTRACT%</description>
	</descriptions>
</resource>`
}
