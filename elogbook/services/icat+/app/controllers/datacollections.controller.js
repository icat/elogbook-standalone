const icat = require('../api/icat.js');




exports.getDataColletionsBySessionId = (req, res, next) => {   
   global.gLogger.info("getDataColletionsBySessionId", {sessionId : req.params.sessionId}); 
   function onSuccess(datasets){   
        res.send(datasets);
    }
    function onError(error){             
       next(error);
    }
    try{
        icat.getDataColletionsBySessionId(req.params.sessionId, onSuccess, onError);
    }
    catch(error){
        global.gLogger.error(error);
    }
};
