const cache = require('../cache/cache.js');
const axios = require('axios-https-proxy-fix');

/**
 * Based on your sessionId will return a should clause with the public datacollections and your investigations
 * Example:
 *  [{ term : {investigationId : "125245"}}]
 * @param {*} sessionId
 */
function getShouldFilter(sessionId){
    /** There is no restriction for administrators */
    if (cache.isAdministrator(sessionId)){
        return [];
    }

    /**
     * Returns ids of dataset involved in the datacollections
     */
    function getDatasetIdsByDataCollections(dataCollections){                
        var datasetIds = [];
        for (var i = 0; i < dataCollections.length; i++){
            for (var j = 0; j < dataCollections[i].dataCollectionDatasets.length; j++){
                datasetIds.push(dataCollections[i].dataCollectionDatasets[j].dataset.id);
            }
        }
        return datasetIds;
    }
    
    var allowedDatasetIdsForPublicDataCollections = getDatasetIdsByDataCollections(cache.getDataCollections(sessionId));    
    if (sessionId){
        /** My investigations */
        var investigationsId = cache.getInvestigationsId(sessionId);
        var shouldInvestigationsId = [];
        var shouldDataCollectionsId = [];
        if (investigationsId){
            if (investigationsId.length > 0){
                shouldInvestigationsId = investigationsId.map(function(i){ return { term : {investigationId : i}}});    
            }
        }
        if (allowedDatasetIdsForPublicDataCollections){
            if (allowedDatasetIdsForPublicDataCollections.length > 0){                
                shouldDataCollectionsId = allowedDatasetIdsForPublicDataCollections.map(function(i){ return { term : {id : i}}});                  
            }
        }
        if (shouldInvestigationsId.length == 0 && shouldDataCollectionsId.length == 0){
            return { term : {investigationId : "Not ACCESS"}};
        }
        return shouldInvestigationsId.concat(shouldDataCollectionsId);
    }
    return { term : {investigationId : "Not ACCESS"}};
}


/**
 * Bascically this function redo the query received from reactbase and then apply a security filter (should clause)
 */
function getResultQuery(sessionId, boolClause, size, from, aggs){
          
    if (boolClause){
        boolClause.should =  getShouldFilter(sessionId);
    }
    else{
        boolClause =  {
                should : getShouldFilter(sessionId)
        };
    }                          
    var query = {
            query : {                                   
                constant_score : {                                                                                                          
                        filter : {
                            bool : boolClause
                        }
                }
            },
            size:size,
            _source:{
                includes:["*"],excludes:[]
            },          
            aggs : aggs
    };

    /** Aggregators has no from clause */
    if (from){
        query.from = from;
    }
    
    return query;
};

exports.query = (req, res) => {     
    if (global.gConfig.elasticsearch.enabled){             
        var queryLines = "";         
        if (req.body.query){
            /** this is for passing the tests **/
            queryLines = req.body.query.split("\n");    
        }
        else{
            queryLines = req.body.split("\n"); 
        }
        if (queryLines) {
            /** clean query */
            var query = "";
            if (queryLines.length > 0) {
                /** For the result grid */ 
                for (var i = 0; i < queryLines.length; i++) {
                    global.gLogger.debug(queryLines[i]);
                }                                                   
                for (var i = 0; i < queryLines.length; i++) {
                    if (i % 2 == 1) {
                        var filterQuery = JSON.parse(queryLines[i]);
                        //global.gLogger.debug("=================");
                        //global.gLogger.debug(queryLines[i]);
                        /** This intercepts and transform the query adding the corresponding filter */
                        queryLines[i] = JSON.stringify(getResultQuery(req.params.sessionId, filterQuery.query.bool, filterQuery.size, filterQuery.from, filterQuery.aggs));
                        //global.gLogger.debug(queryLines[i]);
                        //global.gLogger.debug("******************");
                    }
                    query = query + "\n" + queryLines[i];
                }       
               
                do_mSearch(query, res);   
            }
        }
    }
    else{
         global.gLogger.debug("Elasticsearch is disabled");
         res.status(400).send({"message" : "Elasticsearch is disabled"});
    }
};

/**
 * This method runs a POST _msearch on elastic search with a given query
 */
function do_mSearch(query, res) {    
    global.gLogger.debug("do_mSearch query", { query: "[BEGIN]" + query + "[END]" });
    
    axios.post(global.gConfig.elasticsearch.server + "/datasets/_msearch",
        query,
        {
            headers: {
                'Content-Type': 'application/x-ndjson'
            }
        })
        .then(function (response) {
            global.gLogger.debug("do_mSearch query sends data");
            res.send(response.data);
        })
        .catch(function (error) {            
            global.gLogger.error(error);
            res.status(400).send(error.message);
        });
}
