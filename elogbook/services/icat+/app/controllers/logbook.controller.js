var stream = require('stream');
const _ = require('lodash');
var wkhtmltopdf = require('wkhtmltopdf');
const CONSTANTS = require('../constants');
const Event = require('../models/event.model.js');
const Tag = require('../models/tag.model.js');

var wkhtmltopdf = require('wkhtmltopdf');

const icat = require('../api/icat.js');
const investigationHelper = require('../helpers/investigation.js');
var moment = require('moment');
var replaceall = require("replaceall");
const nl2br = require('nl2br');
const fs = require('fs');
const mongoose = require('mongoose');
const gridfs = require('mongoose-gridfs');
const cache = require('../cache/cache.js');
/** PDF generation response timeout */
const TIMEOUT = 1000 * 60 * 10;


/**
 * Replace the sessionId in every properties of the events. For each event, this includes the event content of all formats plus all other properties of the event
 * @param {array} events the events 
 * @param {string} newSessionId the new sessionId
 * @returns {json} the new events with the new sessionId
 */
function replaceSessionId(events, newSessionId) {
    return JSON.parse((JSON.stringify(events).replace(new RegExp(/\/file\?sessionId=([\w-]*)/g), "/file?sessionId=" + newSessionId)));
}

/**
 * Rebuild the image src value with proper API and a valid sessionId for a set of events
 * @param {Array} events all events to be investigated
 * @param {String} sessionId new session identifier to be used in img src
 * @param {String} investigationId investigation identifier
 * @param {String} serverURI http protocol and servername to use example: https://icatplus.esrf.fr
 * @return {Array} updated events
 */
exports.replaceImageSrc = (events, sessionId, investigationId, serverURI) => {
    let stringifiedEvents = JSON.stringify(events);

    /** The following regular expression:
    * extracts eventual presence of image style or other img options in p1
    * extracts eventId which needs to be updated into p2
    * input: 
    * ....<img src="http://linfalcon.esrf.fr/investigations/{investigationId}/events/{eventId}/file?sessionId={sessionId}" alt="" ... style=.... />
    * OR ....<img style= ..... src="http://linfalcon.esrf.fr/investigations/{investigationId}/events/{eventId}/file?sessionId={sessionId}" alt="" ... />
    * Note: Extraction occurs everywhere the pattern is found in stringifiedEvents. Even when the serverURI is the same as what we expect, the replacement 
    * occurs because the whole API has changed and needs to be replaced completely.
    */
    let newStringifiedEvents = stringifiedEvents.replace(/<img([^>]*)src=\\"\S+\/investigations\/\d+\/events\/(\S+)\/file?\S+\\"/g,
        function (match, p1, p2) {
            if (match && p2) {
                return '<img' + p1 + 'src=\\"' + serverURI + '/resource/' + sessionId + '/file/id/' + p2 + '/investigation/id/' + investigationId + '/download\\"'
            }
        })

    // Replace img src using OLD API surounded by single quotes or \"
    newStringifiedEvents = newStringifiedEvents.replace(/<img([^>]*)src='?(?:\")?\S+\/file\/id\/(\S+)\/investigation\S+'?(?:\")?/g,
        function (match, p1, p2) {
            if (match && p2) {
                return '<img' + p1 + 'src=\\"' + serverURI + '/resource/' + sessionId + '/file/id/' + p2 + '/investigation/id/' + investigationId + '/download\\"'
            }
        })

    return JSON.parse(newStringifiedEvents);
}

/**
 * Returns the URL of a pre-uploaded file
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.params.investigationId investigation identifier
 * @param {string} fileId file identifier stored in mongo gridFS
**/
function getFileDownloadURL(sessionId, investigationId, fileId) {
    return global.gConfig.server.url + "/resource/" + sessionId + "/file/id/" + fileId + "/investigation/id/" + investigationId + "/download"
}
/**
 * This method creates a temporal file and will return the file path
 */
function getTemporalPathforBase64File(base64) {
    var filePath = "/tmp/" + moment() + "_camera.png";
    global.gLogger.info("Creating temporal file", { filePath: filePath });
    /** Writting temporal file */
    fs.writeFile(filePath, base64.replace(/^data:image\/png;base64,/, ""), 'base64', function (err) {
        console.log(err);
    });
    return filePath;
}
/**
 * This method will create an attachment and a annotation from a base64 encoded image
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.params.investigationId investigation identifier
 * @param {string} req.body.base64 base64 image png 
**/
exports.createfrombase64 = (req, res) => {
    global.gLogger.info("createfrombase64", { investigationId: req.params.investigationId || null });
    if (req.params.investigationId) {
        if (req.body.base64) {
            try {
                var { model: Attachment } = require('mongoose-gridfs')({
                    collection: 'fs'
                });
                /** Creating stream */
                var temporalFilePath = getTemporalPathforBase64File(req.body.base64);
                const readStream = fs.createReadStream(temporalFilePath);
                Attachment.write({ filename: temporalFilePath, contentType: 'application/octet-stream' }, readStream, (error, file) => {
                    const event = new Event({
                        investigationId: req.params.investigationId,
                        datasetId: req.body.datasetId || null,
                        type: CONSTANTS.EVENT_TYPE_ATTACHMENT,
                        category: CONSTANTS.EVENT_CATEGORY_FILE,
                        text: "",
                        filename: temporalFilePath,
                        username: cache.getUsernameBySessionId(req.params.sessionId),
                        software: req.body.software || null,
                        machine: req.body.machine || null,
                        creationDate: moment(),
                        file: file._id,
                        contentType: 'application/octet-stream'
                    });
                    event.save()
                        .then(data => {
                            /** File is written */
                            global.gLogger.debug("File is created in gridFS", { id: file._id });
                            global.gLogger.debug("Removing temporal file", { temporalFilePath: temporalFilePath });
                            fs.unlinkSync(temporalFilePath);
                            /** Creating annotation */
                            var event = new Event({
                                investigationId: req.params.investigationId,
                                creationDate: moment(),
                                type: CONSTANTS.EVENT_TYPE_ANNOTATION,
                                category: CONSTANTS.EVENT_CATEGORY_COMMENT,
                                username: cache.getUsernameBySessionId(req.params.sessionId),
                                previousVersionEvent: null,
                                content: this.translateEventContentToHtml([
                                    {
                                        format: "plainText",
                                        text: ""
                                    },
                                    {
                                        format: "html",
                                        text: "<p><img src='" + getFileDownloadURL(req.params.sessionId, req.params.investigationId, data._id) + "' /></p>"
                                    }
                                ])
                            });

                            event.save()
                                .then(data => {
                                    global.gLogger.debug("Annotation created", { event: data });
                                    res.send(data);
                                }).catch(err => {
                                    global.gLogger.error("An error occured during the creation of the event.", { error: err, investigationId: req.params.investigationId || null, body: req.body });
                                    res.status(500).send({
                                        message: err.message || "Some error occurred while creating the event"
                                    });
                                });
                        }).catch(err => {
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating the Attachment."
                            });
                        });
                });
            }
            catch (e) {
                res.status(500).send({
                    message: e.message || "Some error occurred while creating the Attachment."
                });
            }

        }
        else {
            global.gLogger.error("No base64 parameter", { base64: req.body.base64 });
            res.status(400).send({ message: "No base64 parameter" });
        }
    }
    else {
        global.gLogger.error("No investigation Id parameter");
        res.status(400).send({ message: "No investigation Id parameter" });
    }


};


/**
 * Creates an event in an investigation associated logbook
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.params.investigationId investigation identifier
 * @param {object} req.body event to be created in the logbook
**/
exports.createEvent = (req, res) => {
    global.gLogger.info("createEvent", { investigationId: req.params.investigationId || null, body: req.body });
    var event = new Event({
        investigationId: req.params.investigationId || null,
        datasetId: req.body.datasetId || null,
        datasetName: req.body.datasetName || null,
        tag: req.body.tag || [],
        title: req.body.title || "",
        type: req.body.type || null,
        category: req.body.category || null,
        filename: req.body.filename || null,
        fileSize: req.body.fileSize || null,
        username: req.body.username || null,
        software: req.body.software || null,
        machine: req.body.machine || null,
        creationDate: req.body.creationDate || null,
        content: this.translateEventContentToHtml(req.body.content),
        previousVersionEvent: null
    });

    event.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            global.gLogger.error("An error occured during the creation of the event.", { error: err, investigationId: req.params.investigationId || null, body: req.body });
            res.status(500).send({
                message: err.message || "Some error occurred while creating the event"
            });
        });
};

/**
 * Translate a plain text formated event content to html formated event content when necessary. 
 * This operates only on event content which have a plain text formated content  + no html formated content + plain text syntax which needs to be translated such as \n.
 * This concerns notifications or annotations sent from consoles.
 * @param {array} content original event content [{format:'plainText', text: 'this is a comment on one line.\n This is another line' }]
 * @returns {array} updated event content; null when there is no content. 
 * returned example: [{format:'plainText', text: 'this is a comment on one line.\n This is another line' }, {format:'html', text: '<p>this is a comment on one line.</p><p>This is another line</p>' },]
 */
exports.translateEventContentToHtml = (content) => {
    if (content && content instanceof Array && content.length > 0) {
        if (_.find(content, (item) => { return item.format === 'html' }) == undefined) {
            // there is no text in html format
            let plainTextEventContent = _.find(content, (item) => { return item.format === 'plainText' });
            if (plainTextEventContent) {
                if (plainTextEventContent.text.split('\n').length > 1)
                    content.push({
                        format: 'html',
                        text: nl2br(plainTextEventContent.text, false)
                    });
                return content;
            }
        }
        return content;
    }
    return null;
};

/**
 * Create a new event without investigationId. Instead it uses investigationName and instrumentName.
 * @param {string} req.params.investigationName investigation name
 * @param {string} req.params.instrumentName instrument name
 * @param {string} req.params.sessionId the magic token
 */
exports.createEventWithInvestigationNameAndInstrumentName = (req, res) => {
    global.gLogger.info("createEventWithInvestigationNameAndInstrumentName ", { investigationName: req.params.investigationName || null, body: JSON.stringify(req.body), instrumentName: req.params.instrumentName || null });
    if (!req.params.investigationName) {
        global.gLogger.error("createEventWithInvestigationNameAndInstrumentName", { investigationName: req.params.investigationName || null, body: req.body });
        return res.status(400).send({
            message: "Investigation Name can not be empty"
        });
    }

    if (!req.params.instrumentName) {
        global.gLogger.error("createEventWithInvestigationNameAndInstrumentName", { investigationName: req.params.investigationName || null, body: req.body, instrumentName: req.params.instrumentName || null });
        return res.status(400).send({
            message: "Instrument Name can not be empty"
        });
    }

    var investigationName = investigationHelper.normalize(req.params.investigationName);
    global.gLogger.info("InvestigationName has been normalized.", { investigationName: req.params.investigationName, normalized: investigationName });

    /*  Looks for an existing investigation on ICAT which starting date is the closest to the current ingestion time */
    onSession = (sessionId) => {
        onInvestigationsFound = (investigations) => {
            global.gLogger.info("createEventWithInvestigationNameAndInstrumentName. Found investigations by name", { investigationName: investigationName, count: investigations.length });
            /** Filter by instrumentName */
            var investigations = _.filter(investigations, function (inv) {
                var investigationInSameInstrument = _.filter(inv.Investigation.investigationInstruments, function (invInstrument) {
                    return (invInstrument.instrument.name.toUpperCase() == req.params.instrumentName.toUpperCase());
                });

                return (investigationInSameInstrument.length > 0);
            });

	    global.gLogger.info("Investigations filtered by instrument name", {investigations : JSON.stringify(investigations), instrumentName :  req.params.instrumentName, count: investigations.length });
            if (investigations) {
                if (investigations.length > 0) {
                    if (investigations.length == 1) {
                        global.gLogger.info("createEventWithInvestigationNameAndInstrumentName. Investigation found", { investigationName: investigationName, instrumentName: req.params.instrumentName });
                        function onSuccess(data) {
                            /** Data has been successfully stored */
                            res.send(data);
                        }
                        function onError(err) {
                            global.gLogger.error("createEventWithInvestigationNameAndInstrumentName. There was an error", { error: err, investigationName: investigationName || null, body: req.body });
                            res.status(500).send({
                                message: err.message || "Some error occurred while creating the Note."
                            });
                        }
                        // Create a Event
                        const event = new Event({
                            investigationName: investigationName || null,
                            investigationId: investigations[0].Investigation.id || null,
                            datasetId: req.body.datasetId || null,
                            datasetName: req.body.datasetName || null,
                            tag: req.body.tag || [],
                            title: req.body.title || "",
                            type: req.body.type || null,
                            category: req.body.category || null,
                            filename: req.body.filename || null,
                            fileSize: req.body.fileSize || null,
                            username: req.body.username || null,
                            instrumentName: req.params.instrumentName || null,
                            software: req.body.software || null,
                            machine: req.body.machine || null,
                            creationDate: req.body.creationDate || null,
                            content: this.translateEventContentToHtml(req.body.content) || null,
                            previousVersionEvent: null
                        });

                        event.save()
                            .then(data => {
                                onSuccess(data);
                            }).catch(err => {
                                global.gLogger.error("createEventWithInvestigationNameAndInstrumentName. There was an error when saving", { error: err, investigationName: investigationName || null, body: req.body });
                                onError(err);
                            });
                        return;
                    }
                    else {
                        /** Case there are more than 1 investigations */
                        global.gLogger.error("createEventWithInvestigationNameAndInstrumentName. Found multiple sessions for the same investigation and beamline. To be implemented", { investigationName: investigationName, instrumentName: req.params.instrumentName });
                        return res.status(500).send("createEventWithInvestigationNameAndInstrumentName. Found multiple sessions for the same investigation and beamline. To be implemented");
                    }
                }
                else {                    
                    global.gLogger.error("createEventWithInvestigationNameAndInstrumentName. No investigation found with name and beamline", { investigations: investigations, investigationName: investigationName, instrumentName: req.params.instrumentName });
                    return res.status(500).send("createEventWithInvestigationNameAndInstrumentName. No investigation found with name and beamline");
                }
            }
            else {                
                /** No investigations were found */
                global.gLogger.error("createEventWithInvestigationNameAndInstrumentName. No investigation found with name and beamline", { investigations: investigations, investigationName: investigationName, instrumentName: req.params.instrumentName });
                return res.status(500).send("createEventWithInvestigationNameAndInstrumentName. No investigation found with name and beamline");
            }
        }

        function onInvestigationsError(error) {
            global.gLogger.error("createEventWithInvestigationNameAndInstrumentName", { error: error });
            return res.status(400).send("createEventWithInvestigationNameAndInstrumentName. Error when retrieving investigation");
        }

        global.gLogger.error("--> getInvestigationByName", { sessionId: sessionId });
        /** Looks for all investigation with that name */
        icat.getInvestigationByName(sessionId, investigationName, onInvestigationsFound, onInvestigationsError)
    }

    function onSessionError(error) {
        global.gLogger.error(error);
    }

    global.gLogger.error("getSession of notifier user", { user: global.gConfig.icat.authorizations.notifier.user.credentials[0].username });

    /** First we get a session from the notifier in order to find the investigations*/
    icat.getSession(global.gConfig.icat.authorizations.notifier.user, onSession, onSessionError);
};

/**
* Update an event
* @param {string} req.params.investigationId investigation identifier
* @param {string} req.params.sessionId sessin identifier
* @param {string} req.body.event the updated event
*/
exports.modifyEvent = (req, res) => {
    global.gLogger.info("Modify event")
    if (!req.params.investigationId) {
        return res.status(400).send({
            message: "investigationId can not be empty"
        });
    }

    if (!req.body.event) {
        return res.status(400).send({
            message: "Event can not be empty"
        });
    }

    let incomingEvent = null;
    try {
        incomingEvent = req.body.event;
        incomingEvent.investigationId = req.params.investigationId
    }
    catch (exp) {
        return res.status(400).send({
            message: "event is not a JSON object." + exp
        });
    }

    if (incomingEvent) {
        if (!incomingEvent._id) {
            return res.status(400).send({
                message: "_id can not be empty"
            });
        }
    }


    Event.findById(incomingEvent._id).then(currentEventVersion => {
        global.gLogger.info("Found event to modify. _id=" + currentEventVersion._id);
        // create a copy of the current version as it is curretnyl stored in the DB
        let currentEventVersionCopy = JSON.parse(JSON.stringify(currentEventVersion));

        // update event fields
        currentEventVersion.investigationId = incomingEvent.investigationId || null;
        currentEventVersion.datasetId = incomingEvent.datasetId || null;
        currentEventVersion.tag = incomingEvent.tags || [];
        currentEventVersion.title = incomingEvent.title || "";
        currentEventVersion.type = incomingEvent.type || null;
        currentEventVersion.category = incomingEvent.category || null;
        currentEventVersion.filename = incomingEvent.filename || null;
        currentEventVersion.fileSize = incomingEvent.fileSize || null;
        currentEventVersion.username = incomingEvent.username || null;
        currentEventVersion.software = incomingEvent.software || null;
        currentEventVersion.machine = incomingEvent.machine || null;
        currentEventVersion.creationDate = incomingEvent.creationDate || null;
        currentEventVersion.content = incomingEvent.content || null;

        //keep event history
        currentEventVersion.previousVersionEvent = currentEventVersionCopy;

        currentEventVersion.save()
            .then(data => {
                global.gLogger.info("Event modified successfully. _id=" + data._id);
                res.send(data);
            }).catch(err => {
                global.gLogger.error(err);
                res.status(500).send({
                    message: err.message || "Some error occurred while modifying the event."
                });
            });

    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
}


exports.findAllEvents = (req, res) => {
    Event.find()
        .then(events => {
            res.send(events);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving events."
            });
        });
};

/**
 * Find all events related to a given investigationId. An event is a resource which type is 'annotation' or 'notification'
 * @param {*} req 
 * @param {*} res 
 */
exports.findAllEventsByInvestigationId = (req, res) => {
    if (!req.query.sessionId) {
        res.status(401).send({
            message: "Missing sessionId as query param."
        });
    } else {
        Event.find(
            {
                $and: [
                    { 'investigationId': req.params.investigationId },
                    {
                        $or: [{ 'type': 'annotation' }, { 'type': 'notification' }]
                    }
                ]
            })
            /** To convert to javascript objects from Mongoose documents */
            .lean()
            .then(function (events) {
                res.send(replaceSessionId(events, req.query.sessionId));
            })
            .catch(err => {
                console.log(err);
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving events."
                });
            });
    }
}

exports.findByEventId = (req, res) => {
    Event.findById(req.params.eventId)
        .then(event => {
            res.send(event);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving events."
            });
        });
};

exports.deleteAllEvents = (req, res) => {
    Event.remove()
        .then(event => {
            res.send({ message: "Events deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            return res.status(500).send({
                message: "Could not delete note with id " + req.params.noteId
            });
        });
};

function find(find, sort, skip, limit, onSuccess, onError) {
    Event.find(find)
        .sort(sort)
        .skip(skip)
        .limit(limit)
        .lean()
        .then(function (events) {
            onSuccess(events);
        })
        .catch(err => {
            console.log(err);
            onError(err);
        });
}

/**
 * Extract an object from the body on a POST
 * @param {object} req the request which body contains a JSON object
 * @param {string} bodyParam the key of the body param which is searched
 * @return the value of the parameter
 */
function getObject(req, bodyParam) {
    let objectConstructor = {}.constructor;
    function whatTypeIsIt(object) {
        if (object === null) {
            return "null";
        }
        else if (object === undefined) {
            return "undefined";
        }
        else if (object.constructor === objectConstructor) {
            return "Object";
        }
        else {
            return "don't know";
        }
    }

    let findParameterName = "find";
    var filter = {};

    if (req.body && whatTypeIsIt(req.body) === "Object" && req.body[bodyParam]) {
        filter = req.body[bodyParam];
    }

    if (bodyParam === findParameterName) {
        filter.investigationId = req.params.investigationId;
    }

    return filter;
}

/**
 * Get the serverURI
 * @param {object} req the request object
 * @returns {string} the server URI such as https://icatplus.esrf.fr:8000
 */
function getServerURI(req) {
    let protocol = "https://";
    if (!req.secure) {
        protocol = "http://";
    }
    return protocol + req.headers.host;
}

exports.findEvent = (req, res) => {
    let findParameterName = "find";
    global.gLogger.info("findEvent", { investigationId: req.params.investigationId, find: req.body.find, sort: req.body.sort, limit: req.body.limit });

    var onSuccess = (events) => {
        res.send(this.replaceImageSrc(events, req.params.sessionId, req.params.investigationId, getServerURI(req)));
    }
    var onError = (err) => {
        global.gLogger.error(err);
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    }
    find(getObject(req, findParameterName), getObject(req, "sort"), (parseFloat(req.body.skip) || 0), parseFloat(req.body.limit) || global.gConfig.icat.maxQueryLimit, onSuccess, onError);
};

exports.countFind = (req, res) => {
    let findParameterName = "find";
    global.gLogger.info("findEvent", { investigationId: req.params.investigationId, find: req.body.find, sort: req.body.sort, limit: req.body.limit });
    var onSuccess = function (events) {
        res.send(String(events.length));
    }
    var onError = function (err) {
        res.statuds(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    }
    find(getObject(req, findParameterName), getObject(req, "sort"), (parseFloat(req.body.skip) || 0), parseFloat(req.body.limit) || global.gConfig.icat.maxQueryLimit, onSuccess, onError);
};

/**
 * Get HTML formated text for a given event
 * @param {Event} event event to extract the HTML text from
 * @returns {string} html formated text 
 */
function getHTMLTextByEvent(event) {
    let finalHTMLText = "";
    if (event) {
        let eventsToConvert = [];

        // For notifications, get the original version of the event
        if (event.type === CONSTANTS.EVENT_TYPE_NOTIFICATION) {
            if (event.previousVersionEvent) {
                let originalVersion = event;
                try {
                    while (originalVersion.previousVersionEvent) {
                        originalVersion = originalVersion.previousVersionEvent;
                    }
                    eventsToConvert.push(originalVersion);
                }
                catch (e) {
                    global.gLogger.error(e);
                }
            }
        }

        eventsToConvert.push(event);

        // Build the HTML text
        function layoutEventInPDF(eventFormattedContent, date, isANotificationComment) {
            if (eventFormattedContent.text) {
                function getTextColorByCategory(category) {
                    if (category === CONSTANTS.EVENT_CATEGORY_INFO) {
                        return "blue";
                    }
                    if (category === CONSTANTS.EVENT_CATEGORY_ERROR) {
                        return "red";
                    }
                    if (category === CONSTANTS.EVENT_CATEGORY_COMMANDLINE) {
                        return "gray";
                    }
                    if (category === CONSTANTS.EVENT_CATEGORY_DEBUG) {
                        return "purple";
                    }
                    return "black";
                }

                function getFontSizeByType(eventType) {
                    if (eventType === CONSTANTS.EVENT_TYPE_ANNOTATION) {
                        return "12px";
                    }
                    if (eventType === CONSTANTS.EVENT_TYPE_NOTIFICATION) {
                        return "10px";
                    }
                }

                let dateComponent = "<span style='color:#B5B5B5'> [" + date.utc().format('YYYY-MM-DD') + "] </span>";
                let timeComponent = "<span style='color:#B5B5B5'> [" + date.utc().format('HH:mm:ss') + "] </span>";

                if (isANotificationComment) {
                    return "<div style='text-align:left;margin-left:30px;font-size:" + getFontSizeByType(CONSTANTS.EVENT_TYPE_ANNOTATION) + "; color:" + getTextColorByCategory(CONSTANTS.EVENT_CATEGORY_COMMENT) + ";'>" +
                        "<div style='float:left; padding-right:10px;'>" + timeComponent + dateComponent + " <span style='color:#B5B5B5'> (latest comment) </span> </div>" +
                        eventFormattedContent.text.replace(/\"/g, '"') +
                        "</div>";

                } else {
                    return "<div style='text-align:left;font-size:" + getFontSizeByType(event.type) + "; color:" + getTextColorByCategory(event.category) + ";'>" +
                        "<div style='float:left; padding-right:10px;'>" + timeComponent + dateComponent + "</div>" +
                        eventFormattedContent.text.replace(/\"/g, '"') +
                        "</div>";
                }
            }
            return "";
        }

        for (let i = 0; i < eventsToConvert.length; i++) {
            let event = eventsToConvert[i];
            let isANotificationComment = (i === 1) ? true : false;

            if (event.content) {
                let HTMLContent = event.content.find(function (o) {
                    if (o.format) { return o.format === CONSTANTS.EVENT_CONTENT_HTML_FORMAT; }
                });
                if (HTMLContent) {
                    finalHTMLText = finalHTMLText + layoutEventInPDF(HTMLContent, moment(event.creationDate), isANotificationComment);
                } else {
                    let plainTextContent = event.content.find(function (o) {
                        if (o.format) { return o.format === CONSTANTS.EVENT_CONTENT_PLAINTEXT_FORMAT; }
                    });
                    if (plainTextContent) {
                        plainTextContent.text = plainTextContent.text + "<br/>";
                        finalHTMLText = finalHTMLText + layoutEventInPDF(plainTextContent, moment(event.creationDate), isANotificationComment);
                    }
                }
            }
        }
    }
    return finalHTMLText;
}


/**
 * Generate a PDF for the logbook associated to a given investigation after an event filtration step.
 * @param {string} req.params.investigationId investigation identifier
 * @param {string} req.params.sessionId session identifier
 * @param {object} req.query.find selection filter for finding in mongoDB
 * @param {object} req.query.sort selection filter for sorting in mongoDB
 * @param {string} req.query.skip number of events to skip from the find result from mongoDB
 * @param {object} req.query.limit maximum number of event to extract from the find result from mongoDB
 */
exports.generatePDFByQuery = (req, res) => {
    let sessionId = req.params.sessionId;
    let investigationId = req.params.investigationId;
    let findQuery = req.query.find;
    let sort = req.query.sort;
    let skip = req.query.skip;
    let limit = req.query.limit;

    global.gLogger.debug("Generating PDF");
    req.setTimeout(TIMEOUT);

    global.gLogger.info("generatePDFByQuery", { investigationId: investigationId, find: findQuery, sort: sort, skip: skip, limit: limit });

    let onSuccess = function (events) {
        global.gLogger.debug("PDF generation succeeded.");
        sendPDF(convertEventsToHTML(events, sessionId, investigationId, getServerURI(req)), req, sessionId, investigationId, res);
        global.gLogger.debug("PDF has been sent");
    }

    let onError = function (err) {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    }

    let selectionFilter = JSON.parse(findQuery);
    selectionFilter.investigationId = investigationId;

    find(selectionFilter, JSON.parse(sort), (parseFloat(skip) || 0), parseFloat(limit) || global.gConfig.icat.maxQueryLimit, onSuccess, onError);
}

/**
 * Concatenate all events into a single string as html format
 * @param {Array} events list of events to be concatenated
 * @param {string} investigationId investigation identifier
 * @param {string} serverURI protocol and server name of icat+
 * @returns {string} all events in HTML format
 */
convertEventsToHTML = (events, sessionId, investigationId, serverURI) => {
    let html = "";
    if (events) {
        if (events.length > 0) {
            // replaces image src value with a new serverURI, new sessionId and using a new API
            events = this.replaceImageSrc(events, sessionId, investigationId, serverURI)

            events.forEach(function (event) {
                html = html + getHTMLTextByEvent(event);
            }, this);
        }
    }
    return html;
}

sendPDF = (html, req, sessionId, investigationId, response) => {
    function onInvestigationsFound(investigation) {
        var summary = "";
        var proposal = "";
        var instrument = "";
        try {
            if (investigation[0]) {
                summary = (investigation[0].Investigation.summary);
                proposal = (investigation[0].Investigation.title).toUpperCase();
                instrument = investigation[0].Investigation.investigationInstruments[0].instrument.name;
            }
        }
        catch (e) {
            console.log(e);
        }

        /** This is need because we want to access to the ESRF logo that is in the public folder as a img so we need to build the http URL */
        var serverURL = "http://localhost:" + global.gConfig.server.port;
        var searchParams = "?title=" + summary + "&instrument=" + instrument + "&proposal=" + proposal + "&logo=" + serverURL + "/static/pdf/esrf_logo.jpg";

        var header = serverURL + "/static/pdf/header.html" + searchParams;
        var footer = serverURL + "/static/pdf/footer.html" + searchParams;
        global.gLogger.debug("PDF Process has started");
        var pdf = wkhtmltopdf(html, {
            pageSize: 'A4',
            orientation: 'Portrait',
            marginTop: 15,
            marginTop: 10,
            marginBottom: 10,
            marginLeft: 10,
            marginRight: 10,
            //"header-html": header,
            "footer-html": footer,
            "header-spacing": 5
        })
        global.gLogger.debug("PDF Process has finished");
        pdf.pipe(response);
    }

    function onInvestigationsError(error) {
        console.log(error);
    }
    icat.getInvestigationById(sessionId, investigationId, onInvestigationsFound, onInvestigationsError)

}
/**
 * Generate a PDF for a given investigation
 * @param {*} req the http request
 * @param {*} res the http response
 */
exports.generatePDFByInvestigation = (req, res) => {
    req.setTimeout(TIMEOUT);
    let investigationId = req.params.investigationId;


    function onInvestigationsFound(investigation) {
        var summary = "";
        var proposal = "";
        var instrument = "";
        if (investigation[0]) {
            summary = (investigation[0].Investigation.summary);
            proposal = (investigation[0].Investigation.title).toUpperCase();
            instrument = investigation[0].Investigation.investigationInstruments[0].instrument.name;
        }


        find({ 'investigationId': investigationId }, {}, (parseFloat(req.query.skip) || 0), parseFloat(req.query.limit) || global.gConfig.icat.maxQueryLimit,
            function (events) {
                sendPDF(convertEventsToHTML(events, req.query.sessionId), req, req.query.sessionId, investigationId, res);
            },
            function (err) {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving events."
                });
            });
        //Event.find({ 'investigationId': investigationId })
        //    .then(events => {
        /** html variable will contain the HTML to be used to export as PDF */
        //        sendPDF(convertEventsToHTML(events, req.query.sessionId), req, req.query.sessionId, investigationId, res);
        //    }).catch(err => {
        //        res.status(500).send({
        //            message: err.message || "Some error occurred while retrieving events."
        //        });
        //    });
    }
    function onInvestigationsError(error) {
        console.log(error);
    }
    icat.getInvestigationById(req.query.sessionId, investigationId, onInvestigationsFound, onInvestigationsError)
};

/**
 * Get all tags for a given investigation
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.params.investigationId investigation identifier
 * @param {*} res the http response
 */
exports.getTags = (req, res) => {
    Tag.find({ investigationId: req.params.investigationId })
        .then((data) => {
            res.status(200).send({ tags: data })
        })
        .catch(() => {
            res.status(500).send("An error occured while searhcing for tags for investigation " + req.params.investigationId)
        })
}


/**
 * Create an investigation tag. Its scope is limited to a single investigation
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.params.investigationId investigation identifier which determines the scope of the tag to be created
 * @param {object} req.body.tag tag to be created
 * @param {*} res http response
 */
exports.createInvestigationTag = (req, res) => {
    global.gLogger.info("Creating the investigation tag", { tag: req.body || null });

    if (req.body.beamlineName) {
        global.gLogger.error("Could not create the investigation tag because beamlineName attribute was present in the body.", { tag: req.body || null });
        res.status(400).send("BeamlineName attribute should not be present in the body.")
        return
    }
    const tag = new Tag({
        name: req.body.name,
        description: req.body.description,
        color: req.body.color,
        investigationId: parseFloat(req.params.investigationId)
    });


    tag.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            global.gLogger.error("An error occured during the creation of the tag.", { error: err, investigationId: req.params.investigationId || null, body: req.body });
            res.status(500).send({
                message: err.message || "Some error occurred while creating the tag"
            });
        });
}


/**
 * Create a beamline tag. Its scope is limited to all investigations of a given beamline
 * @param {string} req.params.sessionId session identifier
 * @param {string} req.params.instrumentName beamline name which determines the scope of the tag to be created
 * @param {object} req.body.tag tag to be created
 * @param {*} res http response
 */
exports.createBeamlineTag = (req, res) => {
    global.gLogger.info("Creating the beamline tag", { tag: req.body || null });

    if (req.body.investigationId) {
        global.gLogger.error("Could not create the beamline tag because investigationId attribute was present in the body.", { tag: req.body || null });
        res.status(400).send("InvestigationId attribute should not be present in the body.")
        return
    }

    const tag = new Tag({
        name: req.body.name,
        description: req.body.description,
        color: req.body.color,
        beamlineName: req.params.instrumentName
    });

    tag.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            global.gLogger.error("An error occured during the creation of the tag.", { error: err, beamlineName: req.params.instrumentName || null, body: req.body });
            res.status(500).send({
                message: err.message || "Some error occurred while creating the tag"
            });
        });
}

/**
 * Update an existing investigation tag
 * @param {string} req.params.sessionId session indentifier
 * @param {string} req.params.tagId investigation tag identifier which needs to be updated
 * @param {object} req.body.tag investigation tag used for the update.
 */
exports.updateInvestigationTag = (req, res) => {

    // Check that tagId is there and not empty
    if (!req.params.tagId) {
        global.gLogger.error("no tag id found")
        return res.status(400).send({ message: "tagId can not be empty" });
    }
    if (req.params.tagId.length === "") {
        global.gLogger.error("tagId is empty")
        return res.status(400).send({ message: "tagId can not be empty" });
    }
    let tagId = req.params.tagId;

    // Check that investigationId is there and not empty
    if (!req.params.investigationId) {
        global.gLogger.error("investigationId was not found . Tag scope is unknown");
        return res.status(400).send({ message: "investigationId field was not found. Tag scope is unknown" });
    }
    if (req.params.investigationId.length === "") {
        global.gLogger.error("investigationId is empty")
        return res.status(400).send({ message: "investigationId can not be empty" });
    }
    let investigationId = req.params.investigationId;

    global.gLogger.info("Updating the investigation tag: ", { tagId: req.params.tagId, investigationId: req.params.investigationId })


    // Check that tag object is in the body
    if (!req.body) {
        return res.status(400).send({ message: "Could not get the tag object from the body" });
    }

    let incomingTag = req.body;
    // Check that mandatory fields are provided
    if (!incomingTag.name || incomingTag.name === "") {
        global.gLogger.error("name field was not found or empty. Tag name is unknown");
        return res.status(400).send({ message: "name field was not found or empty. Tag name is unknown" });
    }

    Tag.findById(tagId)
        .then((foundTag) => {
            foundTag.name = incomingTag.name;
            foundTag.description = incomingTag.description || null;
            foundTag.color = incomingTag.color || null;
            foundTag.investigationId = investigationId

            foundTag.save()
                .then(data => {
                    global.gLogger.info("Tag modified successfully. _id=" + data._id);
                    res.send(data);
                }).catch(err => {
                    global.gLogger.error(err);
                    res.status(500).send({
                        message: err.message || "Some error occurred while modifying the tag."
                    });
                });
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving tags."
            });
        });
}
