const Event = require('../models/event.model.js');
const fs = require('fs');
const auth = require('../authentication/icat.js');
const constants = require('../constants.js');


// Create a storage object with a given configuration
const storage = require('multer-gridfs-storage')({
    url: global.gConfig.database.uri,
    file: (req, file) => {
        return {
            filename: file.originalname
        };
    }
});

var multer = require('multer');
// Set multer storage engine to the newly created object
const upload = multer({ storage: storage }).single('file');


// Create and Save a new event
exports.upload = (req, res) => {
    global.gLogger.debug("Upload file", { });
    upload(req, res, function (err) {
        if (err) {
            res.json({ error_code: 1, err_desc: err });
            return;
        }

        /**
         * Creating the event
         */
        const event = new Event({
            investigationId: req.body.investigationId,
            datasetId: req.body.datasetId || null,
            type: constants.EVENT_TYPE_ATTACHMENT,
            category: constants.EVENT_CATEGORY_FILE,
            text: req.body.text,
            filename: req.file.filename,
            username: req.body.username || null,
            software: req.body.software || null,
            machine: req.body.machine || null,
            creationDate: req.body.creationDate || null,
            file: req.file.id,
            contentType: req.file.contentType
        });
        event.save()
            .then(data => {
                res.send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the Note."
                });
            });
    });
};

// Create and Save a new event
exports.test = (req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write('<form action="/investigations/0/events/file" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="file"><br>');
    res.write('<input type="text" name="investigationId" value="0">');
    res.write('<input type="submit">');
    res.write('</form>');
    res.send();
};


/**
 * Download a file given a eventId. 
 *
 **/
exports.download = (req, res) => {
    var gridfs = require('mongoose-gridfs')({
        collection: 'fs'
        //model: 'Attachment'
    });
    
    Event.findById(req.params.resourceId)
        .then(event => {
            let comingStream = gridfs.model.readById(event.file[0]);
            comingStream.pipe(res);
            comingStream.on('error', function (err) {
                console.log(err);
            });

        }).catch(err => {
            res.status(500).send({
                message: "Event not found"
            });
        });

};
