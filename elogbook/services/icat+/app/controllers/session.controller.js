const icat = require('../api/icat.js');
const cache = require('../cache/cache.js');

/**
 * Get information about the session.
 * see doc for details on the corresponding API method /session/{sessionId}
 * 
 * @param Object req the http request
 * @param Object res the http response
 * @param {function} next the next function
 */
exports.getSessionInformation = (req, res, next) => {
    function onSuccess(data) {
        res.send(data);
    }

    function onError(error) {
        res.status(500).send("Error when retrieving the session information. Your session might be expired or it is invalid.");
    }

    icat.getSessionInformation(req.params.sessionId, onSuccess, onError);
};



/**
 * Login to ICAT+
 * see doc for details on the corresponding API method /session
 * 
 * @param request the http request
 * @param response the http response
 * @param {function} next the next function
 */
exports.login = (req, res, next) => {
    function onSuccess(sessionId) {
        global.gLogger.info("Login done successfully. ", { username: req.body.username, plugin: req.body.plugin });
        global.gLogger.debug("Init cache for user" + req.body.username);

        cache.initSession(sessionId, req.body.username, function () {
            res.send({
                sessionId: sessionId,
                login: req.body.username,
                name: cache.getUsernameBySessionId(sessionId),
                fullName: cache.getUserFullNameBySessionId(sessionId),
                lifeTimeMinutes: cache.getLifeTimeMinutes(),
                isAdministrator: cache.isAdministrator(sessionId)
            });
        });

    }

    function onError(error) {
        next(error);

    }

    var authentication = {
        "plugin": req.body.plugin,
        "credentials": [
            {
                "username": req.body.username
            },
            { "password": req.body.password }
        ]
    };
    icat.getSession(authentication, onSuccess, onError)
};

