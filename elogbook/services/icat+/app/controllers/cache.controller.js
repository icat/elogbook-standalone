const cache = require('../cache/cache.js');


exports.getSessionById = (req, res) => {    
   res.send(cache.getSessionById(req.query.sessionId));
};

exports.getStats = (req, res) => {    
   res.send(cache.getStats());
};


exports.getCacheKeys = (req, res) => {    
   res.send(cache.getKeys());
};

exports.getCacheKey = (req, res) => {    
   res.send(cache.getDataByKey(req.params.key));
};

