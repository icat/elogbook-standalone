
const axios = require('axios-https-proxy-fix');
const icat = require('../api/icat.js');
const datasetDocumentParser = require('../api/parsers/datasetdocumentparser.js');

exports.getStatusByDatasetIds = (req, res) => {
    function onSuccess(status) {
        res.send(status);
    }
    function onError(error) {        
        res.status(500).send(error.response.data);
    }
    try {
        global.gLogger.info("getStatusByDatasetIds", { datasetIds: req.params.datasetIds });
        icat.getStatusByDatasetIds(req.params.sessionId, req.params.datasetIds, onSuccess, onError);
    }
    catch (error) {
        global.gLogger.error(error);
    }
};
/**
 * Get all datasets for a given investigationId.
 */
exports.getDatasetsByInvestigationId = (req, res) => {
    function onSuccess(datasets) {        
        global.gLogger.info("getDatasetsByInvestigationId succeed", { investigationId: req.params.investigationId });
        res.send(datasets);
    }
    function onError(error) {        
        res.send(error);
    }
    try {        
        global.gLogger.info("getDatasetsByInvestigationId", { investigationId: req.params.investigationId });        
        icat.getDatasetsByInvestigationId(req.params.sessionId, req.params.investigationId, onSuccess, onError);
    }
    catch (error) {
        global.gLogger.error(error);
    }
};

/**
 * Get all datasets for a given investigationId.
 */
exports.getDatasetDocumentsByInvestigationId = (req, res) => {
    function onSuccess(datasets) {
        res.send(datasetDocumentParser.parse(datasets));
    }
    function onError(error) {
        res.send(error);
    }
    try {
        global.gLogger.info("getDatasetDocumentsByInvestigationId", { investigationId: req.params.investigationId });
        icat.getDatasetsByInvestigationId(req.params.sessionId, req.params.investigationId, onSuccess, onError);
    }
    catch (error) {
        global.gLogger.error(error);
    }
};

/**
 * Get dataset from a list of datasetId
 */
exports.getDatasetsById = (req, res, next) => {
    function onSuccess(datasets) {
        res.send(datasets);
    }
    function onError(error) {
        next(error);
    }
    try {
        icat.getDatasetsById(req.params.sessionId, req.params.datasetIds, onSuccess, onError);
    }
    catch (error) {
        global.gLogger.error(error);
    }
};


/**
 * Get dataset documents from a list of datasetId
 */
exports.getDatasetsDocumentsById = (req, res, next) => {
    function onSuccess(datasets) {
        res.send(datasetDocumentParser.parse(datasets));
    }
    function onError(error) {
        next(error);
    }
    try {
        icat.getDatasetsById(req.params.sessionId, req.params.datasetIds, onSuccess, onError);
    }
    catch (error) {
        global.gLogger.error(error);
    }
};



/**
 * Get the datacollection associated to a given dataset id
 */
exports.getDataColletionByDatasetId = (req, res, next) => {
    function onSuccess(datasets) {
        res.send(datasets);
    }
    function onError(error) {
        log.error(error);
        next(error);
    }
    try {
        icat.getDataColletionByDatasetId(req.params.sessionId, req.params.datasetId, onSuccess, onError);
    }
    catch (error) {
        global.gLogger.error(error);
    }
};


exports.getDatasetsByDOI = (req, res, next) => {
    function onSuccess(datasets) {        
        res.send(datasets);
    }
    function onError(error) {
        return res.status(400).send({ message: error });
    }
    try {       
        icat.getDatasetsByDOI(req.query.sessionId, req.params.prefix + "/" + req.params.suffix, onSuccess, onError);
    }
    catch (error) {
        global.gLogger.error(error);
    }
};



