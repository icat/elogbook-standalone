const icat = require('../api/icat.js');
const cache = require('../cache/cache.js');
const investigationHelper = require('../helpers/investigation.js');


exports.getDatafilesByDatasetId = (req, res) => {    
     function onSuccess(datafiles){        
        res.send(datafiles);
    }
    function onError(error){       
       next(error);
    }
    try{
        icat.getDatafilesByDatasetId(req.params.sessionId, req.params.datasetId, onSuccess, onError);
    }
    catch(error){
        console.log(error);
    }
};
