const _ = require('lodash');
const axios = require('axios-https-proxy-fix');
const template = require('../doi/datacite.template.js');
const icat = require('../api/icat.js');
const icatURL = require('../api/query.js');
const CONSTANTS = require('../constants.js');
const cache = require('../cache/cache.js');
const moment = require('moment');

function getDataCiteJson(doi, investigation) {
    var datacite = {};
    datacite.doi = doi;
    datacite.url = global.gConfig.datacite.landingPage + doi;
    datacite.publicationYear = moment(investigation.releaseDate, 'YYYY-MM-DDThh:mm:ss.sTZD').format('YYYY');
    datacite.creators = [];
    if (investigation.investigationUsers) {
        for (var i = 0; i < investigation.investigationUsers.length; i++) {
            datacite.creators.push({
                name: investigation.investigationUsers[i].user.fullName
            });
        }
    }

    datacite.titles = [{
        title: investigation.summary
    }];
    function getResourceType(doi) {
        if (doi.toUpperCase().indexOf("-DC-")) {
            return "Datacollection";
        }
        return "Experiment Session";
    }

    datacite.types = {
        "resourceTypeGeneral": "Dataset",
        "resourceType": getResourceType(doi)
    };

    var investigationType = (cache.getInvestigationTypes().find(function (o) {
        return o.InvestigationType.name == investigation.type.name;
    }));

    var investigationDescription = "";
    if (investigationType) {
        if (investigationType.InvestigationType) {
            investigationDescription = investigationType.InvestigationType.description;
        }
    }

    datacite.publisher = global.gConfig.datacite.publisher;
    datacite.subjects = [{
        "subject": investigationDescription,
        "subjectScheme": "Proposal Type Description"
    },
    {
        "subject": investigation.title,
        "subjectScheme": "Proposal"
    },
    {
        "subject": investigation.visitId,
        "subjectScheme": "Instrument"
    }

    ];

    datacite.dates = [{
        "date": moment(investigation.startDate, 'YYYY-MM-DDThh:mm:ss.sTZD').format('YYYY-MM-DD'),
        "dateType": "Collected"
    },
    {
        "date": moment(investigation.releaseDate, 'YYYY-MM-DDThh:mm:ss.sTZD').format('YYYY'),
        "dateType": "Issued"
    }
    ];
    return datacite;


}

exports.getJSONDataciteByDOI = (req, res, next) => {
    function onSuccess(investigation) {
        if (investigation) {
            if (investigation[0]) {
                if (investigation[0].Investigation) {
                    global.gLogger.info(JSON.stringify(investigation[0].Investigation));
                    res.status(200).send(getDataCiteJson(doi, investigation[0].Investigation));

                }
            }
        }
    }

    function onError(error) {
        global.gLogger.error(error);
    }

    try {
        if (req.params.prefix) {
            if (req.params.suffix) {
                var doi = req.params.prefix + "/" + req.params.suffix;
                icat.getInvestigationByDOI(global.gConfig.icat.authorizations.minting.user, doi, onSuccess, onError);
            } else {
                res.status(400).send();
            }
        }
        else {
            res.status(400).send();
        }
    }
    catch (e) {
        global.gLogger.error(e);
        res.status(500).send();
    }


};

exports.mint = async function (req, res, next) {
    

    let sessionId = req.params.sessionId;
    let datasetIds = req.body.datasetIdList;
    let title = req.body.title;
    let abstract = req.body.abstract;
    let authors = req.body.authors;

    global.gLogger.debug('Starts mint', {sessionId : sessionId, datasetIds:datasetIds, title:title, abstract:abstract,authors:authors});
    
    try {
        
        let investigationsAndInvestigationUsers = await Promise.all([
            getInvestigationsByDatasetListIds(sessionId, datasetIds),
            getInvestigationUserBySessionId(sessionId)
        ])

        if (!investigationsAndInvestigationUsers && investigationsAndInvestigationUsers.length != 2) {
            let error = new Error("[ERROR] getInvestigationsByDatasetListIds() and/or getInvestigationUserBySessionId() failed");
            throw error;
        }

        let investigations = investigationsAndInvestigationUsers[0];
        let investigationUsers = investigationsAndInvestigationUsers[1];

        let investigationIds = _.uniq((investigations.map(function (o) {
            return o.Investigation.id;
        })));
        let investigationNames = _.uniq((investigations.map(function (o) {
            return o.Investigation.name;
        })));
        let instrumentsNames = _.uniq((investigations.map(function (o) {
            return o.Investigation.investigationInstruments[0].instrument.name;
        })));

        global.gLogger.debug('[Mint] Parsing', {investigationIds : investigationIds, investigationNames:investigationNames, title:title, instrumentsNames:instrumentsNames});

        /**
         * Return true if it sessionId corresponds to a PI
         */
        function isPrincipalInvestigator(investigationId) {            
            return _.find(investigationUsers, function (o) {
                return (o.investigationId == investigationId) && (o.role === CONSTANTS.ROLE_PRINCIPAL_INVESTIGATOR);
            });
        }
        
        /** Foreach investigationId we check that user is a local contact or PI it means:  'Principal investigator' or 'Local contact'*/
        for (var i = 0; i < investigationIds.length; i++) {
            if (!isPrincipalInvestigator(investigationIds[i])) {
                /** This user has not permission because it is not PI or local contact */
                var message = 'You are not allowed to mint these dataset/s because are not PI for this investigation';
                global.gLogger.error(message, {
                    investigationUsers: investigationUsers,
                    datasetIds: datasetIds,
                    investigations: investigations
                });
                let error = new Error('You are not allowed to mint these dataset/s because are not PI for this investigation');
                throw error;
            }
        }
        global.gLogger.debug('[Mint] Allowed', {investigationIds : investigationIds, investigationNames:investigationNames, title:title, instrumentsNames:instrumentsNames});


        /** the person who has requested the DOI */
        var mintedByName = null;
        var mintedByFullName = null;
        if (investigationUsers) {
            if (investigationUsers.length > 0) {
                mintedByName = investigationUsers[0].name;
                mintedByFullName = investigationUsers[0].fullName;
            }
        }
        global.gLogger.debug('[Mint] Creating a datacollection');
        /** User is allowed to mint because it is PI */
        let dataCollectionId = await createDataCollection(datasetIds, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName);
        global.gLogger.debug('[Mint] Created datacollection', { dataCollectionId, dataCollectionId});
        dataCollectionId = String(dataCollectionId[0]);
        global.gLogger.debug('[Mint] Minting DOI', { dataCollectionId, dataCollectionId, title:title});
        let doi = await mintDOI(dataCollectionId, title, abstract, authors, datasetIds, investigationNames, instrumentsNames);

        let success = await updateDataCollectionDOI(dataCollectionId, doi)
        res.status(200).send({
            message: doi
        });

    } catch (e) {
        global.gLogger.error('[ERROR] An error occured during the whole minting process');
        next(e);
    }
}

/**
 * Create a datacollection in the metadata manager for given dataset
 * @param {*} minterCredential 
 * @returns {Promise}
 */
function createDataCollection(datasetIds, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName) {
    return new Promise((resolve, reject) => {
        function onSuccess(dataCollectionId) {
            resolve(dataCollectionId)
        }

        function onError(error) {
            console.log(error);
            global.gLogger.error('[ERROR] DataCollection was not created')
            global.gLogger.error(error);
            reject()
        }

        icat.mint(global.gConfig.icat.authorizations.minting.user, datasetIds, title, abstract, investigationNames, instrumentsNames, mintedByName, mintedByFullName, onSuccess, onError);
    })

}

/**
 * Updates a dataCollection with a new DOI on the metadata catalogue
 * @param {String} dataCollectionId datacollection identifier
 * @param {String} doi digital object identifier
 */
function updateDataCollectionDOI(dataCollectionId, doi) {
    return new Promise((resolve, reject) => {
        function onDataCollectionUpdatedSuccess(dataCollection) {
            resolve("success");
        }

        function onDataCollectionUpdatedError(error) {
            reject();
        }

        icat.updateDataCollectionDOI(global.gConfig.icat.authorizations.minting.user, parseInt(dataCollectionId), doi, onDataCollectionUpdatedSuccess, onDataCollectionUpdatedError);
    })
}

/**
 * Get all the investigations chich the provided dataset identified by datasetIds are part of
 * @param {*} sessionId 
 * @param {*} datasetIds
 * @return {Promise} 
 */
function getInvestigationsByDatasetListIds(sessionId, datasetIds) {
    return new Promise((resolve, reject) => {
        // let myResolve = resolve;
        // let myReject = reject;
        const proxy = false;
        axios.get(icatURL.getInvestigationsByDatasetListIdsURL(sessionId, datasetIds), { proxy })
            .then(response => {
                global.gLogger.info('[SUCCESS] Investigations received.', { investigations: JSON.stringify(response.data) });
                var investigations = response.data;
                resolve(investigations);

            }).catch(error => {
                var err = new Error('getInvestigationsByDatasetListIds failed to retrieve data');
                global.gLogger.info('[ERROR] Reception of investigations failed.');

                err.status = 401;
                reject(err);
            });
    })
}

/**
 * Get all the participations of a given user.
 * @param {*} sessionId 
 * @return {Promise} 
 */
function getInvestigationUserBySessionId(sessionId) {
    return new Promise((resolve, reject) => {
        function onInvestigationUserRetrieved(investigationUsers) {
            global.gLogger.info('[SUCCESS] InvestigationUser received.', { investigationUsers: JSON.stringify(investigationUsers) });
            resolve(investigationUsers)
        }

        function onInvestigationUserError(error) {
            var err = new Error('getInvestigationUserBySessionId failed to retrieve data');
            global.gLogger.error('[ERROR] Could not receive InvestigationUsers.');
            err.status = 401;
            reject(err);
        }

        icat.getInvestigationUserBySessionId(sessionId, onInvestigationUserRetrieved, onInvestigationUserError);
    })
}

/**
 * Mint the DOI at datacite
 * @param {*} dataCollectionId dataCollection identifier
 * @param {*} title title of teh DOI
 * @param {*} abstract abstract of the DOI
 * @param {*} authors authors of the DOI
 * @param {*} datasetIdList identifiers of the datasets used in this DOI
 * @param {*} investigationNames 
 * @param {*} instrumentsNames 
 * @return {Promise}
 */
function mintDOI(dataCollectionId, title, abstract, authors, datasetIdList, investigationNames, instrumentsNames) {
    return new Promise((resolve, reject) => {
        let doi = global.gConfig.datacite.prefix + '/' + global.gConfig.datacite.suffix + dataCollectionId;
        let url = global.gConfig.datacite.landingPage + doi;
        let xmls = "";
        try {
            xmls = generateXML(doi, dataCollectionId, title, abstract, authors, investigationNames, instrumentsNames);
            global.gLogger.info("Creation of the XML from the DOI to be minted as requested by datacite", {
                xmls: xmls,
                doi: doi,
                title: title,
                abstract: abstract,
                datasetIdList: datasetIdList,
                investigationNames: investigationNames,
                instrumentsNames: instrumentsNames
            });

        } catch (error) {
            global.gLogger.error("[ERROR] Creation of the XML for DOI failed");
            reject("[ERROR] Creation of the XML for DOI failed");
            return;
        }

        axios({
            method: 'post',
            url: global.gConfig.datacite.mds + "/metadata",
            headers: {
                'content-type': 'application/xml',
                'charset': 'UTF-8'
            },
            auth: {
                username: global.gConfig.datacite.username,
                password: global.gConfig.datacite.password,
            },
            proxy: {
                host: global.gConfig.datacite.proxy.host,
                port: global.gConfig.datacite.proxy.port
            },
            params: {},
            data: xmls

        })
            .then(function (response) {
                global.gLogger.info('[SUCCESS] DOI has been minted on datacite');
                axios({
                    method: 'post',
                    url: global.gConfig.datacite.mds + "/doi",
                    auth: {
                        username: global.gConfig.datacite.username,
                        password: global.gConfig.datacite.password,
                    },
                    proxy: {
                        host: global.gConfig.datacite.proxy.host,
                        port: global.gConfig.datacite.proxy.port
                    },
                    params: {
                        doi: doi,
                        url: url
                    }

                })
                    .then(function (response) {
                        global.gLogger.info('[SUCCESS] DOI has been registered on datacite');
                        resolve(doi);
             
                    })
                    .catch(function (error) {
                        global.gLogger.error('[ERROR] Failed registering the newly created DOI on datacite');
                        reject();
                    });
            })
            .catch(function (error) {
                global.gLogger.error('[ERROR] Could not mint the DOI on datacite');
                reject();
            });
    })
}


function generateXML(doi, dataCollectionId, title, abstract, authors, investigationNames, instrumentsNames) {

    /** This function is a string is blank */
    function isBlank(str) {
        return (!str || /^\s*$/.test(str));
    }

    /** validation */
    if (authors) {
        if (authors.length == 0) {
            throw "List of authors is empty";
        }
    } else {
        throw "No authors found";
    }

    if (isBlank(title)) {
        throw "Title should not be empty";
    }

    if (isBlank(abstract)) {
        throw "Abstract should not be empty";
    }

    if (dataCollectionId) {
        if (Number.isInteger(dataCollectionId)) {
            throw "It was not possible to create a data collection on ICAT";
        }
    }

    /** Parsing authors */
    let authorsXML = "";
    for (var i = 0; i < authors.length; i++) {
        /*
        <creator>
			<creatorName>Fosmire, Michael</creatorName>
		</creator>
        */
        authorsXML = authorsXML + "<creator><creatorName>" + authors[i].surname + "," + authors[i].name + "</creatorName></creator>";
    }

    let xml = template.xml.replace("%DOI%", doi)
        .replace("%PUBLICATION_YEAR%", new Date().getFullYear())
        .replace("%AUTHORS%", authorsXML)
        .replace("%TITLE%", title)
        .replace("%ABSTRACT%", abstract)
        .replace("%INSTRUMENT_NAME%", instrumentsNames)
        .replace("%INVESTIGATION_NAME%", investigationNames);

    return xml;

}
