const icat = require('../api/icat.js');
const cache = require('../cache/cache.js');
const investigationHelper = require('../helpers/investigation.js');
const parser = require('../api/parsers/investigationparser.js');

exports.normalize = (req, res) => {     
    if ( req.params.investigationName ){
        return  res.send(investigationHelper.normalize(req.params.investigationName));
    }
    else{
        return res.status(500).send("No investigation name given");
    }

};


exports.getInvestigationLightById = (req, res) => {    
    function onSuccess(investigation){    
        debugger    
       res.send(parser.parse(investigation));
   }
   function onError(error){       
      next(error);
   }
   try{
       icat.getInvestigationById(req.params.sessionId, req.params.investigationId, onSuccess, onError);
   }
   catch(error){
       console.log(error);
   }
};


exports.getInvestigationById = (req, res) => {    
     function onSuccess(InvestigationUsers){        
        res.send(InvestigationUsers);
    }
    function onError(error){       
       next(error);
    }
    try{
        icat.getInvestigationById(req.params.sessionId, req.params.investigationId, onSuccess, onError);
    }
    catch(error){
        console.log(error);
    }
};

exports.getInvestigationUserByInvestigationId = (req, res, next) => { 
    var sessionId = req.params.sessionId;  
    var investigationId = req.params.investigationId; 
           
    function onSuccess(InvestigationUsers){        
        res.send(InvestigationUsers);
    }
    function onError(error){       
       next(error);
    }

    try{        
        icat.getInvestigationUserByInvestigationId(sessionId, investigationId, onSuccess, onError);
    }
    catch(error){
        console.log(error);
    }
};

exports.getInvestigationsBySessionId = (req, res) => {
    var sessionId = req.params.sessionId;

    var opendata = false;
    var useris = 'participant';
    if (req.query.useris){
        useris = req.query.useris;
    }
    if (req.query.opendata){
        opendata = req.query.opendata;
    }
    global.gLogger.info ("getInvestigationsBySessionId. ", {sessionId : req.params.sessionId, opendata : opendata});  

    function onSuccess(investigations){  
        cache.setAllInvestigations(sessionId, investigations);        
        cache.setAllInvestigationsId(sessionId, investigations.map(function (o) { return o.Investigation.id; }));                     
        global.gLogger.info ("getInvestigationsBySessionId done successfully. ", {openData: opendata, sessionId : req.query.sessionId, investigationsCount : investigations.length});        
        res.send(investigations);
        
    }
    function onError(error){               
       next(error);
    }
    try{       
        var investigations = [];

        /** Instrument scientist */
        if (useris == 'instrumentscientist'){
            function onInstrumentScientistSuccess(investigations){
                global.gLogger.info ("getInvestigationByInstrumentScientist done", {investigationCount : investigations.length});
                res.send(investigations);
            }
            icat.getInvestigationByInstrumentScientist(sessionId, onInstrumentScientistSuccess, onError);
        }

        /** Participant and Open Data */
        if (useris == 'participant'){
            if (opendata == true){
                investigations = cache.getAllInvestigationsBySessionId(sessionId);
                if (!investigations){
                    icat.getAllInvestigationsBySessionId(sessionId, onSuccess, onError);
                }
                else{
                    global.gLogger.info ("getInvestigationsBySessionId retrieved from cache. ", {sessionId : sessionId, investigationsCount : investigations.length}); 
                    onSuccess(investigations);
                }         
            }
            else{
                /** Participant it returns only my data */
                investigations = cache.getMyInvestigationsBySessionId(sessionId);
                if (investigations){
                    onSuccess(investigations); 
                }
                else{
                    icat.getInvestigationsBySessionId(sessionId, onSuccess, onError);
                }
            } 
        }
       
    }
    catch(error){
        global.gLogger.error(error);
    }
};

exports.getReleasedInvestigations = (req, res) => {    
    var sessionId = req.params.sessionId;
    global.gLogger.debug ("getReleasedInvestigations. ", {sessionId : sessionId}); 
    function onSuccess(investigations){                          
        global.gLogger.info ("getReleasedInvestigations done successfully. ", {sessionId : req.params.sessionId, investigationsCount : investigations.length});        
        res.send(investigations);        
    }

    function onError(error){               
       next(error);
    }
    icat.getReleasedInvestigationsBySessionId(sessionId, onSuccess, onError);
};

exports.getEmbargoedInvestigations = (req, res) => {
    var sessionId = req.params.sessionId;
    global.gLogger.debug ("getEmbargoedInvestigations. ", {sessionId : sessionId}); 
    function onSuccess(investigations){                          
        global.gLogger.info ("getEmbargoedInvestigations done successfully. ", {sessionId : req.query.sessionId, investigationsCount : investigations.length});        
        res.send(investigations);        
    }

    function onError(error){               
       next(error);
    }

    icat.getEmbargoedInvestigationsBySessionId(sessionId, onSuccess, onError);
};


exports.getInvestigationByInstrumentScientist = (req, res) => {
    var sessionId = req.params.sessionId;
    global.gLogger.debug ("getInvestigationByInvestigationsScientist. ", {sessionId : sessionId}); 
    function onSuccess(investigations){                          
        global.gLogger.info ("getInvestigationByInvestigationsScientist done successfully. ", {sessionId : req.query.sessionId, investigationsCount : investigations.length});        
        res.send(investigations);        
    }

    function onError(error){               
       next(error);
    }

    icat.getInvestigationByInstrumentScientist(sessionId, onSuccess, onError);
}; 

