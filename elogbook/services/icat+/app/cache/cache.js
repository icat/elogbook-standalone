const icat = require('../api/icat.js');
const NodeCache = require("node-cache");



/**
 * Default construction of the object
 */
var nodeCache = new NodeCache({ stdTTL: 120 * 60, checkperiod: 120 });

exports.getStats = () => {
    return nodeCache.getStats();
};

exports.getKeys = () => {
    return nodeCache.keys();
};

exports.getDataByKey = (key) => {
    return nodeCache.mget([key]);
};



exports.getSessionById = (sessionId) => {
    global.gLogger.info("getSessionById");
    return nodeCache.get(sessionId);
};

exports.getParameterTypes = () => {
    return nodeCache.get("parameterTypes");
};

/**
 * It returns a dictionary id: name 
 * Example:   
 * '127988408': 'InstrumentSource_current_start',
  '127988409': 'TOMO_i0_end',
  '127988410': 'InstrumentSource_current_end',
  '127988411': 'TOMO_it_end',
  '128587580': 'ResourcesGallery',
  '129127505': 'ResourcesGalleryFilePaths'
 */
exports.getParameterTypeDictionary = (id) => {
    return nodeCache.get("parametersDictionary");
};

exports.getParameterTypeById = (id) => {
    var attribute = nodeCache.get("parametersDictionary");
    if (attribute) {
        return attribute[id];
    }
    return null;
};

exports.getInvestigationTypes = () => {
    return nodeCache.get("investigationTypes");
};

exports.getInvestigationTypeNamesList = () => {
    var types = nodeCache.get("investigationTypes");
    return types.map(function (type) { return type.InvestigationType.name; });
};

getAdministrators = () => {
    return nodeCache.get("administrators");
};


initInvestigationTypes = () => {
  
    return new Promise((resolve, reject) => {
        /** Get the investigation Types */
        var _this = this;
        function onInvestigationTypeSuccess(response) {
            nodeCache.set("investigationTypes", response, 0);
            global.gLogger.info("InvestigationTypes is set.", { investigationTypes: _this.getInvestigationTypeNamesList() });            
            resolve();            
        }

        function onInvestigationTypeError(error) {
            global.gLogger.error("Error retrieving InvestigationTypes", { error: error } );
            reject();
        }
        icat.getInvestigationTypes(global.gConfig.icat.authorizations.parameterListReader.user, onInvestigationTypeSuccess, onInvestigationTypeError);
    });
 

};


initParameterTypeList = () => {
    return new Promise((resolve, reject) => {
        /** Get the parameter List */
        global.gLogger.info("[cache] Init global cache");
        function onSuccess(data) {
            global.gLogger.info("ParameterTypeList retrieved for global cache", { parametersCount: data.length });
            nodeCache.set("parameterTypes", data, 0);
            var parametersDictionary = {};
            for (var i = 0; i < data.length; i++) {
                parametersDictionary[data[i].ParameterType.id] = data[i].ParameterType.name;
            }
            nodeCache.set("parametersDictionary", parametersDictionary, 0);
            resolve();
        }
        function onError(error) {
            global.gLogger.error("Error retrieving parameterTypeList", { error: error });
            reject()
        }
        icat.getParameterTypeList(global.gConfig.icat.authorizations.parameterListReader.user, onSuccess, onError);
    })
};

initAdministrators = () => {
    return new Promise((resolve, reject) => {
        /** Get users that are admins */
        function onUserSuccess(response) {
            nodeCache.set("administrators", response, 0);
            var admins = ((getAdministrators().map(function (u) { return (u.User.fullName + "( " + u.User.name + " )"); })));
            global.gLogger.info("getUsersByGroupName is set.", { administrators: admins });
            resolve();
        }

        function onUserError(error) {
            global.gLogger.error("Error retrieving getUsersByGroupName", error);
            reject();
        }
        icat.getUsersByGroupName(global.gConfig.icat.authorizations.adminUsersReader.user, global.gConfig.icat.authorizations.adminUsersReader.administrationGroups, onUserSuccess, onUserError);
    })
};


initUsers = () => {
    return new Promise((resolve, reject) => {
        /** Get users */
        function onUserSuccess(users) {
            nodeCache.set("users", users, 0);
            global.gLogger.info("[cache] Retrieved users", { count: users.length });
            resolve();
        }

        function onUserError(error) {
            global.gLogger.error(error);
            reject();
        }
        icat.getUsers(global.gConfig.icat.authorizations.adminUsersReader.user, onUserSuccess, onUserError);
    })
};

exports.getUsers = () => {
    return nodeCache.get("users");
};


exports.getLifeTimeMinutes = () => {
    return nodeCache.get("lifetimeMinutes");
};

setLifeTimeMinutes = (lifetimeMinutes) => {
    global.gLogger.info("[cache] Set TTL = " + lifetimeMinutes + " minutes");
    nodeCache.set("lifetimeMinutes", lifetimeMinutes, 0);
};
/* 
This method initializes the cache object. There are two functions:
    a) Sets the stdTTL to the ICAT configured life time minutes
    b) Get the list of parameterType as it is supposed not to change very frequently.    
[WARNING] ParameterTypes are configured with a TTL of infinite. In case these parameters change then it is needed to restart this server.
*/
exports.init = () => {

    icat.getPropertiesQuery(
        function (response) {
            if (response) {
                if (response.lifetimeMinutes) {
                    nodeCache = new NodeCache({ stdTTL: response.lifetimeMinutes * 60, checkperiod: 120 });
                    this.setLifeTimeMinutes(response.lifetimeMinutes);

                }
                else {
                    global.gLogger.error("[cache] No lifetimeMinutes retrieved. Exiting");
                    process.exit();
                }
            }
        },
        function (error) {
            console.log(error);
            global.gLogger.error("[cache] Error retrieving properties from ICAT. Exiting");
            process.exit();

        });
    return Promise.all([
        initInvestigationTypes(),
        initParameterTypeList(),
        initAdministrators(),
        initUsers()
    ])

};

exports.setKey = (sessionId, key, value) => {
    var session = this.getSessionById(sessionId);
    if (!session) {
        global.gLogger.info("{}  => cache", { sessionId: sessionId });
        nodeCache.set(sessionId, {});
        session = this.getSessionById(sessionId);
    }

    session[key] = value;
    global.gLogger.info(key + " => cache", { sessionId: sessionId, key: key });
    nodeCache.set(sessionId, session);
};
/**
 * This method sets the list of valid id of investigations for given a sessionId
 */
exports.setAllInvestigationsId = (sessionId, investigationsId) => {
    this.setKey(sessionId, "investigationsId", investigationsId);
};

/**
 * This method return an array with the investigations that I am participant
 */
exports.getInvestigationsId = (sessionId) => {
    global.gLogger.debug("getInvestigationsId " + sessionId);
    var session = this.getSessionById(sessionId);
    if (session) {
        if (session.investigationsId) {
            global.gLogger.info("cache => investigationsId", { sessionId: sessionId, investigationsId: session.investigationsId });
            return session.investigationsId;
        }
    }
    global.gLogger.info("investigationsId No session found", { sessionId: sessionId });
    return null;
};


/**
 * This method sets the list of investigations for given a sessionId
 */
exports.setAllInvestigations = (sessionId, investigations) => {
    this.setKey(sessionId, "investigations", investigations);
};

/**
 * This method sets the list of investigations for given a sessionId
 */
exports.setInvestigations = (sessionId, investigations) => {
    this.setKey(sessionId, "myInvestigations", investigations);
};


/**
 * This method sets the list of datacollections for given a sessionId
 */
exports.setDataCollections = (sessionId, datacollections) => {
    this.setKey(sessionId, "datacollections", datacollections);
};

/**
 * This method sets the list of datacollections for given a sessionId
 */
exports.getDataCollections = (sessionId) => {
    global.gLogger.debug("getDataCollections " + sessionId);
    var session = this.getSessionById(sessionId);
    if (session) {        
        global.gLogger.info("cache => datacollections", { sessionId: sessionId });
        return session.datacollections;        
    }
    global.gLogger.info("No session found", { sessionId: sessionId });
    return null;
};

/**
 * This method sets the list of investigations for given a sessionId
 */
exports.setUsername = (sessionId, username) => {
    this.setKey(sessionId, "username", username);
};

/**
 * This method gets the username for given a sessionId
 */
exports.getUsernameBySessionId = (sessionId) => {
    var session = this.getSessionById(sessionId);
    if (session) {
        if (session.username) {
            global.gLogger.info("cache => username", { sessionId: sessionId, username: session.username });
            return session.username;
        }
    }
    global.gLogger.info("getUsernameBySessionId No session found", { sessionId: sessionId });
    return null;
};

/**
 * This method gets the user fullName for given a sessionId
 */
exports.getUserFullNameBySessionId = (sessionId) => {
    var name = this.getUsernameBySessionId(sessionId);
    var user = this.getUsers().find(function (user) { return (user.User.name == name); });
    if (user) {

        return user.User.fullName;
    }
    return "Unknown";
};


/**
 * Returns true if the sessionId belongs to a user that is administrator. 
 */
exports.isAdministrator = (sessionId) => {
    var administrators = getAdministrators();
    if (administrators) {
        var names = administrators.map(function (o) { return o.User.name; });
        var username = (this.getUsernameBySessionId(sessionId));

        if (names.find(function (o) { return username == o; })) {
            return true;
        }
    }
    return false;
};


/**
 * This method gets the list of investigations for given a sessionId
 */
exports.getInvestigationById = (sessionId, investigationId) => {
    var allInvestigations = this.getAllInvestigationsBySessionId(sessionId);
    var myInvestigations = this.getMyInvestigationsBySessionId(sessionId);
    if (allInvestigations){
        var found = allInvestigations.find(function (inv) { return (inv.Investigation.id == investigationId); });
        if (found){
            return found;
        }
    }
    if (myInvestigations){
        return myInvestigations.find(function (inv) { return (inv.Investigation.id == investigationId); });
    }
    return null;
    
};

/**
 * This method gets the list of investigations for given a sessionId
 */
exports.getAllInvestigationsBySessionId = (sessionId) => {
    var session = this.getSessionById(sessionId);
    if (session) {
        if (session.investigations) {
            global.gLogger.info("cache => investigations", { sessionId: sessionId, investigationCount: session.investigations.length });
            return session.investigations;
        }
    }
    global.gLogger.info("getInvestigationsBySessionId No session found", { sessionId: sessionId });
    return null;
};

/**
 * This method gets the list of investigations for given a sessionId
 */
exports.getMyInvestigationsBySessionId = (sessionId) => {
    var session = this.getSessionById(sessionId);
    if (session) {
        if (session.myInvestigations) {
            global.gLogger.info("cache => investigations", { sessionId: sessionId, investigationCount: session.myInvestigations.length });
            return session.myInvestigations;
        }
    }
    global.gLogger.info("getMyInvestigationsBySessionId No session found", { sessionId: sessionId });
    return null;
};


/**
 * This method intializes a sessionId after the users has done log in
 */
exports.initSession = (sessionId, username, next, onAllInvestigationsRetrieved) => {
    var _this = this;
    global.gLogger.info("Init cache for user", { sessionId: sessionId, username: username });
    if (sessionId) {

        function onSessionSuccess(data) {
            _this.setUsername(sessionId, data.userName);
            global.gLogger.info("[cache] Username is set", { sessionId: sessionId, username: username, isAdministrator: _this.isAdministrator(sessionId) });

   
           
             var onSuccessDataCollection = function (datacollections) {                 
                global.gLogger.debug("[cache] Retrieved " + datacollections.length + " datacollections from ICAT by sessionId.", { sessionId: sessionId, username: username });                            
                _this.setDataCollections(sessionId, datacollections);
            }
            var onErrorDataCollection = function (error) {
                global.gLogger.error(error);
                global.gLogger.error("Error retrieving datacollections during cache initialization for a session.", { sessionId: sessionId, error: error })
            }
            global.gLogger.debug("[cache] Retrieving datacollections by sessionId", { sessionId: sessionId });
            icat.getDataColletionsBySessionId(sessionId, onSuccessDataCollection, onErrorDataCollection);




            var onSuccess = function (investigations) {
                global.gLogger.debug("[cache] Retrieved " + investigations.length + " investigations from ICAT by sessionId.", { sessionId: sessionId, username: username });
                _this.setInvestigations(sessionId, investigations);
                next();
            }
            var onError = function (error) {
                global.gLogger.error(error);
                global.gLogger.error("Error retrieving investigations during cache initialization for a session.", { sessionId: sessionId, error: error })
            }
            global.gLogger.debug("[cache] Retrieving investigations by sessionId", { sessionId: sessionId });
            icat.getInvestigationsBySessionId(sessionId, onSuccess, onError);
        }

        function onSessionError(error) {
            global.gLogger.error("Error retrieving session.", { sessionId: sessionId, error: error })
        }

        icat.getSessionInformation(sessionId, onSessionSuccess, onSessionError)
    }
    else {
        global.gLogger.error("Try to init cache for user with no sessionId", { sessionId: sessionId, username: username });
    }
};

exports.cache = nodeCache