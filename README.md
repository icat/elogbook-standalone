## Elogbook standalone

The goal of this project is, by using docker containers, to deploy the Electronic Logbook standalone. The Electronic Logbook is built on top of a metadata catalogue [ICAT](https://icatproject.org).

# Table of Contents
1. [Metadata catalogue](#metadata-catalogue)
2. [Electronic Logbook](#electronic-logbook)
3. [Installation](#installation)
4. [Notifications](#notifications)


### Metadata catalogue

The ICAT project provides a metadata catalogue and related components to support  Large Facility experimental data, linking all aspects of the research chain from proposal through to publication.

> In the context of this project the Metadata Catalogue is used to store investigations, users and permissions. Each logbook is associated to one investigation.

1. API

* ICAT provides both a SOAP and a RESTful web service to communicate with the core ICAT code which makes calls to a relational DBMS. Documentation can be found on [here](https://repo.icatproject.org/site/icat/server/4.8.0/miredot/index.html)

2. Accessing from Python

* python-icat is a Python package that provides a collection of modules for writing programs that access an ICAT service using the SOAP interface. It is based on Suds and extends it with ICAT specific features. Documentation can be found [here](https://icatproject.org/user-documentation/python-icat/)

3. Accessing from JAVA

* The ICAT4 API is a layer on top of a relational DBMS. The database is wrapped as a web service so that the tables are not exposed directly. Each table in the database is mapped onto a data structure exposed by the web service. When the web service interface definition (WSDL) is processed for Java then each data structure results in a class definition. Documentation can be found [here](https://icatproject.org/mvn/site/icat/client/4.3.3/manual.html)

### Electronic Logbook

* The Electronic Logbook is NodeJS application that includes a frontend done in REACT and a set of Restful web services (icat+) that allows to connect to the metadata catalogue and store the data into the MongoDB database. More detailed information can be found [here](https://gitlab.esrf.fr/icat/icat-plus)

| Screenshot             |  
:-------------------------:|
![](images/6.png)  | 
![](images/2.png)  |  
![](images/3.png)  |
![](images/1.png)  |  





### Installation

#### Requirements

* Docker

#### Installlation steps

> Basically, we need two databases: mysql/mariadb and Mongo. Some data will be injected in these databases in order to get some data to play with. For software, we would need to run a docker for the metadata catalogue, ICAT, and the electronic logbook (code name ICAT+)

1. Clone this project

```
git clone https://gitlab.esrf.fr/icat/elogbook-standalone.git
cd elogbook-standalone
```

2. Run MongoDB database and load data

> This starts a mongoDB instance on localhost in detached mode. We could use a running instance in a remote machine but then [the configuration file](https://gitlab.esrf.fr/icat/elogbook-standalone/blob/master/elogbook/services/icat+/config/server.config.js#L15) should be updated pointing to choosen database.
```
docker run -d --name mongo -p 27017:27017 mongo
docker cp database/icatplus.json mongo:/
docker exec mongo mongoimport --db=icatplus --collection=events --file=icatplus.json

```

3. Run Mysql 

> This will crete a MySQL instance with two users: root/root, icat/icat and a database called icat

```
docker run -d -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=icat -e MYSQL_USER=icat -e MSQL_PASSWORD=icat mysql:5.6
```

4. Grant privileges and load data


```
docker exec mysql /usr/bin/mysql -u root -proot -e "grant all privileges on *.* to icat@'%' identified by 'icat' with grant option;"
docker exec mysql /usr/bin/mysql -u root -proot -e "flush  privileges;"
```

> I have not found any smarter way to do this

```
docker cp database/icat.sql mysql:/
docker exec -it mysql bash
/usr/bin/mysql -u root -proot icat < icat.sql
exit
```




5. Configure the Metadata Catalogue

> In order to build the metadata catalogue it is needed to store the string connections to the MySQL database in [authn_db-setup.properties](https://gitlab.esrf.fr/icat/elogbook-standalone/blob/master/catalogue/services/authn.db/authn_db-setup.properties#L11) and [icat-setup.properties](https://gitlab.esrf.fr/icat/elogbook-standalone/blob/master/catalogue/services/icat.server-4.8/icat-setup.properties#L9)

```
sed -i "s/lindemaria/$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mysql)/g" catalogue/services/icat.server-4.8/icat-setup.properties
sed -i "s/lindemaria/$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mysql)/g" catalogue/services/authn.db/authn_db-setup.properties
```

6. Build and run the Metadata Catalogue


```
cd catalogue
docker build -t catalogue .
docker run -d -it -p 8080:8080 -p 8181:8181 -p 4848:4848 catalogue
```

7. Build and run the Elogbook
```
cd elogbook
docker build -t elogbook .
docker run -d -it --network="host" -p 8000:8000 -p 3000:3000 elogbook
```


8. Test it

At this stage someone could access to the app with http://localhost:3000

> Credentials : icat / icat

![](images/7.png)

### Notifications

An API allows to send notifications to the elogbook. The signature of this method can be found [here](https://icatplus.esrf.fr/api-docs/#/Logbook/post_logbook__sessionId__investigation_name__investigationName__instrument_name__instrumentName__event)

Signature of the method is:
> POST http://{server}:{port}/logbook/be70ac55-fd08-4840-9b29/investigation/name/{investigation}/instrument/name/{instrument}/event

Form data is composed by a json with the type, category and content of the notification. Example:
```
{
"type":"notification", 
"category":"info",
"content":[
    { 
	"text":"This is a info message",
        "format":"plainText"
}],
"creationDate":"2019-10-07T14:56:42.417368"
}
```

Example: 
```
curl -d '{"type":"notification", "category":"info","content":[{"text":"This is a info message","format":"plainText"}],"creationDate":"2019-10-07T14:56:42.417368"}' -H "Content-Type: application/json" -X POST http://lindemaria:8000/logbook/be70ac55-fd08-4840-9b29/investigation/name/ELOG-0001/instrument/name/ID00/event

```

#### Categories

There are several type of categories: 
1. info
2. error
3. debug
4. commandLine
5. comment

#### Format

##### Plain Text

```
{
"type":"notification", 
"category":"info",
"content":[
    { 
	"text":"This is a info message",
        "format":"plainText"
}],
"creationDate":"2019-10-07T14:56:42.417368"
}
```


##### HTML

HTML is also supported when format=html

```
{
	"type":"notification",
	"category":"info",
	"content":[
		{
			"text":"This is a more sophisticated HTML info message containing 1 table that was generated by the CURL command below
				<table class=\ "table table-striped table-condensed table-hover\">
				    <tbody>
					<tr>
					    <td>m2angle</td>
					    <td class=\ "value\">26</td>
					</tr>
					<tr>
					    <td>m0tz</td>
					    <td class=\ "value\">-22</td>
					</tr>
				       [...............]
					<tr>
					    <td>rotz</td>
					    <td class=\ "value\">NaN</td>
					</tr>
					<tr>
					    <td>tilt</td>
					    <td class=\ "value\">0</td>
					</tr>
				    </tbody>
				</table>",
			"format":"html"
		}
	],
	"creationDate":"2019-10-07T14:56:42.417368"
}
```
Example
```
curl -d '{"category":"info","content":[{"text":"This is a more sophisticated HTML info message containing 1 table that was generated by the CURL command below <table class=\"table table-striped table-condensed table-hover\"><tbody><tr><td>m2angle</td><td class=\"value\">26</td></tr><tr><td>m0tz</td><td class=\"value\">-22</td></tr><tr><td>m0tyrot</td><td class=\"value\">6.7</td></tr><tr><td>mono</td><td class=\"value\">15.9385</td></tr><tr><td>enmono</td><td class=\"value\">7.20004</td></tr><tr><td>r2theta</td><td class=\"value\">-0.0829958</td></tr><tr><td>r2gamma</td><td class=\"value\">-0.456489</td></tr><tr><td>qg2</td><td class=\"value\">-0.660105</td></tr><tr><td>dcmy</td><td class=\"value\">5.5</td></tr><tr><td>dcmz</td><td class=\"value\">0.85</td></tr><tr><td>zpx</td><td class=\"value\">-18.5746</td></tr><tr><td>zpy</td><td class=\"value\">-3.49</td></tr><tr><td>zpz</td><td class=\"value\">7.16492</td></tr><tr><td>zpz1</td><td class=\"value\">7.06708</td></tr><tr><td>zpz2</td><td class=\"value\">7.26277</td></tr><tr><td>zpz3</td><td class=\"value\">7.06708</td></tr><tr><td>zppy</td><td class=\"value\">7.5</td></tr><tr><td>zppz</td><td class=\"value\">100</td></tr><tr><td>kbhf</td><td class=\"value\">0.141575</td></tr><tr><td>kbvf</td><td class=\"value\">-0.511</td></tr><tr><td>hexx</td><td class=\"value\">NaN</td></tr><tr><td>hexy</td><td class=\"value\">NaN</td></tr><tr><td>hexz</td><td class=\"value\">NaN</td></tr><tr><td>rotx</td><td class=\"value\">NaN</td></tr><tr><td>roty</td><td class=\"value\">NaN</td></tr><tr><td>rotz</td><td class=\"value\">NaN</td></tr><tr><td>tilt</td><td class=\"value\">0</td></tr></tbody></table>","format":"html"}],"dataset":null,"creationDate":"2019-10-07T14:56:42.417368","type":"notification"}' -H "Content-Type: application/json" -X POST http://localhost:8000/logbook/be70ac55-fd08-4840-9b29/investigation/name/ELOG-0001/instrument/name/ID00/event

```

![](images/4.png) 

#### Images

Images can be also uploaded via API either uploading as a file or taken from a mobile device (tablet or smartphone).

![](images/5.png) 
