icat.server
-----------

General installation instructions are at https://icatproject.org/installation/component

Specific installation instructions are at https://repo.icatproject.org/site/icat/server/4.8.0/installation.html

All documentation on the icat.server may be found at https://repo.icatproject.org/site/icat/server/4.8.0
