authn_db
--------

General installation instructions are at http://www.icatproject.org/installation/component

Specific installation instructions are at http://www.icatproject.org/mvn/site/authn_db/1.2.0/installation.html

All documentation on authn_db may be found at http://www.icatproject.org/mvn/site/authn_db/1.2.0